# Databricks notebook source
# !pip install openpyxl
# !pip install fsspec
# !pip install gcsfs
# !pip install bigquery

# COMMAND ----------

# MAGIC %run /Users/yohan.consani@seara.com.br/comparativo_produtividade/Aves/classes_dimensionador_mao_obra

# COMMAND ----------

# MAGIC %run /Users/yohan.consani@seara.com.br/comparativo_produtividade/Aves/parametros_padrao

# COMMAND ----------

# MAGIC %run /Users/yohan.consani@seara.com.br/comparativo_produtividade/Aves/query_volume_producao

# COMMAND ----------

# MAGIC %run /Users/yohan.consani@seara.com.br/comparativo_produtividade/Aves/query_vol_ton_vivo

# COMMAND ----------

import pandas as pd
import numpy as np
import math
from datetime import datetime
from datetime import timedelta
from classes_dimensionador_mao_obra import AvePesadaCobbMacho
from parametros_padrao import dict_prod_padrao_desossa
from parametros_padrao import dict_prod_padrao_revisao
from parametros_padrao import dict_minutos_efetivos_turno
from query_volume_producao import funcao_query_volume_producao
from query_vol_ton_vivo import funcao_query_vol_ton_vivo

# COMMAND ----------

from google.cloud import bigquery as bq
client = bq.Client()

# COMMAND ----------

data_ref = str(datetime.today().date())
#data_ref = str(datetime(2021,3,31).date())

# COMMAND ----------

#teste = pd.read_csv('gs://gera-txt-funcionario/JORNADA.csv', sep = ';')

#teste

# COMMAND ----------

data_inicial = '2021-01-01'

data_final = datetime.today().date() - timedelta(days = 2)

#data_final = datetime.today().date()

data_final

# COMMAND ----------

# MAGIC %md
# MAGIC ### Caminhos das bases

# COMMAND ----------

path_params_fixos_planejamento = 'gs://parametros_fixos/Parâmetros fixos.xlsx'
sheet_params_fixos_planejamento = "Planejamento"

path_params_fixos_absenteismo = 'gs://parametros_fixos/Parâmetros fixos.xlsx'
sheet_params_fixos_absenteismo = "Absenteísmo"

# COMMAND ----------

# MAGIC %md
# MAGIC ### Parâmetros fixos

# COMMAND ----------

# MAGIC %md
# MAGIC #### Porcentual absenteísmo + férias

# COMMAND ----------

params_fixos_absenteismo = pd.read_excel(path_params_fixos_absenteismo, sheet_name = sheet_params_fixos_absenteismo)

params_fixos_absenteismo.drop(columns=['Diretoria'], inplace = True)

params_fixos_absenteismo['Absenteismo'] = params_fixos_absenteismo['Absenteismo'].apply(lambda x: math.modf(x)[1] + (math.modf(x)[0] > 0) if not pd.isnull(x) else x)

params_fixos_absenteismo['Atestados COVID'] = params_fixos_absenteismo['Atestados COVID'].apply(lambda x: math.modf(x)[1] + (math.modf(x)[0] > 0) if not pd.isnull(x) else x)

params_fixos_absenteismo['Fator_absent'] = round((params_fixos_absenteismo['Absenteismo'])/params_fixos_absenteismo['QL Realizado'],3)

params_fixos_absenteismo['Fator_absent_ferias'] = round((params_fixos_absenteismo['Férias']+params_fixos_absenteismo['Absenteismo'])/params_fixos_absenteismo['QL Realizado'],3)

params_fixos_absenteismo['Fator_absent_covid'] = round((params_fixos_absenteismo['Atestados COVID']+params_fixos_absenteismo['Absenteismo'])/params_fixos_absenteismo['QL Realizado'],3)

params_fixos_absenteismo['Fator_absent_ferias_covid'] = round((params_fixos_absenteismo['Férias']+params_fixos_absenteismo['Absenteismo']+params_fixos_absenteismo['Atestados COVID'])/params_fixos_absenteismo['QL Realizado'],3)

params_fixos_absenteismo['MES REF'] = params_fixos_absenteismo['Ciclo'].apply(lambda x: f'{str(x)[0]}/20{str(x)[-2:]}')

params_fixos_absenteismo['CD_FILIAL'] = params_fixos_absenteismo['Unidade'].apply(lambda x: int(x[:3]))

# COMMAND ----------

# MAGIC %md
# MAGIC #### Planejamento de Produção - Cota Desossa

# COMMAND ----------

#params_fixos_cod_filial = pd.read_excel(path_params_fixos_cod_filial, sheet_name = sheet_params_fixos_cod_filial)

#params_fixos_cod_filial['COD_FILIAL'] = params_fixos_cod_filial['COD_FILIAL'].astype(str)

# COMMAND ----------

params_fixos_planejamento = pd.read_excel(path_params_fixos_planejamento, sheet_name = sheet_params_fixos_planejamento)

#params_fixos_planejamento['FILIAL'] = params_fixos_planejamento['FILIAL'].astype(str)

#params_fixos_planejamento = params_fixos_planejamento.merge(params_fixos_cod_filial[['Nome abreviado','COD_FILIAL']], how = 'left', left_on = 'FILIAL', right_on = 'COD_FILIAL')

#params_fixos_planejamento.drop(columns=['COD_FILIAL'], inplace = True)

# params_fixos_planejamento.head()

# COMMAND ----------

params_fixos_planejamento['STATUS PELE'] = params_fixos_planejamento['G GERENCIAL'].apply(lambda x: 'C/P' if x in ['LEG BONELESS SKIN ON', 'LEG BONELESS KAKUGIRI'] else (
    'S/P' if x == 'LEG BONELESS SKINLESS' else 'Erro'
))

# COMMAND ----------

params_fixos_planejamento.columns = [ x.rstrip() for x in params_fixos_planejamento.columns]

# COMMAND ----------

params_fixos_planejamento = params_fixos_planejamento.groupby(['FILIAL','STATUS PELE']).agg({
    'VOL DIA 202104': 'sum',
    'VOL DIA 202105': 'sum',
    'VOL DIA 202106': 'sum'
}).reset_index()

# COMMAND ----------

# params_fixos_planejamento.head()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Quadro de funcionários tratado e consolidado

# COMMAND ----------

query = """
SELECT
    *
FROM `dmdo-gastos-305013.bases_qlik.quadro_funcionarios_consolidado`
"""

df_quadro_funcionarios = client.query(query).to_dataframe()

df_quadro_funcionarios['DATA'] = pd.to_datetime(df_quadro_funcionarios['DATA'])

print(df_quadro_funcionarios.shape)

# df_quadro_funcionarios.head()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Capacidade Nominal

# COMMAND ----------

query = """
SELECT
    *
FROM `dmdo-gastos-305013.bases_qlik.capacidade_nominal`
"""

df_capacidade_nominal = client.query(query).to_dataframe()

df_capacidade_nominal.rename(columns={'Capacidade_Nominal_Pernasmin': 'Capacidade_Nominal_Pernas_min'}, inplace = True)

print(df_capacidade_nominal.shape)

# df_capacidade_nominal.head()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Siglas Filiais

# COMMAND ----------

query = """
SELECT
    CD_FILIAL, SIGLA
FROM `dmdo-gastos-305013.painel_dmdo.de_para_siglas_filiais`
"""

siglas = client.query(query).to_dataframe()

print(siglas.shape)

# siglas.head()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Diretorias adjuntas

# COMMAND ----------

query = """
SELECT
    COD_FILIAL,
    Empresa,
    NomeDiretoriaAdjunta
FROM `dmdo-gastos-305013.painel_dmdo.de_para_diretoria_adjunta`
"""

diretoria_adjunta = client.query(query).to_dataframe()

diretoria_adjunta = diretoria_adjunta[diretoria_adjunta['COD_FILIAL'].isin(siglas['CD_FILIAL'])]

diretoria_adjunta = diretoria_adjunta[diretoria_adjunta['NomeDiretoriaAdjunta'].isin(
    ['AVES PESADAS - SP',
    'AVES PESADAS - C. OESTE, BA E MG', 
    'AVES PESADAS - PR',
    'AVES PESADAS - RS E SC']
)].sort_values(by=['COD_FILIAL'])

diretoria_adjunta.rename(columns={'COD_FILIAL': 'CD_FILIAL'}, inplace = True)

print(diretoria_adjunta.shape)

# diretoria_adjunta

# COMMAND ----------

# MAGIC %md
# MAGIC ### Volume de Produção - BigQuery

# COMMAND ----------

query = funcao_query_volume_producao(data_inicial, data_final)

df_vol_prod = client.query(query).to_dataframe()

df_vol_prod = df_vol_prod.merge(siglas, how = 'left', on = 'CD_FILIAL')

print(df_vol_prod.shape)

# df_vol_prod.head()

# COMMAND ----------

df_vol_prod = df_vol_prod[df_vol_prod['QNT'] > 0]

df_vol_prod['QNT'] = df_vol_prod['QNT']/1000

df_vol_prod['CD_TURNO_PRODUCAO'] = df_vol_prod['CD_TURNO_PRODUCAO'].astype(str)

df_vol_prod.reset_index(drop = True, inplace = True)

df_vol_prod['FILIAL'] = df_vol_prod['CD_FILIAL'].astype(str)+' - '+df_vol_prod['NM_FILIAL']

df_vol_prod.rename(columns={
    'DT_PRODUCAO': 'DATA',
    'NM_ITEM': 'ITEM',
    'NM_CLASSE': 'CLASSE',
    'NM_SUBGRUPO': 'SUBGRUPO',
    'DS_FAM_ANALITICA_PRO': 'FAM. ANALITICA',
    'DS_FAM_SINTETICA_PRO': 'FAM. SINTETICA',
    'DS_NIVEL_INDUSTRIALIZACAO': 'NV. INDUSTRIALIZAÇÃO',
    'DS_DESTINO_FINAL': 'DESTINO',
    'CD_TURNO_PRODUCAO': 'TURNO',
    'QNT': 'REAL'
},inplace = True)

# df_vol_prod.head()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Volume Ton Vivo - BigQuery

# COMMAND ----------

# df_vol_ton_vivo = pd.read_csv(path_vol_ton_vivo, sep=';', low_memory=False)
# df_vol_ton_vivo = df_vol_ton_vivo.loc[1:]
# df_vol_ton_vivo.reset_index(drop = True, inplace = True)

# df_vol_ton_vivo['PESO M.'] = df_vol_ton_vivo['PESO M.'].apply(lambda x: 1000*float(x.replace(',','.')))

# df_vol_ton_vivo = df_vol_ton_vivo.rename(columns = {'PESO M.': 'PESO MÉDIO (g)'})

# df_vol_ton_vivo = df_vol_ton_vivo[df_vol_ton_vivo['TURNO'].isin([1,2,3])]

# df_vol_ton_vivo['CD FILIAL'] =  df_vol_ton_vivo['CD FILIAL'].astype(int)
# df_vol_ton_vivo['TURNO'] =  df_vol_ton_vivo['TURNO'].astype(int)

# df_vol_ton_vivo['DATA'] = pd.to_datetime(df_vol_ton_vivo['DATA'], format='%d/%m/%Y')

# df_vol_ton_vivo.head()

# COMMAND ----------

query = funcao_query_vol_ton_vivo(data_inicial, data_final)

df_vol_ton_vivo = client.query(query).to_dataframe()

# COMMAND ----------

df_vol_ton_vivo['DATA'] = pd.to_datetime(df_vol_ton_vivo['DATA'], format = '%d/%m/%Y %H:%M:%S')

# COMMAND ----------

df_vol_ton_vivo.rename(columns={
  'CD_FILIAL': 'CD FILIAL',
  'CD_TURNO_PRODUCAO': 'TURNO',
  'QT_CABECA': 'CABEÇAS',
  'PESO_MEDIO': 'PESO MÉDIO (g)',
  'QT_PESO': 'PESO'
}, inplace = True)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Construindo a tabela consolidada

# COMMAND ----------

df_vol_prod['STATUS OSSO'] = df_vol_prod['FAM. ANALITICA'].apply(lambda x: 'S/O' if 'S/O' in x else ('C/O' if 'C/O' in x else 'N/A'))
df_vol_prod['STATUS PELE'] = df_vol_prod['FAM. ANALITICA'].apply(lambda x: 'C/P' if 'C/P' in x else ('S/P' if 'S/P' in x else 'N/A'))

# Estamos interessados na produtividade de perna desossada, nesse primeiro momento
df_vol_prod = df_vol_prod[df_vol_prod['STATUS OSSO'] == 'S/O']

df_vol_prod_agg = df_vol_prod.groupby(['CD_FILIAL',
                                       'FILIAL',
                                       'DATA',
                                       'FAM. SINTETICA', 
                                       'STATUS OSSO',
                                       'TURNO',
                                       'STATUS PELE'
                                      ]).agg(
    PROD_REAL_Toneladas=pd.NamedAgg(column="REAL", aggfunc="sum"),
    ).reset_index()

df_vol_prod_agg['CD_FILIAL'] =  df_vol_prod_agg['CD_FILIAL'].astype(int)
df_vol_prod_agg['TURNO'] =  df_vol_prod_agg['TURNO'].astype(int)

# df_vol_prod_agg.head()

# COMMAND ----------

# Nesse momento, estamos interessados somente na seção de perna de frango
# df_vol_ton_vivo = df_vol_ton_vivo[df_vol_ton_vivo['LINHA'] == 'FRANGO CORTES']

df = df_vol_prod_agg.merge(df_vol_ton_vivo[['CD FILIAL','DATA','TURNO','PESO','CABEÇAS','PESO MÉDIO (g)']], how = 'left', left_on=['CD_FILIAL','DATA','TURNO'], right_on=['CD FILIAL','DATA','TURNO'])

df = df.drop(columns=['CD FILIAL'])

df.reset_index(drop = True, inplace = True)

# COMMAND ----------

df.loc[(df['STATUS PELE'] == 'C/P')&(~pd.isnull(df['PESO MÉDIO (g)'])),'RENDIMENTO PERNAS'] = df.loc[(df['STATUS PELE'] == 'C/P')&(~pd.isnull(df['PESO MÉDIO (g)'])),'PESO MÉDIO (g)'].apply(lambda x: round(AvePesadaCobbMacho(x).rendimento_perna_c_pele,3))

df.loc[(df['STATUS PELE'] == 'S/P')&(~pd.isnull(df['PESO MÉDIO (g)'])),'RENDIMENTO PERNAS'] = df.loc[(df['STATUS PELE'] == 'S/P')&(~pd.isnull(df['PESO MÉDIO (g)'])),'PESO MÉDIO (g)'].apply(lambda x: round(AvePesadaCobbMacho(x).rendimento_perna_s_pele,3))

# COMMAND ----------

df['PESO CADA PERNA (KG)'] = (df['PESO MÉDIO (g)']*0.001*df['RENDIMENTO PERNAS']/2).apply(lambda x: round(x, 3))

df['NUM DE PERNAS'] = (df['PROD_REAL_Toneladas']*1000/df['PESO CADA PERNA (KG)']).apply(lambda x: int(x) if not pd.isnull(x) else x)

# COMMAND ----------

df.dropna(subset=['PESO'], inplace = True)

df['MES REF'] = df['DATA'].apply(lambda x: f'{int(str(x)[:10][5:7])}/{str(x)[:10][:4]}')

# COMMAND ----------

df['CD_FILIAL'] = df['CD_FILIAL'].astype(int)

# COMMAND ----------

df['Atividade'] = 'Desossar Perna'

df_revisar = df.copy()

df_revisar['Atividade'] = 'Revisar Perna'

df = pd.concat([df, df_revisar], ignore_index = True)

df.sort_values(by=['CD_FILIAL','DATA','TURNO','STATUS PELE','Atividade'], inplace = True)

df.reset_index(drop = True, inplace = True)

# COMMAND ----------

df_quadro_funcionarios['DATA'] = pd.to_datetime(df_quadro_funcionarios['DATA'].dt.date)

# COMMAND ----------

df = df.merge(df_quadro_funcionarios[['CD_FILIAL','DATA','TURNO','Atividade','QL_ATIVO','MIN_EFETIVOS_TRABALHADOS','CAPACIDADE_NOMINAL']], how = 'left', left_on = ['CD_FILIAL','DATA','TURNO','Atividade'],
         right_on = ['CD_FILIAL','DATA','TURNO','Atividade']
        )

df['MEDIA_MIN_QL_ATIVO'] = df['MIN_EFETIVOS_TRABALHADOS']/df['QL_ATIVO']

# COMMAND ----------

# Existem dias com um número muito baixo de minutos trabalhados, o que eleva demasiadamente o valor do ql_invertido.
# Para esses dias, substitua o valor de minutos trabalhados pela média de minutos trabalhados na filial no mês

codigos_filiais = df.loc[df['MEDIA_MIN_QL_ATIVO'] <= 180, 'CD_FILIAL'].unique()

for filial in codigos_filiais:

    df.loc[(df['CD_FILIAL'] == filial)&(df['MEDIA_MIN_QL_ATIVO'] <= 180), 'MEDIA_MIN_QL_ATIVO'] = df.loc[(df['CD_FILIAL'] == filial), 'MEDIA_MIN_QL_ATIVO'].mean()

# COMMAND ----------

df['PERNAS_POR_MIN_NO_TURNO'] = df['NUM DE PERNAS']/df['MEDIA_MIN_QL_ATIVO']

# COMMAND ----------

df['QL INVERTIDO'] = (df['PERNAS_POR_MIN_NO_TURNO']/df['CAPACIDADE_NOMINAL']).apply(lambda x: math.modf(x)[1] + (math.modf(x)[0] > 0) if not pd.isnull(x) else x)

df['PRODUÇÃO POSSÍVEL PONDERADA'] = df['CAPACIDADE_NOMINAL']*df['MEDIA_MIN_QL_ATIVO']*df['QL_ATIVO']*df['PESO CADA PERNA (KG)']*0.001*df['PROD_REAL_Toneladas']

# COMMAND ----------

# MAGIC %md
# MAGIC #### Agregando as informações de QL_ATIVO à base consolidada

# COMMAND ----------

df_agg = df.groupby([
    'CD_FILIAL',
    'FILIAL',
    'DATA',
    'MES REF',
    'FAM. SINTETICA',
    'STATUS OSSO',
    'TURNO',
    'MEDIA_MIN_QL_ATIVO',
    'Atividade',
    'CAPACIDADE_NOMINAL',
    ]).agg(
        PROD_REAL_Toneladas=pd.NamedAgg(column="PROD_REAL_Toneladas", aggfunc="sum"),
        NUM_DE_PERNAS_DESOSSADAS=pd.NamedAgg(column="NUM DE PERNAS", aggfunc="sum"),
        PERNAS_POR_MIN_NO_TURNO=pd.NamedAgg(column="PERNAS_POR_MIN_NO_TURNO", aggfunc="sum"),
        QL_ATIVO=pd.NamedAgg(column="QL_ATIVO", aggfunc="mean"),
        QL_INVERTIDO=pd.NamedAgg(column="QL INVERTIDO", aggfunc="sum"),
        PROD_POSSIVEL_PONDERADA=pd.NamedAgg(column="PRODUÇÃO POSSÍVEL PONDERADA", aggfunc="sum"),
    ).reset_index()

df_agg['QL_ATIVO'] = df_agg['QL_ATIVO'].astype(int)

df_agg['QL_INVERTIDO'] = df_agg['QL_INVERTIDO'].astype(int)

df_agg['DIF QL'] = df_agg['QL_ATIVO'] - df_agg['QL_INVERTIDO']

df_agg['PROD.REAL PERNAS/MIN/COLABORADOR'] = (df_agg['PERNAS_POR_MIN_NO_TURNO']/df_agg['QL_ATIVO']).apply(lambda x: round(x, 2))

df_agg['PRODUCAO_POSSIVEL_TONS_DIA'] = (df_agg['PROD_POSSIVEL_PONDERADA']/df_agg['PROD_REAL_Toneladas']).apply(lambda x: round(x, 1))

df_agg.drop(columns=['PROD_POSSIVEL_PONDERADA'], inplace = True)

# df_agg.head()

# COMMAND ----------

df_aux_prod_possivel = df_agg[df_agg['Atividade'] == 'Desossar Perna'][[
    'CD_FILIAL', 
    'FILIAL', 
    'DATA',
    'TURNO',
    'PRODUCAO_POSSIVEL_TONS_DIA'
]].copy()

df_aux_prod_possivel_2 = df_agg[df_agg['Atividade'] == 'Revisar Perna'][[
    'CD_FILIAL', 
    'FILIAL', 
    'DATA',
    'TURNO',
    'PRODUCAO_POSSIVEL_TONS_DIA'
]].copy()

df_aux_prod_possivel = df_aux_prod_possivel.merge(df_aux_prod_possivel_2, how = 'outer', on=['CD_FILIAL', 'FILIAL', 'DATA', 'TURNO',], suffixes=("","_drop"))

df_aux_prod_possivel.loc[df_aux_prod_possivel['PRODUCAO_POSSIVEL_TONS_DIA'].isna(), 'PRODUCAO_POSSIVEL_TONS_DIA'] = df_aux_prod_possivel.loc[df_aux_prod_possivel['PRODUCAO_POSSIVEL_TONS_DIA'].isna(), 'PRODUCAO_POSSIVEL_TONS_DIA_drop']

df_aux_prod_possivel.drop(columns=['PRODUCAO_POSSIVEL_TONS_DIA_drop'], inplace = True)

# df_aux_prod_possivel

# COMMAND ----------

df_desossar_mais_revisar = df_agg.copy()

df_desossar_mais_revisar['POND_MEDIA_MIN_QL_ATIVO'] = df_desossar_mais_revisar['MEDIA_MIN_QL_ATIVO']*df_desossar_mais_revisar['QL_ATIVO']

df_desossar_mais_revisar = df_desossar_mais_revisar.groupby([
    'CD_FILIAL', 
    'FILIAL', 
    'DATA', 
    'MES REF', 
    'FAM. SINTETICA',
    'STATUS OSSO', 
    'TURNO', 
    'PROD_REAL_Toneladas', 
    'NUM_DE_PERNAS_DESOSSADAS'
    ]).agg({
        "QL_ATIVO": 'sum',
        "QL_INVERTIDO": 'sum',
        "DIF QL": 'sum',
        "POND_MEDIA_MIN_QL_ATIVO": 'sum',
        "CAPACIDADE_NOMINAL": 'min',
    }).reset_index()

df_desossar_mais_revisar.rename(columns={'DIF_QL': 'DIF QL'}, inplace = True)

#df_desossar_mais_revisar['PROD_POSSIVEL_PONDERADA'] = np.nan

df_desossar_mais_revisar['Atividade'] = 'Desossar e Revisar'

df_desossar_mais_revisar["MEDIA_MIN_QL_ATIVO"] = df_desossar_mais_revisar["POND_MEDIA_MIN_QL_ATIVO"]/df_desossar_mais_revisar["QL_ATIVO"]

df_desossar_mais_revisar['PERNAS_POR_MIN_NO_TURNO'] = df_desossar_mais_revisar['NUM_DE_PERNAS_DESOSSADAS']/df_desossar_mais_revisar["MEDIA_MIN_QL_ATIVO"]

#df_desossar_mais_revisar['PROD.REAL PERNAS/MIN/COLABORADOR'] = round(df_desossar_mais_revisar['PERNAS_POR_MIN_NO_TURNO']/df_desossar_mais_revisar['QL_ATIVO'],2)

#df_desossar_mais_revisar['QL_INVERTIDO'] = (df_desossar_mais_revisar['PERNAS_POR_MIN_NO_TURNO']/df_desossar_mais_revisar['CAPACIDADE_NOMINAL']).apply(lambda x: math.modf(x)[1] + (math.modf(x)[0] > 0) if not pd.isnull(x) else x)

df_desossar_mais_revisar['PROD.REAL PERNAS/MIN/COLABORADOR'] = df_desossar_mais_revisar['CAPACIDADE_NOMINAL']*df_desossar_mais_revisar['QL_INVERTIDO']/df_desossar_mais_revisar['QL_ATIVO']

df_desossar_mais_revisar.drop(columns=['POND_MEDIA_MIN_QL_ATIVO'], inplace = True)

df_desossar_mais_revisar = df_desossar_mais_revisar.merge(df_aux_prod_possivel, how = 'left', on = ['CD_FILIAL','FILIAL','DATA','TURNO'])

df_agg = pd.concat([df_agg, df_desossar_mais_revisar], ignore_index = True)

# df_agg.head()

# COMMAND ----------

# MAGIC %md
# MAGIC #### Marcando as anomalias da base

# COMMAND ----------

df_agg.loc[(df_agg['PROD.REAL PERNAS/MIN/COLABORADOR'] > 6.5)&(df_agg['Atividade'] == 'Desossar Perna'), 'Anomalia'] = 'Sim'
df_agg.loc[(df_agg['PROD.REAL PERNAS/MIN/COLABORADOR'] > 20)&(df_agg['Atividade'] == 'Revisar Perna'), 'Anomalia'] = 'Sim'
df_agg.loc[(df_agg['PROD.REAL PERNAS/MIN/COLABORADOR'] > 6.5)&(df_agg['Atividade'] == 'Desossar e Revisar'), 'Anomalia'] = 'Sim'

df_agg.loc[(df_agg['PROD.REAL PERNAS/MIN/COLABORADOR'] < 18)
    &(df_agg['Atividade'].isin(['Desossar Perna','Desossar e Revisar']))
    &(df_agg['CD_FILIAL'] == 280)
    , 'Anomalia'] = 'Não'

df_agg['Anomalia'].fillna('Não', inplace = True)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Criando uma tabela resumo com QL Invertido vs QL Ativo e Produtividade real vs Capacidade Nominal

# COMMAND ----------

#df_ql_invertido = df_agg.groupby(['CD_FILIAL','FILIAL','DATA','MES REF','Atividade']).agg({'QL_ATIVO': 'sum', 'QL_INVERTIDO':'sum'}).reset_index()

df_resumo_produt_ql_invertido = df_agg[['CD_FILIAL',
                                        'FILIAL',
                                        'DATA',
                                        'MES REF',
                                        'TURNO',
                                        'Atividade',
                                        'PROD_REAL_Toneladas',
                                        'QL_ATIVO',
                                        'QL_INVERTIDO',
                                        'DIF QL',
                                        'CAPACIDADE_NOMINAL',
                                        'PROD.REAL PERNAS/MIN/COLABORADOR',
                                        'PRODUCAO_POSSIVEL_TONS_DIA',
                                        'Anomalia'
                                       ]]

# df_resumo_produt_ql_invertido

# COMMAND ----------

# MAGIC %md
# MAGIC #### Dimensionamento do QL

# COMMAND ----------

mes_ref = '3/2021'
data_inicial_mes = datetime(int(mes_ref[-4:]), int(mes_ref[:mes_ref.find("/")]), 1)

# COMMAND ----------

df_produt_real_hist = df_agg[(df_agg['Anomalia'] == 'Não')&(df_agg['MES REF'] == mes_ref)].groupby(['CD_FILIAL','Atividade']).agg(PROD_REAL_PERNAS_MINUTO_HIST=pd.NamedAgg(column='PROD.REAL PERNAS/MIN/COLABORADOR', aggfunc='mean')).reset_index().round(2)

df_produt_real_hist['CD_FILIAL'] = df_produt_real_hist['CD_FILIAL'].astype(str)

# COMMAND ----------

df_dmdo = params_fixos_planejamento.round(2)

df_dmdo['FILIAL'] = df_dmdo['FILIAL'].astype(str)

# COMMAND ----------

df_prod_real = df_resumo_produt_ql_invertido[df_resumo_produt_ql_invertido['Atividade']=='Desossar Perna'].groupby(['CD_FILIAL','DATA','MES REF']).agg({
    'PROD_REAL_Toneladas': 'sum'
}).reset_index()

df_prod_real = df_prod_real[df_prod_real['MES REF'] == mes_ref]

df_prod_real = df_prod_real.groupby(['CD_FILIAL']).agg({
    'PROD_REAL_Toneladas': 'mean'
}).reset_index().round(1)

df_prod_real.rename(columns={'PROD_REAL_Toneladas': f'MÉDIA PROD REAL NO DIA {mes_ref}'}, inplace = True)

df_prod_real['CD_FILIAL'] = df_prod_real['CD_FILIAL'].astype(str)

# df_prod_real

# COMMAND ----------

df_dmdo = df_dmdo.merge(df_prod_real, how = 'left', left_on = 'FILIAL', right_on = 'CD_FILIAL')

# COMMAND ----------

df_vol_ton_vivo['DATA'] = pd.to_datetime(df_vol_ton_vivo['DATA'], format='%d/%m/%Y')

# COMMAND ----------

df_peso_medio = df_vol_ton_vivo[df_vol_ton_vivo['DATA'].apply(lambda x: x.month == data_inicial_mes.month)].copy()

df_peso_medio = df_peso_medio.groupby(['CD FILIAL']).agg({'PESO MÉDIO (g)': 'mean'}).reset_index().round()

df_peso_medio['CD FILIAL'] = df_peso_medio['CD FILIAL'].astype(str)

# COMMAND ----------

df_dmdo = df_dmdo.merge(df_peso_medio, how = 'left', left_on = 'FILIAL', right_on = 'CD FILIAL')

df_dmdo.drop(columns=['CD FILIAL'], inplace = True)

df_dmdo = df_dmdo.dropna(subset=['PESO MÉDIO (g)'])

df_dmdo.loc[df_dmdo['STATUS PELE'] == 'C/P', 'RENDIMENTO PERNAS'] = df_dmdo.loc[df_dmdo['STATUS PELE'] == 'C/P', 'PESO MÉDIO (g)'].apply(lambda x: round(AvePesadaCobbMacho(x).rendimento_perna_c_pele,3))

df_dmdo.loc[df_dmdo['STATUS PELE'] == 'S/P', 'RENDIMENTO PERNAS'] = df_dmdo.loc[df_dmdo['STATUS PELE'] == 'S/P', 'PESO MÉDIO (g)'].apply(lambda x: round(AvePesadaCobbMacho(x).rendimento_perna_s_pele,3))

# df_dmdo.head()

# COMMAND ----------

df_dmdo['PESO CADA PERNA (KG)'] = round(df_dmdo['PESO MÉDIO (g)']*df_dmdo['RENDIMENTO PERNAS']*0.001/2,3)

df_dmdo['NUM PERNAS DIA 202104'] = round(df_dmdo['VOL DIA 202104']*1000/df_dmdo['PESO CADA PERNA (KG)'])
df_dmdo['NUM PERNAS DIA 202105'] = round(df_dmdo['VOL DIA 202105']*1000/df_dmdo['PESO CADA PERNA (KG)'])
df_dmdo['NUM PERNAS DIA 202106'] = round(df_dmdo['VOL DIA 202106']*1000/df_dmdo['PESO CADA PERNA (KG)'])

# COMMAND ----------

df_dmdo = df_dmdo.groupby(['FILIAL','MÉDIA PROD REAL NO DIA 3/2021']).agg({
    'VOL DIA 202104': 'sum',
    'VOL DIA 202105': 'sum',
    'VOL DIA 202106': 'sum',
    'PESO MÉDIO (g)': 'mean',
    'NUM PERNAS DIA 202104': 'sum',
    'NUM PERNAS DIA 202105': 'sum',
    'NUM PERNAS DIA 202106': 'sum'
}).reset_index()

# COMMAND ----------

#Escolha somente os últimos dois meses
df_minutos_medio_turno = df_agg[(df_agg['Atividade'] != 'Dessosar e Revisar')&(df_agg['MES REF'].isin(['2/2021','3/2021']))&(df_agg['Anomalia'] == 'Não')]

df_minutos_medio_turno = df_minutos_medio_turno.groupby(['CD_FILIAL','TURNO','Atividade']).agg({
    'MEDIA_MIN_QL_ATIVO': 'mean',
    'QL_ATIVO': 'mean'
}).reset_index()

df_minutos_medio_turno['MEDIA_MIN_QL_ATIVO_PONDERADA'] = df_minutos_medio_turno['MEDIA_MIN_QL_ATIVO']*df_minutos_medio_turno['QL_ATIVO']

df_minutos_medio_turno = df_minutos_medio_turno.groupby(['CD_FILIAL','Atividade']).agg({
    'MEDIA_MIN_QL_ATIVO_PONDERADA': 'sum',
    'QL_ATIVO': 'sum'
}).reset_index()

df_minutos_medio_turno['MEDIA_MIN_QL_ATIVO'] = df_minutos_medio_turno['MEDIA_MIN_QL_ATIVO_PONDERADA']/df_minutos_medio_turno['QL_ATIVO']

df_minutos_medio_turno.drop(columns=['MEDIA_MIN_QL_ATIVO_PONDERADA','QL_ATIVO'], inplace = True)

df_minutos_medio_turno['CD_FILIAL'] = df_minutos_medio_turno['CD_FILIAL'].astype(str)

df_minutos_medio_turno.rename(columns={'CD_FILIAL':'FILIAL'}, inplace = True)

# df_minutos_medio_turno

# COMMAND ----------

df_dmdo = df_dmdo.merge(df_minutos_medio_turno, how = 'left', on=['FILIAL'])

# COMMAND ----------

df_dmdo['PERNAS/MIN NO TURNO 202104'] = round(df_dmdo['NUM PERNAS DIA 202104']/df_dmdo['MEDIA_MIN_QL_ATIVO'])
df_dmdo['PERNAS/MIN NO TURNO 202105'] = round(df_dmdo['NUM PERNAS DIA 202105']/df_dmdo['MEDIA_MIN_QL_ATIVO'])
df_dmdo['PERNAS/MIN NO TURNO 202106'] = round(df_dmdo['NUM PERNAS DIA 202106']/df_dmdo['MEDIA_MIN_QL_ATIVO'])

# COMMAND ----------

df_dmdo = df_dmdo.merge(df_produt_real_hist[['CD_FILIAL','Atividade','PROD_REAL_PERNAS_MINUTO_HIST']], how = 'left', left_on = ['FILIAL','Atividade'], right_on = ['CD_FILIAL','Atividade'])

df_dmdo.drop(columns=['CD_FILIAL'], inplace = True)

df_dmdo = df_dmdo[df_dmdo['Atividade'] != 'Dessosar e Revisar']

# df_dmdo.head()

# COMMAND ----------

# MAGIC %md
# MAGIC #### Turnover

# COMMAND ----------

df_turnover = pd.read_excel('gs://resultados_dmdo/Turnover/Turnover.xlsx', sheet_name = 'Resultados Turnover')

# Mantenha somente os últimos meses
df_turnover = df_turnover[df_turnover['MES REF'].isin(['01-2021', '02-2021', '03-2021'])]

# df_turnover.head()

# COMMAND ----------

df_turnover = df_turnover.groupby(['Filial Contábil','Atividade']).agg({
    'TURNOVER': 'mean'
}).reset_index().round(2)

df_turnover['Filial Contábil'] = df_turnover['Filial Contábil'].astype(str)

df_turnover = df_turnover.fillna(0.00)

# COMMAND ----------

df_dmdo = df_dmdo.merge(df_turnover, how = 'left', left_on=['FILIAL','Atividade'], right_on=['Filial Contábil','Atividade'])

df_dmdo.drop(columns=['Filial Contábil'], inplace = True)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Absenteísmo

# COMMAND ----------

df_absent = params_fixos_absenteismo[['CD_FILIAL','MES REF','Fator_absent']]

df_absent = df_absent[df_absent['MES REF'].isin(['2/2021','3/2021'])].groupby(['CD_FILIAL']).agg({
        'Fator_absent': 'mean'
}).reset_index()

df_absent['CD_FILIAL'] = df_absent['CD_FILIAL'].astype(str)

# COMMAND ----------

df_dmdo = df_dmdo.merge(df_absent, how = 'left', left_on='FILIAL', right_on='CD_FILIAL')

df_dmdo.drop(columns=['CD_FILIAL'], inplace = True)

# COMMAND ----------

mes_ref

# COMMAND ----------

df_ql_ativo_aux = df_resumo_produt_ql_invertido.groupby(['CD_FILIAL','MES REF','DATA','Atividade']).agg({
    'QL_ATIVO': 'sum',
}).reset_index()

df_ql_ativo_aux = df_ql_ativo_aux[(df_ql_ativo_aux['MES REF'] == mes_ref)&(df_ql_ativo_aux['Atividade'] != 'Dessosar e Revisar')]

df_ql_ativo_aux = df_ql_ativo_aux.groupby(['CD_FILIAL','Atividade']).agg({
    'QL_ATIVO': 'mean',
}).reset_index().round(1)

df_ql_ativo_aux['CD_FILIAL'] = df_ql_ativo_aux['CD_FILIAL'].astype(str)

df_ql_ativo_aux.rename(columns={'QL_ATIVO': f'QL_ATIVO {mes_ref}'}, inplace = True)

# df_ql_ativo_aux.head()

# COMMAND ----------

df_dmdo = df_dmdo.merge(df_ql_ativo_aux, how = 'left', left_on = ['FILIAL','Atividade'], right_on = ['CD_FILIAL','Atividade'])

df_dmdo.drop(columns = ['CD_FILIAL'], inplace = True)

# COMMAND ----------

df_hist_capac_nominal = df_resumo_produt_ql_invertido.groupby(['CD_FILIAL','MES REF','Atividade']).agg({
    'CAPACIDADE_NOMINAL': 'mean',
}).reset_index().round(2)

df_hist_capac_nominal = df_hist_capac_nominal[df_hist_capac_nominal['MES REF']== mes_ref]

df_hist_capac_nominal.drop(columns=['MES REF'], inplace = True)

df_hist_capac_nominal['CD_FILIAL'] = df_hist_capac_nominal['CD_FILIAL'].astype(str)

df_hist_capac_nominal.rename(columns={'CAPACIDADE_NOMINAL': f'CAPACIDADE_NOMINAL {mes_ref}',
                                      'CD_FILIAL': 'FILIAL'
                                     }, inplace = True)

#df_hist_capac_nominal = df_hist_capac_nominal[df_hist_capac_nominal['Atividade'].isin(['Desossar Perna','Revisar Perna'])]

# df_hist_capac_nominal.head()

# COMMAND ----------

df_dmdo = df_dmdo.merge(df_hist_capac_nominal, how = 'left', left_on = ['FILIAL','Atividade'], right_on = ['FILIAL','Atividade'])

# COMMAND ----------

df_dmdo.rename(columns={'FILIAL':'CD_FILIAL'}, inplace = True)

df_dmdo['Produtiv. para fazer o plano 202104'] = round(df_dmdo['PERNAS/MIN NO TURNO 202104']/df_dmdo[f'QL_ATIVO {mes_ref}'],2)
df_dmdo['Produtiv. para fazer o plano 202105'] = round(df_dmdo['PERNAS/MIN NO TURNO 202105']/df_dmdo[f'QL_ATIVO {mes_ref}'],2)
df_dmdo['Produtiv. para fazer o plano 202106'] = round(df_dmdo['PERNAS/MIN NO TURNO 202106']/df_dmdo[f'QL_ATIVO {mes_ref}'],2)

# df_dmdo.head()

# COMMAND ----------

df_dmdo['QL DIMENSIONADO - PRODUTIV.REAL 202104'] = (df_dmdo['PERNAS/MIN NO TURNO 202104']/(df_dmdo['PROD_REAL_PERNAS_MINUTO_HIST']*(1 - df_dmdo['TURNOVER'] - df_dmdo['Fator_absent']))).apply(lambda x: math.modf(x)[1] + (math.modf(x)[0] > 0) if not pd.isnull(x) else x)
df_dmdo['QL DIMENSIONADO - PRODUTIV.REAL 202105'] = (df_dmdo['PERNAS/MIN NO TURNO 202105']/(df_dmdo['PROD_REAL_PERNAS_MINUTO_HIST']*(1 - df_dmdo['TURNOVER'] - df_dmdo['Fator_absent']))).apply(lambda x: math.modf(x)[1] + (math.modf(x)[0] > 0) if not pd.isnull(x) else x)
df_dmdo['QL DIMENSIONADO - PRODUTIV.REAL 202106'] = (df_dmdo['PERNAS/MIN NO TURNO 202106']/(df_dmdo['PROD_REAL_PERNAS_MINUTO_HIST']*(1 - df_dmdo['TURNOVER'] - df_dmdo['Fator_absent']))).apply(lambda x: math.modf(x)[1] + (math.modf(x)[0] > 0) if not pd.isnull(x) else x)

df_dmdo['QL DIMENSIONADO - CAP.NOMINAL 202104'] = (df_dmdo['PERNAS/MIN NO TURNO 202104']/(df_dmdo['CAPACIDADE_NOMINAL 3/2021']*(1 - df_dmdo['TURNOVER'] - df_dmdo['Fator_absent']))).apply(lambda x: math.modf(x)[1] + (math.modf(x)[0] > 0) if not pd.isnull(x) else x)
df_dmdo['QL DIMENSIONADO - CAP.NOMINAL 202105'] = (df_dmdo['PERNAS/MIN NO TURNO 202105']/(df_dmdo['CAPACIDADE_NOMINAL 3/2021']*(1 - df_dmdo['TURNOVER'] - df_dmdo['Fator_absent']))).apply(lambda x: math.modf(x)[1] + (math.modf(x)[0] > 0) if not pd.isnull(x) else x)
df_dmdo['QL DIMENSIONADO - CAP.NOMINAL 202106'] = (df_dmdo['PERNAS/MIN NO TURNO 202106']/(df_dmdo['CAPACIDADE_NOMINAL 3/2021']*(1 - df_dmdo['TURNOVER'] - df_dmdo['Fator_absent']))).apply(lambda x: math.modf(x)[1] + (math.modf(x)[0] > 0) if not pd.isnull(x) else x)

de_para_nome_filiais  = df_resumo_produt_ql_invertido[['CD_FILIAL','FILIAL']].drop_duplicates()
de_para_nome_filiais['CD_FILIAL'] = de_para_nome_filiais['CD_FILIAL'].astype(str)
df_dmdo = df_dmdo.merge(de_para_nome_filiais, how = 'left', on = 'CD_FILIAL')

df_dmdo = df_dmdo[[
    'CD_FILIAL','FILIAL','MÉDIA PROD REAL NO DIA 3/2021','VOL DIA 202104', 'VOL DIA 202105',
    'VOL DIA 202106', 'Atividade','PROD_REAL_PERNAS_MINUTO_HIST',
    'CAPACIDADE_NOMINAL 3/2021',
    'Produtiv. para fazer o plano 202104',
    'Produtiv. para fazer o plano 202105',
    'Produtiv. para fazer o plano 202106',
    'TURNOVER',
    'Fator_absent',
    'MEDIA_MIN_QL_ATIVO',
    'PESO MÉDIO (g)',
    'PERNAS/MIN NO TURNO 202104',
    'PERNAS/MIN NO TURNO 202105', 'PERNAS/MIN NO TURNO 202106',
    f'QL_ATIVO {mes_ref}',
    'QL DIMENSIONADO - PRODUTIV.REAL 202104',
    'QL DIMENSIONADO - PRODUTIV.REAL 202105',
    'QL DIMENSIONADO - PRODUTIV.REAL 202106',
    'QL DIMENSIONADO - CAP.NOMINAL 202104',
    'QL DIMENSIONADO - CAP.NOMINAL 202105',
    'QL DIMENSIONADO - CAP.NOMINAL 202106'
]]

df_dmdo['DIF QL 202104'] = df_dmdo[f'QL_ATIVO {mes_ref}'] - df_dmdo['QL DIMENSIONADO - CAP.NOMINAL 202104']
df_dmdo['DIF QL 202105'] = df_dmdo[f'QL_ATIVO {mes_ref}'] - df_dmdo['QL DIMENSIONADO - CAP.NOMINAL 202105']
df_dmdo['DIF QL 202106'] = df_dmdo[f'QL_ATIVO {mes_ref}'] - df_dmdo['QL DIMENSIONADO - CAP.NOMINAL 202106']

# COMMAND ----------

df_resumo_produt_ql_invertido.rename(columns={
    'PROD_REAL_Toneladas': 'Produção real de perna desossada (Tons)',
    'PRODUCAO_POSSIVEL_TONS_DIA': 'Produção possível de perna desossada (Tons)',
    'PROD.REAL PERNAS/MIN/COLABORADOR': 'Produtividade (Pernas/Min)',
    'CAPACIDADE_NOMINAL': 'Capacidade Nominal (Pernas/Min)',
    'DIF QL': 'DIF QL (Ativo - Invertido)'
}, inplace = True)

# COMMAND ----------

df_anomalias = df_resumo_produt_ql_invertido[df_resumo_produt_ql_invertido['Anomalia'] == 'Sim']

df_anomalias = df_anomalias[[
    'CD_FILIAL', 'FILIAL', 'DATA', 'MES REF', 'TURNO', 'Atividade',
       'Produção real de perna desossada (Tons)', 'Produção possível de perna desossada (Tons)','QL_ATIVO', 'QL_INVERTIDO',
       'DIF QL (Ativo - Invertido)', 'Capacidade Nominal (Pernas/Min)',
       'Produtividade (Pernas/Min)',
        'Anomalia'
]]

# df_anomalias.head()

# COMMAND ----------

df_resumo_producao = df_resumo_produt_ql_invertido[df_resumo_produt_ql_invertido['Atividade']=='Desossar Perna'][[
    'CD_FILIAL','FILIAL', 'DATA', 'MES REF', 'TURNO',
       'Produção real de perna desossada (Tons)', 'Produção possível de perna desossada (Tons)'
]]

# COMMAND ----------

df_resumo_produt_ql_invertido = df_resumo_produt_ql_invertido[[
    'CD_FILIAL', 'FILIAL', 'DATA', 'MES REF', 'TURNO', 'Atividade',
        'QL_ATIVO', 'QL_INVERTIDO',
       'DIF QL (Ativo - Invertido)', 'Capacidade Nominal (Pernas/Min)',
       'Produtividade (Pernas/Min)','Anomalia'  
]]

# COMMAND ----------

df_dmdo.rename(columns={
    'MÉDIA PROD REAL NO DIA 3/2021': 'Produção real de perna desossada por dia (3/2021)',
    'VOL DIA 202104': 'Produção por dia perna desossada plano 202104',
    'VOL DIA 202105': 'Produção por dia perna desossada plano 202105',
    'VOL DIA 202106': 'Produção por dia perna desossada plano 202106',
    'PROD_REAL_PERNAS_MINUTO_HIST': f'Produtividade (Pernas/Min) ultimo mes',
    'CAPACIDADE_NOMINAL 3/2021': 'Capacidade Nominal ultimo mes'
}, inplace = True)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Carrega bases no BQ

# COMMAND ----------

dataset = bq.dataset.Dataset("dmdo-gastos-305013.painel_dmdo")

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Absenteismo

# COMMAND ----------

table_id = "absenteismo"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    # Optionally, set the write disposition. BigQuery appends loaded rows
    # to an existing table by default, but with WRITE_TRUNCATE write
    # disposition it replaces the table with the loaded data.
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_absent, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Turnover

# COMMAND ----------

df_turnover.columns = [x.replace('í','i').replace('ç','c').replace('á','a').replace('ã','a').replace(" - ","_").replace(" ","_").replace("(","").replace(")","").replace("/","_") for x in df_turnover.columns]

# COMMAND ----------

table_id = "turnover"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    # Optionally, set the write disposition. BigQuery appends loaded rows
    # to an existing table by default, but with WRITE_TRUNCATE write
    # disposition it replaces the table with the loaded data.
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_turnover, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Resumo Produtividade e QL Inv

# COMMAND ----------

df_resumo_produt_ql_invertido.columns = [x.replace(" - ","_").replace(" ","_").replace("(","").replace(")","").replace("/","_") for x in df_resumo_produt_ql_invertido.columns]

# COMMAND ----------

df_resumo_produt_ql_invertido_aux = df_resumo_produt_ql_invertido.copy()

df_resumo_produt_ql_invertido_aux['POND_Capacidade_Nominal_Pernas_Min'] = df_resumo_produt_ql_invertido_aux['Capacidade_Nominal_Pernas_Min']*df_resumo_produt_ql_invertido_aux['QL_ATIVO']

df_resumo_produt_ql_invertido_aux['POND_Produtividade_Pernas_Min'] = df_resumo_produt_ql_invertido_aux['Produtividade_Pernas_Min']*df_resumo_produt_ql_invertido_aux['QL_ATIVO']

df_resumo_produt_ql_invertido_aux = df_resumo_produt_ql_invertido_aux.groupby([
    'CD_FILIAL', 'FILIAL', 'DATA', 'MES_REF', 'Atividade','Anomalia'
]).agg({
    'QL_ATIVO': 'sum',
    'QL_INVERTIDO': 'sum',
    'DIF_QL_Ativo_Invertido': 'sum',
    'POND_Capacidade_Nominal_Pernas_Min': 'sum',
    'POND_Produtividade_Pernas_Min': 'sum'
}).reset_index()

df_resumo_produt_ql_invertido_aux['Capacidade_Nominal_Pernas_Min'] = df_resumo_produt_ql_invertido_aux['POND_Capacidade_Nominal_Pernas_Min']/df_resumo_produt_ql_invertido_aux['QL_ATIVO']
df_resumo_produt_ql_invertido_aux['Produtividade_Pernas_Min'] = df_resumo_produt_ql_invertido_aux['POND_Produtividade_Pernas_Min']/df_resumo_produt_ql_invertido_aux['QL_ATIVO']

df_resumo_produt_ql_invertido_aux.drop(columns=['POND_Capacidade_Nominal_Pernas_Min','POND_Produtividade_Pernas_Min'], inplace = True)

df_resumo_produt_ql_invertido_aux['TURNO'] = 4

# COMMAND ----------

df_resumo_produt_ql_invertido = pd.concat([df_resumo_produt_ql_invertido, df_resumo_produt_ql_invertido_aux], ignore_index = True)

df_resumo_produt_ql_invertido['TURNO'] = df_resumo_produt_ql_invertido['TURNO'].astype(str)

# COMMAND ----------

df_resumo_produt_ql_invertido_aux = df_resumo_produt_ql_invertido.merge(diretoria_adjunta[['CD_FILIAL','NomeDiretoriaAdjunta']], how = 'left', on = 'CD_FILIAL')

df_resumo_produt_ql_invertido_aux['POND_Capacidade_Nominal_Pernas_Min'] = df_resumo_produt_ql_invertido_aux['Capacidade_Nominal_Pernas_Min']*df_resumo_produt_ql_invertido_aux['QL_ATIVO']
df_resumo_produt_ql_invertido_aux['POND_Produtividade_Pernas_Min'] = df_resumo_produt_ql_invertido_aux['Produtividade_Pernas_Min']*df_resumo_produt_ql_invertido_aux['QL_ATIVO']

df_resumo_produt_ql_invertido_aux = df_resumo_produt_ql_invertido_aux.groupby([
    'NomeDiretoriaAdjunta',
    'DATA',
    'MES_REF',
    'TURNO',
    'Atividade',
    'Anomalia'
]).agg({
    'QL_ATIVO': 'sum',
    'QL_INVERTIDO': 'sum',
    'DIF_QL_Ativo_Invertido': 'sum',
    'POND_Capacidade_Nominal_Pernas_Min': 'sum',
    'POND_Produtividade_Pernas_Min': 'sum'
}).reset_index()

df_resumo_produt_ql_invertido_aux['Capacidade_Nominal_Pernas_Min'] = df_resumo_produt_ql_invertido_aux['POND_Capacidade_Nominal_Pernas_Min']/df_resumo_produt_ql_invertido_aux['QL_ATIVO']
df_resumo_produt_ql_invertido_aux['Produtividade_Pernas_Min'] = df_resumo_produt_ql_invertido_aux['POND_Produtividade_Pernas_Min']/df_resumo_produt_ql_invertido_aux['QL_ATIVO']

df_resumo_produt_ql_invertido_aux.rename(columns={'NomeDiretoriaAdjunta': 'FILIAL'}, inplace = True)

df_resumo_produt_ql_invertido_aux.drop(columns=['POND_Capacidade_Nominal_Pernas_Min','POND_Produtividade_Pernas_Min'], inplace = True)



# COMMAND ----------

df_resumo_produt_ql_invertido_aux2 = df_resumo_produt_ql_invertido.copy()

df_resumo_produt_ql_invertido_aux2['POND_Capacidade_Nominal_Pernas_Min'] = df_resumo_produt_ql_invertido_aux2['Capacidade_Nominal_Pernas_Min']*df_resumo_produt_ql_invertido_aux2['QL_ATIVO']
df_resumo_produt_ql_invertido_aux2['POND_Produtividade_Pernas_Min'] = df_resumo_produt_ql_invertido_aux2['Produtividade_Pernas_Min']*df_resumo_produt_ql_invertido_aux2['QL_ATIVO']

df_resumo_produt_ql_invertido_aux2 = df_resumo_produt_ql_invertido_aux2.groupby([
    'DATA',
    'MES_REF',
    'TURNO',
    'Atividade',
    'Anomalia'
]).agg({
    'QL_ATIVO': 'sum',
    'QL_INVERTIDO': 'sum',
    'DIF_QL_Ativo_Invertido': 'sum',
    'POND_Capacidade_Nominal_Pernas_Min': 'sum',
    'POND_Produtividade_Pernas_Min': 'sum'
}).reset_index()

df_resumo_produt_ql_invertido_aux2['FILIAL'] = 'TOTAL'

df_resumo_produt_ql_invertido_aux2['Capacidade_Nominal_Pernas_Min'] = df_resumo_produt_ql_invertido_aux2['POND_Capacidade_Nominal_Pernas_Min']/df_resumo_produt_ql_invertido_aux2['QL_ATIVO']
df_resumo_produt_ql_invertido_aux2['Produtividade_Pernas_Min'] = df_resumo_produt_ql_invertido_aux2['POND_Produtividade_Pernas_Min']/df_resumo_produt_ql_invertido_aux2['QL_ATIVO']

df_resumo_produt_ql_invertido_aux2.drop(columns=['POND_Capacidade_Nominal_Pernas_Min','POND_Produtividade_Pernas_Min'], inplace = True)

# COMMAND ----------

df_resumo_produt_ql_invertido = pd.concat([df_resumo_produt_ql_invertido, df_resumo_produt_ql_invertido_aux, df_resumo_produt_ql_invertido_aux2], ignore_index = True)

# COMMAND ----------

dict_codigos_regionais = {
    'AVES PESADAS - C. OESTE, BA E MG': 10001,
    'AVES PESADAS - PR': 10002,
    'AVES PESADAS - RS E SC': 10003, 
    'AVES PESADAS - SP': 10004,
    'TOTAL': 10005,
}

# COMMAND ----------

for nome in ['AVES PESADAS - C. OESTE, BA E MG', 'AVES PESADAS - PR','AVES PESADAS - RS E SC','AVES PESADAS - SP','TOTAL']:
    
    df_resumo_produt_ql_invertido.loc[df_resumo_produt_ql_invertido['FILIAL'] == nome, 'CD_FILIAL'] = dict_codigos_regionais[nome]

# COMMAND ----------

table_id = "resumo_produt_ql_invertido"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    # Optionally, set the write disposition. BigQuery appends loaded rows
    # to an existing table by default, but with WRITE_TRUNCATE write
    # disposition it replaces the table with the loaded data.
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_resumo_produt_ql_invertido, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Produção Real vs Possível

# COMMAND ----------

df_resumo_producao.columns = [x.replace('í','i').replace('ç','c').replace('ã','a').replace(" - ","_").replace(" ","_").replace("(","").replace(")","").replace("/","_") for x in df_resumo_producao.columns]

# COMMAND ----------

df_resumo_producao_aux = df_resumo_producao.copy()

df_resumo_producao_aux = df_resumo_producao_aux.groupby([
    'CD_FILIAL', 'FILIAL', 'DATA', 'MES_REF',
]).agg({
    'Producao_real_de_perna_desossada_Tons': 'sum',
    'Producao_possivel_de_perna_desossada_Tons': 'sum'
}).reset_index()

df_resumo_producao_aux['TURNO'] = 4

# COMMAND ----------

df_resumo_producao = pd.concat([df_resumo_producao, df_resumo_producao_aux], ignore_index = True)

df_resumo_producao['TURNO'] = df_resumo_producao['TURNO'].astype(str)

# COMMAND ----------

df_resumo_producao_aux = df_resumo_producao.merge(diretoria_adjunta[['CD_FILIAL','NomeDiretoriaAdjunta']], how = 'left', on = 'CD_FILIAL')

df_resumo_producao_aux = df_resumo_producao_aux.groupby([
    'NomeDiretoriaAdjunta', 
    'MES_REF',
    'DATA',
    'TURNO',
    ]).agg({
    'Producao_real_de_perna_desossada_Tons': 'sum',
    'Producao_possivel_de_perna_desossada_Tons': 'sum'
}).reset_index()

df_resumo_producao_aux.rename(columns={'NomeDiretoriaAdjunta': 'FILIAL'}, inplace = True)

df_resumo_producao = pd.concat([df_resumo_producao, df_resumo_producao_aux], ignore_index = True)

# COMMAND ----------

df_resumo_producao_aux = df_resumo_producao[~df_resumo_producao['CD_FILIAL'].isna()]

df_resumo_producao_aux = df_resumo_producao_aux.groupby([
    'DATA',
    'MES_REF', 
    'TURNO',
    ]).agg({
    'Producao_real_de_perna_desossada_Tons': 'sum',
    'Producao_possivel_de_perna_desossada_Tons': 'sum'
}).reset_index()

df_resumo_producao_aux['FILIAL'] = 'TOTAL'

df_resumo_producao = pd.concat([df_resumo_producao, df_resumo_producao_aux], ignore_index = True)

# COMMAND ----------

for nome in ['AVES PESADAS - C. OESTE, BA E MG', 'AVES PESADAS - PR','AVES PESADAS - RS E SC','AVES PESADAS - SP','TOTAL']:
    
    df_resumo_producao.loc[df_resumo_producao['FILIAL'] == nome, 'CD_FILIAL'] = dict_codigos_regionais[nome]

# COMMAND ----------

table_id = "resumo_producao_possivel_e_real"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    # Optionally, set the write disposition. BigQuery appends loaded rows
    # to an existing table by default, but with WRITE_TRUNCATE write
    # disposition it replaces the table with the loaded data.
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_resumo_producao, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

df_resumo_producao['DATA'].max()

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Analítico dimensionamento QL

# COMMAND ----------

df_dmdo.columns = [x.replace('É','E').replace('.','').replace('í','i').replace('ú','u').replace('ç','c').replace('ã','a').replace(" - ","_").replace(" ","_").replace("(","").replace(")","").replace("/","_") for x in df_dmdo.columns]

# COMMAND ----------

table_id = "analitico_dimensionamento"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    # Optionally, set the write disposition. BigQuery appends loaded rows
    # to an existing table by default, but with WRITE_TRUNCATE write
    # disposition it replaces the table with the loaded data.
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_dmdo, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Filial

# COMMAND ----------

filial = df_resumo_producao[['CD_FILIAL','FILIAL']].drop_duplicates()

siglas['CD_FILIAL'] = siglas['CD_FILIAL'].astype(int)

filial['CD_FILIAL'] = filial['CD_FILIAL'].astype(int)

filial = filial.merge(siglas, how = 'left', on = 'CD_FILIAL')

dict_siglas_regionais = {
    'AVES PESADAS - C. OESTE, BA E MG': 'TOTAL CO,BA,MG',
    'AVES PESADAS - PR': 'TOTAL PR',
    'AVES PESADAS - RS E SC': 'TOTAL RS,SC', 
    'AVES PESADAS - SP': 'TOTAL SP',
    'TOTAL': 'TOTAL',
}

for nome in ['AVES PESADAS - C. OESTE, BA E MG', 'AVES PESADAS - PR','AVES PESADAS - RS E SC','AVES PESADAS - SP','TOTAL']:
    
    filial.loc[filial['FILIAL'] == nome, 'SIGLA'] = dict_siglas_regionais[nome]
    
df_resumo_producao

filial['CD_SIGLA_FILIAL'] = filial['CD_FILIAL'].astype(str)+' - '+filial['SIGLA']

# COMMAND ----------

table_id = "filial"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    # Optionally, set the write disposition. BigQuery appends loaded rows
    # to an existing table by default, but with WRITE_TRUNCATE write
    # disposition it replaces the table with the loaded data.
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(filial, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Experiência trabalhadores

# COMMAND ----------

capacidade_nominal = df_capacidade_nominal.copy()

# COMMAND ----------

capacidade_nominal.columns = [x.replace("º","").replace(" - ","_").replace(" ","_").replace("(","").replace(")","").replace("/","_") for x in capacidade_nominal.columns]

# COMMAND ----------

capacidade_nominal = capacidade_nominal[capacidade_nominal['CODEMPRESA'].isin(filial['CD_FILIAL'].unique())]

capacidade_nominal = capacidade_nominal.groupby(['MES_REF','CODEMPRESA','EMPRESA','Atividade','TURNO','Capacidade_Nominal_Pernas_min','Tempo_de_Empresa_Meses']).agg({
    'No_Colaboradores': 'mean'
}).reset_index()

capacidade_nominal.rename(columns={"No_Colaboradores":"Numero_medio_colaboradores"}, inplace = True)

capacidade_nominal.sort_values(by=['CODEMPRESA', 'MES_REF', 'TURNO', 'Atividade', 'Tempo_de_Empresa_Meses'], inplace = True)

capacidade_nominal['Numero_medio_colaboradores'] = capacidade_nominal['Numero_medio_colaboradores'].apply(lambda x: round(x,1))

# capacidade_nominal.head(10)

# COMMAND ----------

capacidade_nominal_aux = capacidade_nominal.copy()

capacidade_nominal_aux['POND_Capacidade_Nominal_Pernas_min'] = capacidade_nominal_aux['Capacidade_Nominal_Pernas_min']*capacidade_nominal_aux['Numero_medio_colaboradores']

capacidade_nominal_aux = capacidade_nominal_aux.groupby([
    'MES_REF', 'CODEMPRESA', 'EMPRESA', 'Atividade','Tempo_de_Empresa_Meses'
]).agg({
    'Numero_medio_colaboradores': 'sum',
    'POND_Capacidade_Nominal_Pernas_min': 'sum',
}).reset_index()

capacidade_nominal_aux['Capacidade_Nominal_Pernas_min'] = capacidade_nominal_aux['POND_Capacidade_Nominal_Pernas_min']/capacidade_nominal_aux['Numero_medio_colaboradores']

capacidade_nominal_aux.drop(columns=['POND_Capacidade_Nominal_Pernas_min'], inplace = True)

capacidade_nominal_aux['TURNO'] = 4

# COMMAND ----------

capacidade_nominal = pd.concat([capacidade_nominal, capacidade_nominal_aux], ignore_index = True)

capacidade_nominal['TURNO'] = capacidade_nominal['TURNO'].astype(str)

# COMMAND ----------

table_id = "experiencia_trabalhadores"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    # Optionally, set the write disposition. BigQuery appends loaded rows
    # to an existing table by default, but with WRITE_TRUNCATE write
    # disposition it replaces the table with the loaded data.
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(capacidade_nominal, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Turno

# COMMAND ----------

turno = pd.DataFrame(columns=['TURNO','Cd_turno','DescTurno'])

turno.loc[0] = [1,1,'1º Turno']
turno.loc[1] = [2,2,'2º Turno']
turno.loc[2] = [3,3,'3º Turno']
turno.loc[3] = [4,4,'Todos os turnos']

turno['TURNO'] = turno['TURNO'].astype(int)
turno['Cd_turno'] = turno['Cd_turno'].astype(int)
turno['DescTurno'] = turno['DescTurno'].astype(str)

# COMMAND ----------

table_id = "turno"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    # Optionally, set the write disposition. BigQuery appends loaded rows
    # to an existing table by default, but with WRITE_TRUNCATE write
    # disposition it replaces the table with the loaded data.
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(turno, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Atividade

# COMMAND ----------

atividade = pd.DataFrame(columns=['Atividade', 'CdAtividade'])

atividade.loc[0] = ['Desossar Perna', 1]
atividade.loc[1] = ['Revisar Perna', 2]
atividade.loc[2] = ['Desossar e Revisar', 3]

# COMMAND ----------

table_id = "atividade"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    # Optionally, set the write disposition. BigQuery appends loaded rows
    # to an existing table by default, but with WRITE_TRUNCATE write
    # disposition it replaces the table with the loaded data.
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(atividade, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Anomalias

# COMMAND ----------

df_anomalias.columns = [x.replace('É','E').replace('.','').replace('í','i').replace('ú','u').replace('ç','c').replace('ã','a').replace(" - ","_").replace(" ","_").replace("(","").replace(")","").replace("/","_") for x in df_anomalias.columns]

# COMMAND ----------

table_id = "anomalias"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    # Optionally, set the write disposition. BigQuery appends loaded rows
    # to an existing table by default, but with WRITE_TRUNCATE write
    # disposition it replaces the table with the loaded data.
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_anomalias, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# MAGIC %md
# MAGIC ### <span style="color: red;">Consulta</span>

# COMMAND ----------

# consulta2 = df_resumo_producao.groupby(['CD_FILIAL','FILIAL','MES_REF','TURNO']).agg({
#     'Producao_real_de_perna_desossada_Tons': 'mean',
#     'Producao_possivel_de_perna_desossada_Tons': 'mean'
# }).reset_index()
