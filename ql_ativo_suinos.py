# Databricks notebook source
from pyspark.sql.functions import when, col, round, floor, trim, regexp_replace, lit, current_date, to_timestamp, create_map, split 
from pyspark.sql.types import DoubleType, IntegerType, StringType
from datetime import date,  timedelta
from pyspark.sql.functions import unix_timestamp, from_unixtime
from pyspark.sql.functions import to_date
from pyspark.sql.functions import udf
import pyspark.sql.functions as F
from itertools import chain
from pyspark.sql import SparkSession
import unicodedata

from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
sc = SparkContext.getOrCreate()
spark = SparkSession(sc)

# COMMAND ----------

df_hierarquia = spark.read.format('parquet').load('gs://seara-us-dev-absent-data/base_cad_funcionarios')

# COMMAND ----------

# df_hierarquia.printSchema()

# COMMAND ----------

# df_hierarquia.filter('CPF = 97333336549')[['CODIGOEMPRESA','CCUSTO','SECAO','SITUACAO','DATAADMISSAO','DATADEMISSAO', 'CODPESSOA', 'CHAPA_CAD','CPF','NOME', 'RANK','DATA_CAD']].orderBy(['CHAPA_CAD','RANK']).display()

# COMMAND ----------

#df_hierarquia_aux = df_hierarquia.groupBy(['CODIGOEMPRESA','CPF','CHAPA_CAD']).agg({'RANK': 'max'}).withColumnRenamed('max(RANK)', 'RANK')

# COMMAND ----------

# df_hierarquia = df_hierarquia.join(
#   df_hierarquia_aux,
#   how = 'inner',
#   on = ['CODIGOEMPRESA','CPF','CHAPA_CAD', 'RANK']
# )

# COMMAND ----------

df_hierarquia_aux = df_hierarquia.groupBy(['CPF']).agg({'DATA_CAD': 'max'}).withColumnRenamed('max(DATA_CAD)', 'DATA_CAD')

# COMMAND ----------

df_hierarquia = df_hierarquia.join(
  df_hierarquia_aux,
  how = 'inner',
  on = ['CPF','DATA_CAD']
)

# COMMAND ----------

df_hierarquia = df_hierarquia.filter(df_hierarquia['SITUACAO'].isin(['Ativo','Férias']))

# COMMAND ----------

# df_hierarquia.filter('CPF = 97333336549')[['CODIGOEMPRESA','CCUSTO','SECAO','SITUACAO','DATAADMISSAO','DATADEMISSAO', 'CODPESSOA', 'CHAPA_CAD','CPF','NOME', 'RANK','DATA_CAD']].orderBy(['CHAPA_CAD','RANK']).display()

# COMMAND ----------

filiais = [
    633,
    902,
    918,
    475,
    136,
    228,
    827,
    581
]

# COMMAND ----------

df_hierarquia = df_hierarquia.filter(
  (df_hierarquia['CODIGOEMPRESA'].isin(filiais))
  &(df_hierarquia['DIRETORIA_EXECUTIVA'] == 'NEG. SUÍNOS')
  &(df_hierarquia['GERENCIA_EXECUTIVA'] == 'OPERAÇÕES - SUÍNOS')
)

# COMMAND ----------

df_hierarquia = df_hierarquia.withColumn('CODIGOEMPRESA', df_hierarquia['CODIGOEMPRESA'].cast("int"))

# COMMAND ----------

df_hierarquia = df_hierarquia.filter(
  ~(df_hierarquia['CCUSTO'].isin([
#         "ABATE DE SUINOS",
        "ADM.PRODUCAO",
        "ADM.VESTIARIOS",
#         "BENEF. DE TRIPAS",
#         "BENEFICIAMENTO/PADRONIZACAO",
#         "BISTECA SUÍNA",
#         "CALIBRAÇÃO DE TRIPAS",
        "CASA DE MAQUINAS",
#         "CORTES ATP",
#         "CORTES ESPECIAIS HANS",
        "ESTACAO TRATAMENTO DAGUA",
#         "FABRICA DE FARINHAS - SUINOS",
        "INSPECAO FEDERAL",
        "ARMAZENAGEM/ EXPEDIÇÃO LOG. SUINOS / IND",
        "ARMAZ/EXPED LOG SUINOS/IND",
        "INSTALACOES TERMICAS",
        "LAGOAS DE DECANTACAO / E.T.E.",
        "LIMPEZA E HIGIENIZACAO",
        "MANUTENCAO GERAL",
        "MATO CASTELHANO",
        "MEIO AMBIENTE",
#         "PADRONIZACAO/EMBALAGEM SUINOS",
        "PCP",
#         "REFINARIA SUINO",
#         "SALA DE CABECA",
#         "SALA DE CORTES SUINOS",
#         "SALGADOS SUINOS",
#         "TEMPERADOS SUI",
#         "TUNEL CONT.CONGELAMENTO SUINOS",
    ]))
)

df_hierarquia = df_hierarquia[[
    'CODIGOEMPRESA',
#     'CHAVE_FATO_HIERARQUIA',
#     'CODCOLIGADA',
#     'CODFILIAL',
    'CODPESSOA',
    'CHAPA_CAD',
    'CODSECAO',
    'SITUACAO',
#     'TIPO',
    'DATAADMISSAO',
    'DTTRANSFERENCIA',
    'DATADEMISSAO',
    'SECAO',
    'DEPARTAMENTO',
    'SECAODESCRICAO',
    'CD_REDUZ_CCUSTO',
    'CCUSTO',
    'FUNCAO',
    'CARGO',
#     'DESCRICAOTIPODEMISSAO',
#     'TIPODEMISSAO',
#     'MOTIVODEMISSAO',
#     'MOTIVODEMISSAODESCRICAO',
#     'CODSITUACAO',
#     'CPFGESTOR',
    'NOME_GESTOR',
#     'DATA_CAD',
#     'DATAROTATIVIDADE',
#     'CODFUNCAO',
#     'INATIVAFUNCAO',
#     'PIS',
#     'CODFILIALCTB',
#     'JORNADAMENSAL',
    'NOME',
    'CPF',
#     'DTNASCIMENTO',
#     'ESTADOCIVIL',
#     'SEXO',
#     'COR',
#     'DEFICIENTEAUDITIVO',
#     'DEFICIENTEFALA',
#     'DEFICIENTEVISUAL',
#     'DEFICIENTEMENTAL',
#     'DEFICIENTEFISICO',
#     'DEFICIENTEINTELECTUAL',
#     'CODESCOLARIDADE',
#     'ESCOLARIDADE',
#     'REABILITADO',
#     'ESTADO',
#     'CIDADE',
#     'ESTADONATAL',
#     'NATURALIDADE',
#     'EMAIL',
#     'NASCIONALIDADE',
#     'EMPRESA_ORIGINAL',
#     'CD_EMPRESA_HIERARQUIA',
#     'CD_FILIAL_HIERARQUIA',
    'DIRETORIA_EXECUTIVA',
    'DIRETORIA_ADJUNTA',
    'GERENCIA_EXECUTIVA',
    'GERENCIA_DE_AREA',
    'COORD_SUPERVISAO',
#     'CD_EMPRESA_CENTRALIZADORA',
#     'EMPRESA',
#     'NM_FILIAL',
#     'FILIAL',
#     'EMPRESAFORPONTO_CAD',
#     'RANK',
#     'DATA_FIM',
#     'PORTEIRAFECHADA' 
]]


# COMMAND ----------

def limpa_string (string):
  
    nfkdStr = unicodedata.normalize('NFKD', string)
    
    withOutAccents = u"".join([c for c in nfkdStr if not unicodedata.combining(c)])
    
    return withOutAccents.replace(" ","").replace("-","_").lower()
  
limpa_string_UDF = udf(lambda x: limpa_string (x), StringType())

# COMMAND ----------

df_hierarquia = df_hierarquia.withColumn('SECAO_TRATADA', limpa_string_UDF(col('SECAO')))

# COMMAND ----------

def de_para_secao_rm_subarea_dmdo (secao):
    
    if "barriga" in secao:
        return "Barriga"
    
    elif 'paleta_atp' in secao:
        return "ATM"
    
    elif ("_camarasresfriamento" in secao)|("_evisceracao" in secao)|("_escaldedepel" in secao):
        return "Escaldagem / Depilagem / Evisceração"
    
    elif "_serrarespinhaco" in secao:
        return "Paleta"
    
    elif ("carre/lombo"in secao)|("_copa"  in secao):
        return "Carré / Filé / Copa"
    
    elif "_embalagem"  in secao:
        return "Embalagem"
    
    elif ("ffo"  in secao)|("ffo(subprodutos)" in secao):
        return "Fábrica de Farinhas"
    
    elif "_grandescortes"  in secao:
        return "Grandes Cortes"
    
    elif ("miudos"  in secao)|("saladecabeca" in secao):
        return "Miúdos"
    
    elif "_montagemdecaixas"  in secao:
        return "Montagem de Caixas"
    
    elif "_padron/embal"  in secao:
        return "Paletização"
    
    elif "_paleta_"  in secao:
        return "Paleta"
    
    elif "pernil"  in secao:
        return "Pernil"
    
    elif ("_recebimento"  in secao)|("garantiadaqualidade" in secao):
        return "Recebimento de Suíno"
    
    elif "_triparia"  in secao:
        return "Aproveitamento de Tripa"
    
    elif "_salga"  in secao:
        return "Salgados"
    
    elif "cortesespeciais"  in secao:
        return "Linhas Especiais"
      
    elif "refinaria"  in secao:
        return "Refinaria"
      
    elif "linhafesta"  in secao:
      return "Linha Festa"
    
    else:
        return None
      
de_para_secao_rm_subarea_dmdo_UDF = udf(lambda x: de_para_secao_rm_subarea_dmdo(x), StringType())

# COMMAND ----------

df_hierarquia = df_hierarquia.withColumn('SUBAREA', de_para_secao_rm_subarea_dmdo_UDF(col('SECAO_TRATADA')))

# COMMAND ----------

df_hierarquia = df_hierarquia.withColumnRenamed('CODIGOEMPRESA', 'CD_FILIAL')

# COMMAND ----------

# df_hierarquia.groupBy(['CD_FILIAL']).agg({'CPF': 'count'}).orderBy(['CD_FILIAL']).display()

# COMMAND ----------

df_hierarquia.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'dmdo-gastos-305013.dmdo_dataset.dmdo_ql_ativo').save()
