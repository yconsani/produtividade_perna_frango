# Databricks notebook source
def funcao_query_vol_ton_vivo(data_inicial, data_final):

    query = f"""

    SELECT apn_viagem_seq_abs_abd.dt_producao 							AS DATA
        , viagem_abs_abatedouro.cd_filial 								AS CD_FILIAL
        -- , apn_viagem_seq_abs_abd.cd_viagem_transporte 					AS CD_VIAGEM_TRANSPORTE
        -- , viagem_abs_abatedouro.cd_subgrupo_itm_for 					AS CD_SUBGRUPO_ITEM
        , apn_viagem_seq_abs_abd.cd_turno_producao 					AS CD_TURNO_PRODUCAO
        -- , fazenda.nm_fazenda 											AS NM_FAZENDA
        -- , viagem_abs_abatedouro.id_sexo_lote 							AS ID_SEXO_LOTE
        , round(sum(COALESCE(apn_viagem_seq_abs_abd.qt_abatida,0))/1000 )			AS QT_CABECA
        -- , sum(COALESCE(apn_viagem_seq_abs_abd.qt_mortos_transporte,0)) AS QT_CABECA_MORTA
        , round(sum(COALESCE(apn_viagem_seq_abs_abd.qt_peso_abatido,0))/1000 )		AS QT_PESO
        , ROUND(SAFE_DIVIDE(sum(apn_viagem_seq_abs_abd.qt_peso_abatido), sum(apn_viagem_seq_abs_abd.qt_abatida))*1000) AS PESO_MEDIO
    FROM `seara-analytics-prod.stg_erp_seara.viagem_abs_abatedouro`   viagem_abs_abatedouro
        , `seara-analytics-prod.stg_erp_seara.viagem_seq_abs_abd`      viagem_seq_abs_abd
        , `seara-analytics-prod.stg_erp_seara.apn_viagem_seq_abs_abd`  apn_viagem_seq_abs_abd
        , `seara-analytics-prod.stg_erp_seara.filial_cgc`              filial
        , `seara-analytics-prod.stg_erp_seara.fazenda`                 fazenda
    WHERE viagem_abs_abatedouro.cd_empresa            = viagem_seq_abs_abd.cd_empresa
    AND viagem_abs_abatedouro.cd_filial             = viagem_seq_abs_abd.cd_filial
    AND viagem_abs_abatedouro.cd_viagem_transporte  = viagem_seq_abs_abd.cd_viagem_transporte
    AND viagem_seq_abs_abd.cd_empresa               = apn_viagem_seq_abs_abd.cd_empresa
    AND viagem_seq_abs_abd.cd_filial                = apn_viagem_seq_abs_abd.cd_filial
    AND viagem_seq_abs_abd.cd_seq_abate             = apn_viagem_seq_abs_abd.cd_seq_abate
    AND viagem_seq_abs_abd.cd_viagem_transporte     = apn_viagem_seq_abs_abd.cd_viagem_transporte
    AND viagem_abs_abatedouro.cd_empresa            = filial.cd_empresa
    AND viagem_abs_abatedouro.cd_filial             = filial.cd_filial
    AND viagem_abs_abatedouro.cd_base_fornecedor    = fazenda.cd_base_fornecedor
    AND viagem_abs_abatedouro.cd_estab_fornecedor   = fazenda.cd_estab_fornecedor
    AND viagem_abs_abatedouro.cd_fazenda            = fazenda.cd_fazenda
    AND viagem_abs_abatedouro.id_situacao_abate     =  4
    AND DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', viagem_abs_abatedouro.dt_atualizacao) ) between '{data_inicial}' AND '{data_final}'
    AND viagem_abs_abatedouro.dt_cancelamento_viagem IS NULL
    GROUP BY apn_viagem_seq_abs_abd.dt_producao
            , viagem_abs_abatedouro.cd_filial
            -- , apn_viagem_seq_abs_abd.cd_viagem_transporte
            -- , viagem_abs_abatedouro.cd_subgrupo_itm_for
            , apn_viagem_seq_abs_abd.cd_turno_producao
            -- , fazenda.nm_fazenda
            -- , viagem_abs_abatedouro.id_sexo_lote
    """

    return query
