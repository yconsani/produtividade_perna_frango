# Databricks notebook source
# !pip install unidecode

# COMMAND ----------

import pandas as pd
import numpy as np
import math
import unidecode
import datetime
from datetime import timedelta
from calendar import monthrange
from google.cloud import storage
import unidecode
import math

from google.cloud import bigquery as bq
client = bq.Client()

# COMMAND ----------

def carrega_tabela_bq(id_projeto, nome_dataset, nome_tabela, df, modo = 'WRITE_TRUNCATE'):
  
  table_id = nome_tabela
  
  dataset = bq.dataset.Dataset(f"{id_projeto}.{nome_dataset}")

  table_ref = dataset.table(table_id)

  job_config = bq.LoadJobConfig(
      write_disposition= modo,
  )

  job = client.load_table_from_dataframe(df, table_ref, job_config=job_config)

  job.result()  # Waits for table load to complete.
  
#   df.to_gbq(
#     f'{nome_dataset}.{nome_tabela}',
#     project_id=id_projeto,
#     chunksize=None,
#     if_exists=modo
#   )
  
  return print(f'tabela {nome_tabela} carregada no local {id_projeto}.{nome_dataset}.{nome_tabela}')  #print(f"Loaded dataframe to {table_ref.path}")

# COMMAND ----------

hoje = datetime.datetime.now().date()

data_inicio_mes = datetime.date(hoje.year, hoje.month, 1)

if hoje.month == 12:
    
    data_final_mes = hoje.replace(day=31)
    data_final_proximo_mes = datetime.date(hoje.year + 1, 1, 31)
    
else:
    
    data_final_mes = datetime.date(hoje.year, hoje.month+1, 1) - datetime.timedelta(days=1)
    
    if data_final_mes.month == 11:
    
        data_final_proximo_mes = datetime.date(hoje.year, 12, 31)
        
    else:
    
        data_final_proximo_mes = datetime.date(hoje.year, hoje.month+2, 1) - datetime.timedelta(days=1)

# COMMAND ----------

data_inicio_periodo = datetime.date(2022, 1, 1)

# COMMAND ----------

filiais = [
    633,
    902,
    918,
    475,
    136,
    228,
    827,
    581
]

filiais = str(filiais).replace('[','').replace(']','')

dict_siglas_filiais = {
    633: 'ITS',
    902: 'CBI',
    918: 'TRP',
    475: 'SBI',
    136: 'SEA',
    228: 'DDO',
    827: 'ANR',
    581: 'SMO'
}

# COMMAND ----------

# MAGIC %md
# MAGIC #### Dimensões das filiais

# COMMAND ----------

query = """

WITH A AS (
    SELECT
        CD_FILIAL,
        NM_FILIAL,
        NM_MNEMONICO,
        DT_ATUALIZACAO,
        ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
    FROM
        `seara-analytics-prod.stg_erp_seara.filial_cgc`
)

SELECT 
    CD_FILIAL, 
    NM_FILIAL,
    CONCAT(CD_FILIAL, ' - ', NM_FILIAL) AS FILTRO_FILIAL
FROM A WHERE A.rn = 1


"""

df_filiais = client.query(query).to_dataframe()

df_filiais['SIGLA'] = df_filiais['CD_FILIAL'].apply(lambda x: dict_siglas_filiais.get(x))

# COMMAND ----------

# MAGIC %md
# MAGIC #### De/para família analítica/grupo rendimento

# COMMAND ----------

de_para_fam_analitica_rendimento = pd.read_excel('gs://comparativo_produtividade/inputs_comp_suinos/INPUTS MANUAIS - SUÍNOS.xlsx', sheet_name = "01- CADASTRO DE PARA FAMILIA")

de_para_fam_analitica_rendimento.rename(columns={
    'FAMILIA ANALITICA': 'DS_FAM_ANALITICA_PRO',
    'GRUPO RENDIMENTO': 'GRUPO_RENDIMENTO'
}, inplace = True)

de_para_fam_analitica_rendimento = de_para_fam_analitica_rendimento[['DS_FAM_ANALITICA_PRO', 'GRUPO_RENDIMENTO']]

# COMMAND ----------

# MAGIC %md
# MAGIC #### Dimensões itens 

# COMMAND ----------

query = """
SELECT
    ITEM.CD_ITEM,
    ITEM.NM_ITEM,
    CONCAT(ITEM.CD_ITEM, " - ", ITEM.NM_ITEM) AS FILTRO_ITEM,
    ITEM.DS_ITEM,
    ITEM.CD_CLASSE_ITEM,
    CLASSE_ITEM.NM_CLASSE,
    ITEM.CD_GRUPO_ITEM,
    GRUPO_ITEM.NM_GRUPO,
    ITEM.CD_SUBGRUPO_ITEM,
    SUBGRUPO_ITEM.NM_SUBGRUPO,
    --ITEM.CD_FAMILIA_ITEM,
    ITEM.CD_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
    FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
    FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
    FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO,
    CLASSIFICACAO_CARNE.DS_CLASSIFICACAO,
    ITEM.CD_DESTINO_FINAL,
    REF_CODES.DS_DESTINO_FINAL,
    MERCADO_EXPORTACAO.CD_MERCADO,
    MERCADO_EXPORTACAO.NM_MERCADO,
    ITEM.CD_NEGOCIO,
    NEGOCIO.NM_NEGOCIO
    
FROM
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
    AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
    AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
ON
    ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
ON
    SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
ON
    FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
    --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
ON
    FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
ON
    FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
    AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_item_carne` AS CLASSIFICACAO_ITEM_CARNE
ON
    CLASSIFICACAO_ITEM_CARNE.CD_ITEM = ITEM.CD_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_carne` AS CLASSIFICACAO_CARNE
ON
    CLASSIFICACAO_CARNE.CD_CLASSIFICACAO = CLASSIFICACAO_ITEM_CARNE.CD_CLASSIFICACAO
LEFT JOIN
    (
        SELECT
          CAST(RV_LOW_VALUE AS INT) AS CD_DESTINO_FINAL,
          RV_MEANING AS DS_DESTINO_FINAL
        FROM
          `seara-analytics-prod.stg_erp_seara.bcmg_ref_codes`
        WHERE
          RV_DOMAIN = 'ITEM.CD_DESTINO_FINAL'
    ) AS REF_CODES
ON
    ITEM.CD_DESTINO_FINAL = REF_CODES.CD_DESTINO_FINAL
LEFT JOIN
    `seara-analytics-prod.erp_seara.mercado_exportacao` MERCADO_EXPORTACAO
ON
    ITEM.CD_MERCADO = MERCADO_EXPORTACAO.CD_MERCADO
LEFT JOIN
    `seara-analytics-prod.erp_seara.negocio` NEGOCIO
ON
    ITEM.CD_NEGOCIO = NEGOCIO.CD_NEGOCIO
"""

ITEM = client.query(query).to_dataframe()

# print(ITEM.shape)

# ITEM.head()

# COMMAND ----------

df_itens_dmdo = pd.read_excel('gs://comparativo_produtividade/inputs_comp_suinos/dim_itens_suinos_v6.xlsx')

# print(df_itens_dmdo.shape)

# df_itens_dmdo.head()

# COMMAND ----------

# df_itens_dmdo.groupby(['CD_FILIAL','CD_ITEM']).agg({'CD_ITEM': 'count'}).rename(columns={'CD_ITEM': 'CONTAGEM'}).reset_index().sort_values(by=['CONTAGEM'], ascending = False)

# COMMAND ----------

df_itens_dmdo = df_itens_dmdo.merge(ITEM, how = 'left', on = ['CD_ITEM'])

df_itens_dmdo = df_itens_dmdo.merge(de_para_fam_analitica_rendimento, how = 'left', on = ['DS_FAM_ANALITICA_PRO'])

# COMMAND ----------

# MAGIC %md
# MAGIC #### Plano de produção

# COMMAND ----------

query = f"""

WITH 

TABELA_ITEM AS (
    SELECT
    ITEM.*,
    CLASSE_ITEM.NM_CLASSE,
    SUBCLASSE_PRODUTO.DS_SUBCLASSE_PRODUTO,
    GRUPO_ITEM.NM_GRUPO,
    SUBGRUPO_ITEM.NM_SUBGRUPO,
    --ITEM.CD_FAMILIA_ITEM,
    FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
    FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
    FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
    FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO,
    ACLIMATACAO.NM_TIPO_ACLIMATACAO_PRO,
    CLASSIFICACAO_CARNE.DS_CLASSIFICACAO
FROM
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
    AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
    AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
ON
    ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
ON
    SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
ON
    FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
    --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
ON
    FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
ON
    FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
    AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subclasse_produto` AS SUBCLASSE_PRODUTO
ON 
    ITEM.CD_SUBCLASSE_PRODUTO = SUBCLASSE_PRODUTO.CD_SUBCLASSE_PRODUTO
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.classificacao_item_carne` AS CLASSIFICACAO_ITEM_CARNE
ON
    CLASSIFICACAO_ITEM_CARNE.CD_ITEM = ITEM.CD_ITEM
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.classificacao_carne` AS CLASSIFICACAO_CARNE
ON
    CLASSIFICACAO_CARNE.CD_CLASSIFICACAO = CLASSIFICACAO_ITEM_CARNE.CD_CLASSIFICACAO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.tipo_aclimatacao_produto` AS ACLIMATACAO  
ON
    ITEM.CD_TIPO_ACLIMATACAO = ACLIMATACAO.CD_TIPO_ACLIMATACAO_PRO
)


SELECT
  PLANO.CD_EMPRESA,
  PLANO.CD_FILIAL,
  PLANO.CD_ITEM,
  TABELA_ITEM.NM_ITEM,
  CONCAT(PLANO.CD_ITEM,' - ', TABELA_ITEM.NM_ITEM) AS FILTRO_ITEM,
  CONCAT(CD_FILIAL, ' - ', PLANO.CD_ITEM) AS CHAVE_FILIAL_ITEM,
  COALESCE(PLANO.QT_PESO_ALR, PLANO.QT_PESO) AS QT_PESO_ALTERADO,
  PARSE_DATE('%d/%m/%Y', LEFT(PLANO.DT_PRODUCAO,10)) AS DT_PRODUCAO,
  LEFT(CAST(PARSE_DATE('%d/%m/%Y', LEFT(PLANO.DT_PRODUCAO,10)) AS STRING),7) AS MES_REF,
  TABELA_ITEM.DS_FAMILIA_PRODUTO,
  TABELA_ITEM.NM_GRUPO,
  TABELA_ITEM.NM_SUBGRUPO,
  TABELA_ITEM.NM_CLASSE,
  TABELA_ITEM.DS_SUBCLASSE_PRODUTO,
  TABELA_ITEM.DS_FAM_SINTETICA_PRO,
  TABELA_ITEM.DS_FAM_ANALITICA_PRO,
  TABELA_ITEM.DS_CLASSIFICACAO,
  TABELA_ITEM.CD_UNIDADE_MEDIDA,
  --TABELA_ITEM.CD_UND_MED_COMERCIAL,
  --TABELA_ITEM.CD_UND_MED_TRANSPORTE,
  TABELA_ITEM.NM_TIPO_ACLIMATACAO_PRO,
  --TABELA_ITEM.QT_MINIMO,
  --TABELA_ITEM.QT_MAXIMA,
  --TABELA_ITEM.QT_PESO_PECA_MINIMO,
  --TABELA_ITEM.QT_PESO_PECA_MAXIMO,
  --TABELA_ITEM.VL_VOLUME_ITEM
FROM 
  `seara-analytics-prod.stg_erp_seara.plano_diario_pdc_item_cne` PLANO
LEFT JOIN
  TABELA_ITEM
ON
  PLANO.CD_ITEM = TABELA_ITEM.CD_ITEM
WHERE 
  PARSE_DATE('%d/%m/%Y', LEFT(PLANO.DT_PRODUCAO,10)) BETWEEN '{data_inicio_periodo}' AND '{data_final_proximo_mes}' 
  AND PLANO.CD_FILIAL IN ({filiais})
  AND COALESCE(PLANO.QT_PESO_ALR, PLANO.QT_PESO) > 0
ORDER BY
  PLANO.DT_PRODUCAO
"""

df_plano = client.query(query).to_dataframe()

# print(df_plano.shape)

# df_plano.head()

# COMMAND ----------

df_ds_pacote = df_itens_dmdo[['CD_FILIAL','CD_ITEM', 'FAMILIA_PLANO_PRODUCAO','PESO_PACOTE', 'NUM_PCT_CX', 'PESO_CX_AUT','PESO_CX_MANUAL', 'PESO_COMBO', 'PESO_CESTO_AR', 'PESO_SACO','PESO_BLOCO']]

# COMMAND ----------

df_plano = df_plano.merge(de_para_fam_analitica_rendimento, how = 'left', on = ['DS_FAM_ANALITICA_PRO'])

# COMMAND ----------

df_plano = df_plano.merge(df_ds_pacote, how = 'left', on = ['CD_FILIAL','CD_ITEM'])

df_plano['PCT_DIA'] = round(df_plano['QT_PESO_ALTERADO']/df_plano['PESO_PACOTE'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano['CX_DIA_AUTOMATICA'] = round(df_plano['QT_PESO_ALTERADO']/df_plano['PESO_CX_AUT'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano['CX_DIA_MANUAL'] = round(df_plano['QT_PESO_ALTERADO']/df_plano['PESO_CX_MANUAL'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano['COMBO_DIA'] = round(df_plano['QT_PESO_ALTERADO']/df_plano['PESO_COMBO'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano['CESTO_DIA'] = round(df_plano['QT_PESO_ALTERADO']/df_plano['PESO_CESTO_AR'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano['SACO_DIA'] = round(df_plano['QT_PESO_ALTERADO']/df_plano['PESO_SACO'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano['BLOCO_DIA'] = round(df_plano['QT_PESO_ALTERADO']/df_plano['PESO_BLOCO'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano['CX_DIA'] = df_plano['CX_DIA_AUTOMATICA'].fillna(0) + df_plano['CX_DIA_MANUAL'].fillna(0) + df_plano['COMBO_DIA'].fillna(0) + df_plano['CESTO_DIA'].fillna(0) + df_plano['SACO_DIA'].fillna(0) + df_plano['BLOCO_DIA'].fillna(0)

# COMMAND ----------

# df_plano[
#     (df_plano['CD_FILIAL'] == 918)
#     &(df_plano['CD_ITEM'] == 1953)
# ]

# COMMAND ----------

df_resumo_volumes = df_plano[[
    'CD_EMPRESA', 
    'CD_FILIAL', 
    'MES_REF', 
    'DT_PRODUCAO', 
    'CD_ITEM', 
    'FILTRO_ITEM',
    'DS_FAM_SINTETICA_PRO', 
    'DS_FAM_ANALITICA_PRO', 
    'FAMILIA_PLANO_PRODUCAO', 
    'GRUPO_RENDIMENTO',
    'QT_PESO_ALTERADO',
    'PCT_DIA',
    'CX_DIA',
    'CX_DIA_AUTOMATICA',
    'CX_DIA_MANUAL',
    'COMBO_DIA',
    'CESTO_DIA',
    'SACO_DIA',
    'BLOCO_DIA'
]].copy()

# COMMAND ----------

# tabela de referência agrupada pelas famílias criadas no simulador

df_plano_familia_dmdo = df_plano.groupby([
    'CD_EMPRESA', 
    'CD_FILIAL', 
    'MES_REF', 
    'DT_PRODUCAO', 
    'FAMILIA_PLANO_PRODUCAO']).agg({
        'QT_PESO_ALTERADO': 'sum',
        'PCT_DIA': 'sum',
        'CX_DIA': 'sum',
        'CX_DIA_AUTOMATICA': 'sum',
        'CX_DIA_MANUAL': 'sum',
        'COMBO_DIA': 'sum',
        'CESTO_DIA': 'sum',
        'SACO_DIA': 'sum',
        'BLOCO_DIA': 'sum'
}).reset_index()

# tabela de referência agrupada pela familia sintetica

df_plano_familia_sintetica = df_plano.groupby([
    'CD_EMPRESA', 
    'CD_FILIAL', 
    'MES_REF', 
    'DT_PRODUCAO', 
    'DS_FAM_SINTETICA_PRO']).agg({
        'QT_PESO_ALTERADO': 'sum',
        'PCT_DIA': 'sum',
        'CX_DIA': 'sum',
        'CX_DIA_AUTOMATICA': 'sum',
        'CX_DIA_MANUAL': 'sum',
        'COMBO_DIA': 'sum',
        'CESTO_DIA': 'sum',
        'SACO_DIA': 'sum',
        'BLOCO_DIA': 'sum'
}).reset_index()

# tabela de referência agrupada pela familia sintetica

df_plano_familia_analitica = df_plano.groupby([
    'CD_EMPRESA', 
    'CD_FILIAL', 
    'MES_REF', 
    'DT_PRODUCAO', 
    'DS_FAM_ANALITICA_PRO']).agg({
        'QT_PESO_ALTERADO': 'sum',
        'PCT_DIA': 'sum',
        'CX_DIA': 'sum',
        'CX_DIA_AUTOMATICA': 'sum',
        'CX_DIA_MANUAL': 'sum',
        'COMBO_DIA': 'sum',
        'CESTO_DIA': 'sum',
        'SACO_DIA': 'sum',
        'BLOCO_DIA': 'sum'
}).reset_index()

# tabela de referência com a quantidade total de caixas, caixas manuais e pacotes que serão criados por dia

df_plano_total_caixa = df_plano.groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO',]).agg({'CX_DIA': 'sum'}).reset_index()

df_plano_total_caixa_sem_pernil_terceiro = df_plano[
    ~df_plano['FAMILIA_PLANO_PRODUCAO'].isin(['Pernil terceiro'])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO',]).agg({'CX_DIA': 'sum'}).reset_index()

df_plano_total_caixa_manual = df_plano.groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO',]).agg({'CX_DIA_MANUAL': 'sum'}).reset_index()

df_plano_total_pacote = df_plano.groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO',]).agg({'PCT_DIA': 'sum'}).reset_index()

# COMMAND ----------

# MAGIC %md
# MAGIC #### Rendimento

# COMMAND ----------

if hoje.day <= 5:
  
  final_mes_anterior = hoje.replace(day = 1) - timedelta(days=1)
  
  string_mes_ref = str(final_mes_anterior.year) + str(final_mes_anterior.month)
  
else:
  
  string_mes_ref = str(hoje.year) + str(hoje.month)
  
query = f"""

SELECT
  *
FROM
  `seara-analytics-prod.dmdo_gastos_305013.rendimentos_suinos`
WHERE
  DT_REFERENCIA = '{string_mes_ref}'
  AND ITEM IS NOT NULL
  AND ITEM != ' - '

"""

df_rendimento = client.query(query).to_dataframe()

dict_cd_filial = {
    'DOURADOS': 228,
    'ITAPIRANGA': 633,
    'CARAMBEI': 902,
    'TRES PASSOS': 918,
    'SEBERI': 475,
    'SEARA': 136,
    'ANA RECH': 827,
    'SÃO MIGUEL DO OESTE': 581
}

df_rendimento['CD_FILIAL'] = df_rendimento['FILIAL'].apply(lambda x: dict_cd_filial.get(x))

df_rendimento['CD_ITEM'] = df_rendimento['ITEM'].apply(lambda x: int(x.split(' - ')[0]))

df_rendimento.rename(columns={
    'GERENCIAL': 'GRUPO_RENDIMENTO',
    'POTENCIAL': 'RENDIMENTO_POTENCIAL',
    'REAL': 'RENDIMENTO_REAL'
}, inplace = True)

df_rendimento = df_rendimento.groupby(['CD_FILIAL','GRUPO_RENDIMENTO']).agg({
    'RENDIMENTO_POTENCIAL': 'sum',
    'RENDIMENTO_REAL': 'sum',
    'QT_PESO_PRODUTO': 'sum',
    'VL_VOLUME_CIRURGICO_TOTAL': 'sum'
}).reset_index().round(6)

# df_rendimento['RENDIMENTO_B100_OLD'] = df_rendimento['RENDIMENTO_REAL']/(df_rendimento['RENDIMENTO_POTENCIAL'].apply(lambda x: x if x > 0 else np.nan))

df_rendimento['RENDIMENTO_B100'] = df_rendimento['QT_PESO_PRODUTO']/(df_rendimento['VL_VOLUME_CIRURGICO_TOTAL'].apply(lambda x: x if x > 0 else np.nan))

df_rendimento = df_rendimento[[
  'CD_FILIAL',
  'GRUPO_RENDIMENTO',
  'RENDIMENTO_POTENCIAL',
  'RENDIMENTO_REAL',
  'RENDIMENTO_B100'
]]
# COMMAND ----------

# MAGIC %md
# MAGIC #### Dimensão atividades

# COMMAND ----------

df_dim_atividades = pd.read_excel('gs://comparativo_produtividade/inputs_comp_suinos/Tempos_padrao_suinos.xlsx', sheet_name = 'Dim Atividade')

# COMMAND ----------

df_dim_atividades.dropna(subset=['ATIVIDADE'], inplace = True)

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Tabelas com a ordem correta das áreas

# COMMAND ----------

# AREA
df_sort_area = df_dim_atividades.groupby(['AREA']).agg({'ORDEM': 'min'}).reset_index()

# SUBAREA
df_sort_subarea = df_dim_atividades.groupby(['SUBAREA']).agg({'ORDEM': 'min'}).reset_index()

# ETAPA
df_sort_etapa = df_dim_atividades.groupby(['ETAPA']).agg({'ORDEM': 'min'}).reset_index()

# COMMAND ----------

df_dim_atividades = df_dim_atividades.merge(
    df_sort_area.rename(columns={'ORDEM': 'ORDEM_AREA'}), how = 'left', on = ['AREA']
    ).merge(
    df_sort_subarea.rename(columns={'ORDEM': 'ORDEM_SUBAREA'}), how = 'left', on = ['SUBAREA']
    ).merge(
    df_sort_etapa.rename(columns={'ORDEM': 'ORDEM_ETAPA'}), how = 'left', on = ['ETAPA']
)

# COMMAND ----------

# Adiciona uma flag para as etapas nas quais o ql dimensionado só deve ser arredondado para cima após a soma de toadas as atividades

lista_etapas = [
    "Vísceras vermelhas",
    "Refilar Filé",
    "Refilar Copa",
#     "Refilar Lombo",
#     "Desossar Lombo",
    "Padronizar Lombo",
    "Passar filme nos cortes - Linha Vacuo",
    "Ensacar cortes - Linha Vacuo",
    "Selar cortes - Linha Vacuo",
    "Abastecer maquinas seladoras - Linha Vacuo",
    "Encaixotar produtos - Linha Vacuo",
    "Pesar e selar caixas - Linha Vacuo",
    "Padronizar Cortes - Porcionados",
    "Embalar cortes - Porcionados",
    "Encaixotar e pesar produtos - Porcionados",
    "Abastecer Termoformadora",
    "Etiquetar Termoformados",
    "Encaixotar Termoformados",
    "Direcionar barriga para descorear",
    "Refilar barriga",
    "Refilar Costela da Barriga",
    "Padronizar Costela da Barriga",
    "Selar barriga",
    "Serrar Costela Ponta",
    "Separar Costela Ponta",
    "Refilar Costela Ponta",
    "Refilar gordura mole na paleta",
    "Passar Trimmer na paleta",
    "Refilar  paleta",
    "Refilar gordura mole",
    "Passar trimmer no pernil",
    "Separar peças e limpar tortuguita",
    "Passar peças nas desmembradeiras",
    "Refilar pernil",
    "Refilar coxão Mole",
    "Refilar coxão duro",
    "Refilar patinho",
    "Refilar alcatra",
    "Courear pernil c/osso Linha Festa",
    "Serrar pernil c/osso Linha Festa",
    "Refilar pernil c/osso Linha Festa",
    "Pesar pernil c/osso Linha Festa",
    "Embalar e selar pernil c/ osso Linha Festa",
    "Pesar e encaixotar pernil c/osso Linha Festa",
    "Injetar produtos temperados Linha Festa",
    "Tamblear produtos temperados Linha Festa",
    "Embalar produtos em filme Linha festa",
    "Embalagem Comercial Linha Festa",
    "Selar produtos Linha festa",
    "Embalar, grampear e termoencolher produtos Linha festa",
    "Encaixotar e pesar Linha festa",
    "Embalar e pesar recorte Magro",
    "Embalar e pesar recorte Gordo",
    "Embalar e pesar recorte Gorduras",
    "Refilar e embalar toucinho (Lombar)",
    "Embalar e pesar recorte Toucinho",
    "Refilar Pele",
    "Embalar Pele",
    "Embalar e selar filé",
    "Embalar e selar copa",
    "Abastecer Termoformadora",
    "Abastecer Seladoras",
    "Embalar e selar carré",
    "Embalar Lombo",
    "Embalar Barriga",
    "Embalar costela da barriga",
    "Embalar costela ponta",
    "Embalar  paleta",
    "Embalar Pernil",
    "Encaixotar filé",
    "Encaixotar copa",
    "Encaixotar carré",
    "Encaixotar Lombo",
    "Encaixotar Barriga",
    "Encaixotar costela da barriga",
    "Encaixotar  paleta",
    "Encaixotar pernil",
    "Ensacar salgados individuais / Abrir embalagem",
    "Abastecer seladora Salgados",
    "Serrar produtos termoformados",
    "Operar Injetora - Termoformados",
    "Operar tambler - Termoformados",
    "Carregar e descarregar tambler - Termoformados",
    "Embalar produtos - Termoformados"
]

df_dim_atividades['ARREDONDAMENTO_APOS_SOMA'] = 0

df_dim_atividades.loc[
    df_dim_atividades['ETAPA'].isin(lista_etapas)
, 'ARREDONDAMENTO_APOS_SOMA'] = 1

# COMMAND ----------

aux = df_dim_atividades.groupby(['ETAPA']).agg({'ATIVIDADE': ';'.join}).reset_index()

aux['ETAPA_POSSUI_REVEZAMENTO'] = aux['ATIVIDADE'].apply(
    lambda x: 1 if ('Revezamento de pausas' in x) else 0)

aux['ETAPA_POSSUI_ESCALA'] = aux['ATIVIDADE'].apply(
    lambda x: 1 if ('Escala Móvel' in x) else 0)

aux['ETAPA_POSSUI_AFF'] = aux['ATIVIDADE'].apply(
    lambda x: 1 if ('AFF' in x) else 0)

df_dim_atividades = df_dim_atividades.merge(aux[['ETAPA','ETAPA_POSSUI_REVEZAMENTO','ETAPA_POSSUI_ESCALA','ETAPA_POSSUI_AFF']], how = 'left', on = ['ETAPA'])

# COMMAND ----------

# MAGIC %md
# MAGIC #### Tempos padrão

# COMMAND ----------

df_fatos_atividades = pd.read_excel('gs://comparativo_produtividade/inputs_comp_suinos/Tempos_padrao_suinos.xlsx', sheet_name = 'Base')

# COMMAND ----------

df_fatos_atividades = df_fatos_atividades[[
    'CD_FILIAL',
    'ATIVIDADE',
    'TURNO',
    'TEMPO_PADRAO_SEG',
    'FATOR_TEMPO',
    'QL_ADICIONAL',
    'MOTIVO_QL_ADICIONAL'
]]

# COMMAND ----------

df_fatos_atividades = df_fatos_atividades.merge(df_dim_atividades[[
    'ATIVIDADE', 'UNIDADE_TEMPO_PADRAO','FAMILIA_PARA_CONSIDERAR',
       'FAMILIA_PLANO_PRODUCAO', 'FAMILIA_ANALITICA', 'FAMILIA_SINTETICA',
       'GRUPO_RENDIMENTO'
]], how = 'left', on = ['ATIVIDADE'])

# COMMAND ----------

df_fatos_atividades = df_fatos_atividades.merge(
    df_rendimento[['CD_FILIAL','GRUPO_RENDIMENTO', 'RENDIMENTO_POTENCIAL','RENDIMENTO_REAL','RENDIMENTO_B100']], 
    how = 'left', 
    left_on = ['CD_FILIAL','GRUPO_RENDIMENTO'],
    right_on = ['CD_FILIAL', 'GRUPO_RENDIMENTO']
)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Plano de produção diário

# COMMAND ----------

# dfs auxiliares para encontrar a quantidade correta a ser utilizada na velocidade da linha

# ------------ Cabeça condicional -------------------- #
df_fatos_atividades_cabeca_condicional = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Cabeça - condicional')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Analitica')
].copy()

# ------------ Plano de produção -------------------- #
df_fatos_atividades_plano_dmdo = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Plano de Produção')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Plano')
].copy()

df_fatos_atividades_plano_sintetica = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Plano de Produção')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Sintetica')
].copy()

df_fatos_atividades_plano_analitica = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Plano de Produção')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Analitica')
].copy()

# ------------ Caixa -------------------- #
df_fatos_atividades_caixa_dmdo = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Caixa')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Plano')
].copy()

df_fatos_atividades_caixa_sintetica = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Caixa')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Sintetica')
].copy()

df_fatos_atividades_caixa_analitica = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Caixa')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Analitica')
].copy()

# ------------ Total caixas -------------------- #
df_fatos_atividades_total_caixa = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Total Caixa/hr')
].copy()

df_fatos_atividades_total_caixa_manual = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Total Caixa Manual/hr')
].copy()

df_fatos_atividades_total_pacote = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Total Pacote/hr')
].copy()

# ------------ Pausa -------------------- #
df_fatos_atividades_pausa = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Pausa')
].copy()

# ------------ Repouso Térmico -------------------- #
df_fatos_atividades_repouso = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Repouso')
].copy()


# ------------ Exceção -------------------- #
df_fatos_atividades_excecao = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Exceção')
].copy()

# Adicionando os respectivos valores de plano em kg, plano em caixas, regras das exceções etc

# Plano de produção
df_fatos_atividades_plano_dmdo = df_fatos_atividades_plano_dmdo.merge(
    df_plano_familia_dmdo[['CD_FILIAL','MES_REF','DT_PRODUCAO','FAMILIA_PLANO_PRODUCAO','QT_PESO_ALTERADO']],
    how = 'left',
    on = ['CD_FILIAL','FAMILIA_PLANO_PRODUCAO']
).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'})

df_fatos_atividades_plano_sintetica = df_fatos_atividades_plano_sintetica.merge(
    df_plano_familia_sintetica[['CD_FILIAL','MES_REF','DT_PRODUCAO','DS_FAM_SINTETICA_PRO','QT_PESO_ALTERADO']],
    how = 'left',
    left_on = ['CD_FILIAL','FAMILIA_SINTETICA'],
    right_on = ['CD_FILIAL', 'DS_FAM_SINTETICA_PRO']
).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'}).drop(columns=['DS_FAM_SINTETICA_PRO'])

df_fatos_atividades_plano_analitica = df_fatos_atividades_plano_analitica.merge(
    df_plano_familia_analitica[['CD_FILIAL','MES_REF','DT_PRODUCAO','DS_FAM_ANALITICA_PRO','QT_PESO_ALTERADO']],
    how = 'left',
    left_on = ['CD_FILIAL','FAMILIA_ANALITICA'],
    right_on = ['CD_FILIAL', 'DS_FAM_ANALITICA_PRO']
).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'}).drop(columns=['DS_FAM_ANALITICA_PRO'])

# Caixa
df_fatos_atividades_caixa_dmdo = df_fatos_atividades_caixa_dmdo.merge(
    df_plano_familia_dmdo[['CD_FILIAL','MES_REF','DT_PRODUCAO','FAMILIA_PLANO_PRODUCAO','CX_DIA']],
    how = 'left',
    on = ['CD_FILIAL','FAMILIA_PLANO_PRODUCAO']
).rename(columns={'CX_DIA': 'QT_CONSIDERADA'})

df_fatos_atividades_caixa_sintetica = df_fatos_atividades_caixa_sintetica.merge(
    df_plano_familia_sintetica[['CD_FILIAL','MES_REF','DT_PRODUCAO','DS_FAM_SINTETICA_PRO','CX_DIA']],
    how = 'left',
    left_on = ['CD_FILIAL','FAMILIA_SINTETICA'],
    right_on = ['CD_FILIAL','DS_FAM_SINTETICA_PRO']
).rename(columns={'CX_DIA': 'QT_CONSIDERADA'}).drop(columns=['DS_FAM_SINTETICA_PRO'])

df_fatos_atividades_caixa_analitica = df_fatos_atividades_caixa_analitica.merge(
    df_plano_familia_analitica[['CD_FILIAL','MES_REF','DT_PRODUCAO','DS_FAM_ANALITICA_PRO','CX_DIA']],
    how = 'left',
    left_on = ['CD_FILIAL','FAMILIA_ANALITICA'],
    right_on = ['CD_FILIAL','DS_FAM_ANALITICA_PRO']
).rename(columns={'CX_DIA': 'QT_CONSIDERADA'}).drop(columns=['DS_FAM_ANALITICA_PRO'])

# Total de caixas, caixas manuais e pacotes

# Dourados possui uma área de refile de pernil terceiro. É necessário retirar o numero de caixar dessa área.

df_fatos_atividades_total_caixa_ddo = df_fatos_atividades_total_caixa[df_fatos_atividades_total_caixa['CD_FILIAL'] == 228].copy()
df_fatos_atividades_total_caixa_ddo = df_fatos_atividades_total_caixa_ddo.merge(
    df_plano_total_caixa_sem_pernil_terceiro, 
    how = 'left', on = ['CD_FILIAL']
).rename(columns={'CX_DIA': 'QT_CONSIDERADA'})

df_fatos_atividades_total_caixa_sem_ddo = df_fatos_atividades_total_caixa[df_fatos_atividades_total_caixa['CD_FILIAL'] != 228].copy()
df_fatos_atividades_total_caixa_sem_ddo = df_fatos_atividades_total_caixa_sem_ddo.merge(
    df_plano_total_caixa, 
    how = 'left', 
    on = ['CD_FILIAL']
).rename(columns={'CX_DIA': 'QT_CONSIDERADA'})

df_fatos_atividades_total_caixa = pd.concat([df_fatos_atividades_total_caixa_ddo, df_fatos_atividades_total_caixa_sem_ddo])

df_fatos_atividades_total_caixa_manual = df_fatos_atividades_total_caixa_manual.merge(df_plano_total_caixa_manual, how = 'left', on = ['CD_FILIAL']).rename(columns={'CX_DIA_MANUAL': 'QT_CONSIDERADA'})

df_fatos_atividades_total_pacote = df_fatos_atividades_total_pacote.merge(df_plano_total_pacote, how = 'left', on = ['CD_FILIAL']).rename(columns={'PCT_DIA': 'QT_CONSIDERADA'})

# Cabeça - condicional

df_fatos_atividades_cabeca_condicional = df_fatos_atividades_cabeca_condicional.merge(
    df_plano_familia_analitica[['CD_FILIAL','MES_REF','DT_PRODUCAO','DS_FAM_ANALITICA_PRO','QT_PESO_ALTERADO']],
    how = 'left',
    left_on = ['CD_FILIAL','FAMILIA_ANALITICA'],
    right_on = ['CD_FILIAL', 'DS_FAM_ANALITICA_PRO']
).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'}).drop(columns=['DS_FAM_ANALITICA_PRO'])

df_fatos_atividades_cabeca_condicional['EXISTE_PLANO'] = df_fatos_atividades_cabeca_condicional['QT_CONSIDERADA'].apply(lambda x: 1 if x > 0 else 0)

df_fatos_atividades_cabeca_condicional['QT_CONSIDERADA'] = np.nan

df_fatos_atividades_cabeca_condicional = df_fatos_atividades_cabeca_condicional[
    df_fatos_atividades_cabeca_condicional['EXISTE_PLANO'] == 1
]

##### Exceções

# Salgados individuais

lista_salgados_individuais = [43277,43275,43272,43274,43276]

df_plano_salgados_individuais = df_plano[df_plano['CD_ITEM'].isin(lista_salgados_individuais)].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_ALTERADO':'sum'}).reset_index()

df_fatos_atividades_excecao_aux1 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Abastecer Linha Individual - Salgados",
        "Ensacar individuais / Abrir embalagem - Costela",
        "Ensacar individuais / Abrir embalagem - Rabo",
        "Ensacar individuais / Abrir embalagem - Mix",
        "Ensacar individuais / Abrir embalagem - Pé",
        "Ensacar individuais / Abrir embalagem - Máscara",
        "Abastecer seladora - Costela - Salgados",
        "Abastecer seladora - Rabo - Salgados",
        "Abastecer seladora - Mix - Salgados",
        "Abastecer seladora - Pé - Salgados",
        "Abastecer seladora - Máscara - Salgados"
    ])
]

df_fatos_atividades_excecao_aux1 = df_fatos_atividades_excecao_aux1.merge(df_plano_salgados_individuais, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'})


# Salgados

df_plano_salgados = df_plano[df_plano['DS_FAM_ANALITICA_PRO'].isin([
    "INGREDIENTES PARA FEIJOADA",
    "RABO SUINO SALGADO",
    "MASCARA SUINA SALGADA",
    "PES SUINO SALGADO",
    "COSTELA TEMPERADA",
    "COSTELA SUINA SALGADA"])].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_ALTERADO':'sum'}).reset_index()

df_fatos_atividades_excecao_aux2 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Abastecer Linha Granel - Salgados",
        "Ressalgar / Montar Kis - Salgados",
        "Pesar Produto - Salgados",
        "Selar produtos / Passar filme - Salgados",
        "Ensacar Produto - Salgados",
        "Selar Pacotes - Salgados"
    ])
]

df_fatos_atividades_excecao_aux2 = df_fatos_atividades_excecao_aux2.merge(df_plano_salgados, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'})

# Transportar - Linha de Vacuo

df_plano_aux3 = df_fatos_atividades_plano_dmdo[
    df_fatos_atividades_plano_dmdo['ATIVIDADE'].isin(["Abastecer Selar Costela","Abastecer Selar Carré","Abastecer Selar Paleta","Abastecer Selar Pernil","Abastecer Selar Pernil Vacuo c/ Osso"])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_CONSIDERADA':'sum'}).reset_index()

df_fatos_atividades_excecao_aux3 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Transportar  - Linha de Vacuo"
    ])
]

df_fatos_atividades_excecao_aux3 = df_fatos_atividades_excecao_aux3.merge(df_plano_aux3, how = 'left', on = ['CD_FILIAL'])

# Paletizar caixas - Linha de Vacuo

df_plano_aux4 = df_fatos_atividades_plano_dmdo[
    df_fatos_atividades_plano_dmdo['ATIVIDADE'].isin(["Pesar e selar caixas de Costela","Pesar e selar caixas de Carré","Pesar e selar caixas de Paleta","Pesar e selar caixas de Pernil",
        "Pesar e selar caixas de Pernil Vacuo c/ Osso"])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_CONSIDERADA':'sum'}).reset_index()

df_fatos_atividades_excecao_aux4 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Paletizar caixas - Linha de Vacuo"
    ])
]

df_fatos_atividades_excecao_aux4 = df_fatos_atividades_excecao_aux4.merge(df_plano_aux4, how = 'left', on = ['CD_FILIAL'])


# Montar caixas Termoformados

df_plano_aux5 = df_fatos_atividades_plano_dmdo[
    df_fatos_atividades_plano_dmdo['ATIVIDADE'].isin(["Encaixotar Costela Temp","EncaixotarFilé Temp","EncaixotarLombo Temp","EncaixotarPicanha Temp"])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_CONSIDERADA':'sum'}).reset_index()

df_fatos_atividades_excecao_aux5 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Montar caixas Termoformados"
    ])
]

df_fatos_atividades_excecao_aux5 = df_fatos_atividades_excecao_aux5.merge(df_plano_aux5, how = 'left', on = ['CD_FILIAL'])


# Transportar produtos Lina festa

df_plano_aux6 = df_fatos_atividades_plano_dmdo[
    df_fatos_atividades_plano_dmdo['ATIVIDADE'].isin(["Embalar em filme pernil festa","Embalar em filme pernil c/osso s/pele festa","Embalar em filme filé Swift"])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_CONSIDERADA':'sum'}).reset_index()

df_fatos_atividades_excecao_aux6 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Transportar produtos Lina festa"
    ])
]

df_fatos_atividades_excecao_aux6 = df_fatos_atividades_excecao_aux6.merge(df_plano_aux6, how = 'left', on = ['CD_FILIAL'])

# Pesar e embalar miudos

df_plano_aux7 = df_plano[
    (df_plano['DS_FAM_SINTETICA_PRO'] == 'MIUDOS')
    &(~(df_plano['DS_FAM_ANALITICA_PRO'].isin([
        'RETO SUINO', 
        'TRIPA SUINA NAO BENEFICIADA',
        "TRIPA SUINA",
        "TRIPA BENEF.SUINA",
        "TRIPA SUINA MATRIZ"
    ])))].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'CX_DIA':'sum'}).reset_index()

df_fatos_atividades_excecao_aux7 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Embalar e pesar miudos"
    ])
]

df_fatos_atividades_excecao_aux7 = df_fatos_atividades_excecao_aux7.merge(df_plano_aux7, how = 'left', on = ['CD_FILIAL']).rename(columns={'CX_DIA': 'QT_CONSIDERADA'})


# BDJ ATM

df_plano_aux8 = df_plano[
    (df_plano['DS_FAM_ANALITICA_PRO'].isin([
    "COSTELA P/ ATM",
    "COSTELA INTEIRA ATM",
    "COPA ATM",
    "LOMBO PEDACOS ATM",
    "FILE SUINO ATM",
    "BARRIGA P/ ATM",
    "CARRE SEM VERTEBRA ATM",
    "COPA ATM",
    "LOMBO PEDACOS ATM"
    ]))].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_ALTERADO':'sum'}).reset_index()

peso_bandeja = 0.5

df_plano_aux8['QT_PESO_ALTERADO'] = df_plano_aux8['QT_PESO_ALTERADO']/peso_bandeja

df_fatos_atividades_excecao_aux8 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Transportar cortes - ATM",
        "Embandejar cortes - ATM",
        "Abastecer maquina de BDJ - ATM",
        "Operador da maquina de BDJ - ATM",
        "Colar etiqueta na BDJ - ATM",
        "Encaixotar BDJ - ATM",
        "Pesar, paletizar e transportar caixas - ATM",
        "Colar etiqueta e colocar absorvente nas BDJ - ATM",
        "Monitor de processo - ATM"
    ])
]

df_fatos_atividades_excecao_aux8 = df_fatos_atividades_excecao_aux8.merge(df_plano_aux8, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'})

# -------------------------------------------------------------------------- #

# Descarte de bisteca

lista_bisteca = [
    'BISTECA 10 kg',
    'BISTECA 15 kg',
    'BISTECA 4 kg'
]

pr_rejeito_bisteca = 0.12

df_plano_aux11 = df_plano[
    (df_plano['FAMILIA_PLANO_PRODUCAO'].isin(lista_bisteca))].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_ALTERADO':'sum'}).reset_index()

df_plano_aux11['QT_PESO_ALTERADO'] = pr_rejeito_bisteca*df_plano_aux11['QT_PESO_ALTERADO']

df_fatos_atividades_excecao_aux11 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Desossar descarte de bisteca(12%)"
    ])
]

df_fatos_atividades_excecao_aux11 = df_fatos_atividades_excecao_aux11.merge(df_plano_aux11, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'})

# -------------------------------------------------------------------------- #

# Abastecer tunel de bisteca

df_plano_aux12 = df_plano[
    (df_plano['FAMILIA_PLANO_PRODUCAO'].isin(lista_bisteca))].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_ALTERADO':'sum'}).reset_index()

df_fatos_atividades_excecao_aux12 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Abastecer tunel de bisteca"
    ])
]

df_fatos_atividades_excecao_aux12 = df_fatos_atividades_excecao_aux12.merge(df_plano_aux12, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'})

# -------------------------------------------------------------------------- #

# Embalar e pesar carre bisteca

df_plano_aux13 = df_plano[
    (df_plano['FAMILIA_PLANO_PRODUCAO'].isin(lista_bisteca))].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'CX_DIA':'sum'}).reset_index()

df_fatos_atividades_excecao_aux13 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Embalar e pesar carre bisteca",
        "Montar caixa / Encaixotar e selar bisteca"
    ])
]

df_fatos_atividades_excecao_aux13 = df_fatos_atividades_excecao_aux13.merge(df_plano_aux13, how = 'left', on = ['CD_FILIAL']).rename(columns={'CX_DIA': 'QT_CONSIDERADA'})

# -------------------------------------------------------------------------- #

# Coletar Panceta - Temp/Term

df_plano_aux14 = df_plano[
    df_plano['FAMILIA_PLANO_PRODUCAO'].isin([
        'Panceta - Temp/Term',
        'Panceta - Term'])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_ALTERADO':'sum'}).reset_index()

df_fatos_atividades_excecao_aux14 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Coletar Panceta - Temp/Term"
    ])
]

df_fatos_atividades_excecao_aux14 = df_fatos_atividades_excecao_aux14.merge(df_plano_aux14, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'})

# -------------------------------------------------------------------------- #

# Transportar Mp para as camaras

df_plano_aux15 = df_plano[
    df_plano['FAMILIA_PLANO_PRODUCAO'].isin([
        "Short Rib - Term",
        'Panceta - Temp/Term',
        'Ancho - Termp/Term',
        'Lombo - Temp/Term',
        'Filé - Temp/Term',
        'Costela - Temp/Term',
        'Acaltra - Temp/Term',
        'Picanha - Temp/Term',
        'Costela - Term',
        'Prime Rib - Term',
        'Assado de Tiras - Term',
        'Panceta - Term',
        'Picanha - Term',
        'Filé - Term',
        'Short Rib - Festa'        
    ])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_ALTERADO':'sum'}).reset_index()

df_fatos_atividades_excecao_aux15 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Transportar Mp para as camaras",
        "Abastecer mesas e congelador com Mp",
        "Colar etiquetas - Termoformados"
    ])
]

df_fatos_atividades_excecao_aux15 = df_fatos_atividades_excecao_aux15.merge(df_plano_aux15, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'})

# -------------------------------------------------------------------------- #

# Serrar Costelas - Termoformados

df_plano_aux16 = df_plano[
    df_plano['FAMILIA_PLANO_PRODUCAO'].isin([
        'Costela - Temp/Term',
        'Costela - Term'])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_ALTERADO':'sum'}).reset_index()

df_fatos_atividades_excecao_aux16 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Serrar Costelas - Termoformados"
    ])
]

df_fatos_atividades_excecao_aux16 = df_fatos_atividades_excecao_aux16.merge(df_plano_aux16, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'})

# -------------------------------------------------------------------------- #

# Operar desmembradeira - Termoformados

df_plano_aux17 = df_plano[
    df_plano['FAMILIA_PLANO_PRODUCAO'].isin([
        'Filé - Temp/Term',
        'Filé - Term'])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_ALTERADO':'sum'}).reset_index()

df_fatos_atividades_excecao_aux17 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Operar desmembradeira - Termoformados"
    ])
]

df_fatos_atividades_excecao_aux17 = df_fatos_atividades_excecao_aux17.merge(df_plano_aux17, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'})

# -------------------------------------------------------------------------- #

# Abastecer termoformadora - Termoformados

df_plano_aux18 = df_plano[
    df_plano['FAMILIA_PLANO_PRODUCAO'].isin([
        "Short Rib - Term"
        ,"Panceta - Temp/Term"
        ,"Ancho - Termp/Term"
        ,"Lombo - Temp/Term"
        ,"Filé - Temp/Term"
        ,"Costela - Temp/Term"
        ,"Acaltra - Temp/Term"
        ,"Picanha - Temp/Term"
        ,"Costela - Term"
        ,"Prime Rib - Term"
        ,"Assado de Tiras - Term"
        ,"Panceta - Term"
        ,"Picanha - Term"
        ,"Filé - Term"
    ])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_ALTERADO':'sum'}).reset_index()

df_fatos_atividades_excecao_aux18 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Abastecer termoformadora - Termoformados"
    ])
]

df_fatos_atividades_excecao_aux18 = df_fatos_atividades_excecao_aux18.merge(df_plano_aux18, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_ALTERADO': 'QT_CONSIDERADA'})

# -------------------------------------------------------------------------- #

# Encaixotar /Pesar/Paletizar caixas - Termoformados

df_plano_aux19 = df_plano[
    df_plano['FAMILIA_PLANO_PRODUCAO'].isin([
        "Short Rib - Term",
        'Panceta - Temp/Term',
        'Ancho - Termp/Term',
        'Lombo - Temp/Term',
        'Filé - Temp/Term',
        'Costela - Temp/Term',
        'Acaltra - Temp/Term',
        'Picanha - Temp/Term',
        'Costela - Term',
        'Prime Rib - Term',
        'Assado de Tiras - Term',
        'Panceta - Term',
        'Picanha - Term',
        'Filé - Term',
        'Short Rib - Festa' 
    ])].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'CX_DIA':'sum'}).reset_index()

df_fatos_atividades_excecao_aux19 = df_fatos_atividades_excecao[
    df_fatos_atividades_excecao['ATIVIDADE'].isin([
        "Encaixotar /Pesar/Paletizar caixas - Termoformados"
    ])
]

df_fatos_atividades_excecao_aux19 = df_fatos_atividades_excecao_aux19.merge(df_plano_aux19, how = 'left', on = ['CD_FILIAL']).rename(columns={'CX_DIA': 'QT_CONSIDERADA'})

# -------------------------------------------------------------------------- #

# df_fatos_atividades_excecao = pd.concat([
#     df_fatos_atividades_excecao_aux1,
#     df_fatos_atividades_excecao_aux2,
#     df_fatos_atividades_excecao_aux3,
#     df_fatos_atividades_excecao_aux4,
#     df_fatos_atividades_excecao_aux5,
#     df_fatos_atividades_excecao_aux6          
#           ], ignore_index = True)

df_fatos_atividades_abate = df_fatos_atividades[df_fatos_atividades['UNIDADE_TEMPO_PADRAO'].isin(['Cabeça', 'QL Fixo','Pausa', 'Escala','AFF', 'Repouso'])]

df_dias_producao = df_plano[['CD_FILIAL', 'MES_REF', 'DT_PRODUCAO']].drop_duplicates().sort_values(by=['CD_FILIAL','DT_PRODUCAO'])

df_fatos_atividades_abate = df_fatos_atividades_abate.merge(df_dias_producao, how = 'left', on = ['CD_FILIAL'])

# df_fatos_atividades = pd.concat([
#     df_fatos_atividades_abate,
#     df_fatos_atividades_cabeca_condicional,
#     df_fatos_atividades_plano_dmdo,
#     df_fatos_atividades_plano_sintetica,
#     df_fatos_atividades_plano_analitica,
#     df_fatos_atividades_caixa_dmdo,
#     df_fatos_atividades_caixa_sintetica,
#     df_fatos_atividades_caixa_analitica,
#     df_fatos_atividades_total_caixa,
#     df_fatos_atividades_total_caixa_manual,
#     df_fatos_atividades_total_pacote,
#     df_fatos_atividades_excecao
# ], ignore_index = True)

# df_fatos_atividades = df_fatos_atividades[[
#     'CD_FILIAL', 
#     'ATIVIDADE',
#     'DT_PRODUCAO',
#     'QT_CONSIDERADA',
#     'EXISTE_PLANO'
# ]]

# COMMAND ----------

# MAGIC %md
# MAGIC #### Plano de produção mensal - média diária

# COMMAND ----------

query = f"""
SELECT
  *,
  LEFT(CAST(PARSE_DATE('%d/%m/%Y', LEFT(DT_PRODUCAO,10)) AS STRING),7) AS MES_REF
FROM
  `seara-analytics-prod.stg_erp_seara.calendario_producao_cne`
WHERE
  CD_FILIAL IN ({filiais})
  AND PARSE_DATE('%d/%m/%Y', LEFT(DT_PRODUCAO,10)) BETWEEN '{data_inicio_periodo}' AND '{data_final_proximo_mes}'
  AND CD_PLANTA_FABRIL = 1
ORDER BY
  CD_FILIAL,
  PARSE_DATE('%d/%m/%Y', LEFT(DT_PRODUCAO,10))

"""

df_calendario_producao = client.query(query).to_dataframe()

print(df_calendario_producao.shape)

df_calendario_producao_dias = df_calendario_producao[['CD_FILIAL', 'MES_REF','DT_PRODUCAO']].copy()

df_calendario_producao_dias['DT_PRODUCAO'] = pd.to_datetime(df_calendario_producao_dias['DT_PRODUCAO'].apply(lambda x: x[:10]), format='%d/%m/%Y').dt.date

# df_calendario_producao.head()

# COMMAND ----------

df_calendario_producao = df_calendario_producao.groupby(['CD_FILIAL','MES_REF']).agg({'DT_PRODUCAO': 'count'}).reset_index().rename(columns={'DT_PRODUCAO': 'NUM_DIAS_PRODUTIVOS'})

# COMMAND ----------

query = f"""

WITH 

TABELA_ITEM AS (
        SELECT
    ITEM.*,
    CLASSE_ITEM.NM_CLASSE,
    SUBCLASSE_PRODUTO.DS_SUBCLASSE_PRODUTO,
    GRUPO_ITEM.NM_GRUPO,
    SUBGRUPO_ITEM.NM_SUBGRUPO,
    --ITEM.CD_FAMILIA_ITEM,
    FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
    FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
    FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
    FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO,
    ACLIMATACAO.NM_TIPO_ACLIMATACAO_PRO,
    CLASSIFICACAO_CARNE.DS_CLASSIFICACAO,
    ITEM.CD_DESTINO_FINAL,
    REF_CODES.DS_DESTINO_FINAL,
    MERCADO_EXPORTACAO.CD_MERCADO,
    MERCADO_EXPORTACAO.NM_MERCADO,
    ITEM.CD_NEGOCIO,
    NEGOCIO.NM_NEGOCIO
FROM
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
    AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
    AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
ON
    ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
ON
    SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
ON
    FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
    --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
ON
    FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
ON
    FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
    AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subclasse_produto` AS SUBCLASSE_PRODUTO  
ON 
    ITEM.CD_SUBCLASSE_PRODUTO = SUBCLASSE_PRODUTO.CD_SUBCLASSE_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.tipo_aclimatacao_produto` AS ACLIMATACAO  
ON
    ITEM.CD_TIPO_ACLIMATACAO = ACLIMATACAO.CD_TIPO_ACLIMATACAO_PRO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_item_carne` AS CLASSIFICACAO_ITEM_CARNE
ON
    CLASSIFICACAO_ITEM_CARNE.CD_ITEM = ITEM.CD_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_carne` AS CLASSIFICACAO_CARNE
ON
    CLASSIFICACAO_CARNE.CD_CLASSIFICACAO = CLASSIFICACAO_ITEM_CARNE.CD_CLASSIFICACAO
LEFT JOIN
    (
        SELECT
          CAST(RV_LOW_VALUE AS INT) AS CD_DESTINO_FINAL,
          RV_MEANING AS DS_DESTINO_FINAL
        FROM
          `seara-analytics-prod.stg_erp_seara.bcmg_ref_codes`
        WHERE
          RV_DOMAIN = 'ITEM.CD_DESTINO_FINAL'
    ) AS REF_CODES
ON
    ITEM.CD_DESTINO_FINAL = REF_CODES.CD_DESTINO_FINAL
LEFT JOIN
    `seara-analytics-prod.erp_seara.mercado_exportacao` MERCADO_EXPORTACAO
ON
    ITEM.CD_MERCADO = MERCADO_EXPORTACAO.CD_MERCADO
LEFT JOIN
    `seara-analytics-prod.erp_seara.negocio` NEGOCIO
ON
    ITEM.CD_NEGOCIO = NEGOCIO.CD_NEGOCIO
),

FILIAIS AS (

    WITH A AS (
        SELECT
            CD_FILIAL,
            NM_FILIAL,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
    )

SELECT CD_FILIAL, NM_FILIAL FROM A WHERE A.rn = 1
)


SELECT
  --PLANO.CD_EMPRESA,
  PLANO.CD_FILIAL,
  PLANO.CD_ITEM,
  TABELA_ITEM.NM_ITEM,
  CONCAT(PLANO.CD_ITEM,' - ', TABELA_ITEM.NM_ITEM) AS FILTRO_ITEM,
  --CONCAT(CD_FILIAL, ' - ', PLANO.CD_ITEM) AS CHAVE_FILIAL_ITEM,
  SUM(COALESCE(PLANO.QT_PRD_ALTERADA, PLANO.QT_PRD_PLANO)) AS QT_PESO_ALTERADO,
  --PARSE_DATE('%d/%m/%Y', LEFT(PLANO.DT_MES_ANO_PRODUCAO,10)) AS DT_PRODUCAO,
  LEFT(CAST(PARSE_DATE('%d/%m/%Y', LEFT(PLANO.DT_MES_ANO_PRODUCAO,10)) AS STRING),7) AS MES_REF,
  --TABELA_ITEM.DS_FAMILIA_PRODUTO,
  --TABELA_ITEM.NM_GRUPO,
  --TABELA_ITEM.NM_SUBGRUPO,
  --TABELA_ITEM.NM_CLASSE,
  --TABELA_ITEM.DS_SUBCLASSE_PRODUTO,
  TABELA_ITEM.DS_FAM_SINTETICA_PRO,
  TABELA_ITEM.DS_FAM_ANALITICA_PRO,
  --TABELA_ITEM.DS_CLASSIFICACAO,
  --TABELA_ITEM.CD_UNIDADE_MEDIDA,
  --TABELA_ITEM.CD_UND_MED_COMERCIAL,
  --TABELA_ITEM.CD_UND_MED_TRANSPORTE,
  --TABELA_ITEM.NM_TIPO_ACLIMATACAO_PRO,
  --TABELA_ITEM.QT_MINIMO,
  --TABELA_ITEM.QT_MAXIMA,
  --TABELA_ITEM.QT_PESO_PECA_MINIMO,
  --TABELA_ITEM.QT_PESO_PECA_MAXIMO,
  --TABELA_ITEM.VL_VOLUME_ITEM,
  TABELA_ITEM.DS_CLASSIFICACAO,
--   TABELA_ITEM.CD_DESTINO_FINAL,
  TABELA_ITEM.DS_DESTINO_FINAL,
--   TABELA_ITEM.CD_MERCADO,
  TABELA_ITEM.NM_MERCADO,
--   TABELA_ITEM.CD_NEGOCIO,
  TABELA_ITEM.NM_NEGOCIO
FROM 
  `seara-analytics-prod.stg_erp_seara.plano_mensal_pdc_item_cne` PLANO
LEFT JOIN
  TABELA_ITEM
ON
  PLANO.CD_ITEM = TABELA_ITEM.CD_ITEM
WHERE 
  PARSE_DATE('%d/%m/%Y', LEFT(PLANO.DT_MES_ANO_PRODUCAO,10)) BETWEEN '{data_inicio_periodo}' AND '{data_final_proximo_mes}' 
  AND COALESCE(PLANO.QT_PRD_ALTERADA, PLANO.QT_PRD_PLANO) > 0
  AND PLANO.CD_FILIAL IN ({filiais})
GROUP BY
  CD_FILIAL,
  CD_ITEM,
  NM_ITEM,
  FILTRO_ITEM,
  MES_REF,
  DS_FAM_SINTETICA_PRO,
  DS_FAM_ANALITICA_PRO,
  TABELA_ITEM.DS_CLASSIFICACAO,
  TABELA_ITEM.DS_DESTINO_FINAL,
  TABELA_ITEM.NM_MERCADO,
  TABELA_ITEM.NM_NEGOCIO
"""

df_plano_mensal = client.query(query).to_dataframe()

# print(df_plano_mensal.shape)

# df_plano_mensal.head()

# COMMAND ----------

df_plano_mensal_manual = pd.DataFrame(columns=list(df_plano_mensal.columns))

# COMMAND ----------

dict_itens_adicionar_plano_mensal = {
    586242: {
        'CD_FILIAL': 581,
        'QT_PESO_ALTERADO': 10500,
        'MES_REF': '2021-10'
    }
}

# COMMAND ----------

for item in dict_itens_adicionar_plano_mensal.keys():
    
    df_plano_mensal_manual.loc[len(df_plano_mensal_manual)] = [
        dict_itens_adicionar_plano_mensal[item]['CD_FILIAL'],
        item,
        ITEM.loc[ITEM['CD_ITEM'] == item, 'NM_ITEM'].item(),
        ITEM.loc[ITEM['CD_ITEM'] == item, 'FILTRO_ITEM'].item(),
        dict_itens_adicionar_plano_mensal[item]['QT_PESO_ALTERADO'],
        dict_itens_adicionar_plano_mensal[item]['MES_REF'],
        ITEM.loc[ITEM['CD_ITEM'] == item, 'DS_FAM_SINTETICA_PRO'].item(),
        ITEM.loc[ITEM['CD_ITEM'] == item, 'DS_FAM_ANALITICA_PRO'].item(),
        ITEM.loc[ITEM['CD_ITEM'] == item, 'DS_CLASSIFICACAO'].item(),
        ITEM.loc[ITEM['CD_ITEM'] == item, 'DS_DESTINO_FINAL'].item(),
        ITEM.loc[ITEM['CD_ITEM'] == item, 'NM_MERCADO'].item(),
        ITEM.loc[ITEM['CD_ITEM'] == item, 'NM_NEGOCIO'].item(),
    ]

# COMMAND ----------

for item in dict_itens_adicionar_plano_mensal.keys():
    
    filial = dict_itens_adicionar_plano_mensal[item]['CD_FILIAL']
    
    if item in df_plano_mensal.loc[df_plano_mensal['CD_FILIAL'] == filial, 'CD_ITEM']:
        
        df_plano_mensal_manual = df_plano_mensal_manual[df_plano_mensal_manual['CD_ITEM'] != item]
        

# COMMAND ----------

if df_plano_mensal_manual.shape[0] > 0:
    
    df_plano_mensal = pd.concat([df_plano_mensal, df_plano_mensal_manual])

# COMMAND ----------

df_plano_mensal['QT_PESO_ALTERADO'] = df_plano_mensal['QT_PESO_ALTERADO'].astype(float)

# COMMAND ----------

df_plano_mensal = df_plano_mensal.merge(df_calendario_producao, how = 'left', on = ['CD_FILIAL', 'MES_REF'])

# COMMAND ----------

df_plano_mensal['QT_PESO_DIARIZADO'] = round(df_plano_mensal['QT_PESO_ALTERADO']/df_plano_mensal['NUM_DIAS_PRODUTIVOS'].apply(lambda x: x if x > 0 else None),0)

# COMMAND ----------

# Adiciona o peso das caixas dos itens etc

df_plano_mensal = df_plano_mensal.merge(df_ds_pacote, how = 'left', on = ['CD_FILIAL','CD_ITEM'])

df_plano_mensal['PCT_DIA'] = round(df_plano_mensal['QT_PESO_DIARIZADO']/df_plano_mensal['PESO_PACOTE'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano_mensal['CX_DIA_AUTOMATICA'] = round(df_plano_mensal['QT_PESO_DIARIZADO']/df_plano_mensal['PESO_CX_AUT'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano_mensal['CX_DIA_MANUAL'] = round(df_plano_mensal['QT_PESO_DIARIZADO']/df_plano_mensal['PESO_CX_MANUAL'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano_mensal['COMBO_DIA'] = round(df_plano_mensal['QT_PESO_DIARIZADO']/df_plano_mensal['PESO_COMBO'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano_mensal['CESTO_DIA'] = round(df_plano_mensal['QT_PESO_DIARIZADO']/df_plano_mensal['PESO_CESTO_AR'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano_mensal['SACO_DIA'] = round(df_plano_mensal['QT_PESO_DIARIZADO']/df_plano_mensal['PESO_SACO'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano_mensal['BLOCO_DIA'] = round(df_plano_mensal['QT_PESO_DIARIZADO']/df_plano_mensal['PESO_BLOCO'].apply(lambda x: np.nan if x == 0 else x),1)

df_plano_mensal['CX_DIA'] = df_plano_mensal['CX_DIA_AUTOMATICA'].fillna(0) + df_plano_mensal['CX_DIA_MANUAL'].fillna(0) + df_plano_mensal['COMBO_DIA'].fillna(0) + df_plano_mensal['CESTO_DIA'].fillna(0) + df_plano_mensal['SACO_DIA'].fillna(0) + df_plano_mensal['BLOCO_DIA'].fillna(0)

# COMMAND ----------

# df_plano_mensal.loc[
#     (df_plano_mensal['CD_FILIAL'] == 918)
#     &(df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isna())
# # , 'DS_FAM_ANALITICA_PRO'
# ]#.unique()

# COMMAND ----------

df_plano_mensal_copy = df_plano_mensal.copy()

# df_plano_mensal_copy.to_excel('plano_mensal.xlsx', index = False)

# COMMAND ----------

df_plano_mensal.drop(columns=['QT_PESO_ALTERADO'], inplace = True)

# COMMAND ----------

df_plano_mensal = df_plano_mensal.merge(df_calendario_producao_dias, how = 'left', on = ['CD_FILIAL', 'MES_REF'])

# COMMAND ----------

# tabela de referência agrupada pelas famílias criadas no simulador

df_plano_mensal_familia_dmdo = df_plano_mensal.groupby([ 
    'CD_FILIAL', 
    'MES_REF',
    'DT_PRODUCAO',
    'FAMILIA_PLANO_PRODUCAO']).agg({
        'QT_PESO_DIARIZADO': 'sum',
        'PCT_DIA': 'sum',
        'CX_DIA': 'sum',
        'CX_DIA_AUTOMATICA': 'sum',
        'CX_DIA_MANUAL': 'sum',
        'COMBO_DIA': 'sum',
        'CESTO_DIA': 'sum',
        'SACO_DIA': 'sum',
        'BLOCO_DIA': 'sum'
}).reset_index()

# tabela de referência agrupada pela familia sintetica

df_plano_mensal_familia_sintetica = df_plano_mensal.groupby([
    'CD_FILIAL', 
    'MES_REF',
    'DT_PRODUCAO',
    'DS_FAM_SINTETICA_PRO']).agg({
        'QT_PESO_DIARIZADO': 'sum',
        'PCT_DIA': 'sum',
        'CX_DIA': 'sum',
        'CX_DIA_AUTOMATICA': 'sum',
        'CX_DIA_MANUAL': 'sum',
        'COMBO_DIA': 'sum',
        'CESTO_DIA': 'sum',
        'SACO_DIA': 'sum',
        'BLOCO_DIA': 'sum'
}).reset_index()

# tabela de referência agrupada pela familia sintetica

df_plano_mensal_familia_analitica = df_plano_mensal.groupby([
    'CD_FILIAL', 
    'MES_REF',
    'DT_PRODUCAO',
    'DS_FAM_ANALITICA_PRO']).agg({
        'QT_PESO_DIARIZADO': 'sum',
        'PCT_DIA': 'sum',
        'CX_DIA': 'sum',
        'CX_DIA_AUTOMATICA': 'sum',
        'CX_DIA_MANUAL': 'sum',
        'COMBO_DIA': 'sum',
        'CESTO_DIA': 'sum',
        'SACO_DIA': 'sum',
        'BLOCO_DIA': 'sum'
}).reset_index()

# tabela de referência com a quantidade total de caixas, caixas manuais e pacotes que serão criados por dia

df_plano_mensal_total_caixa = df_plano_mensal.groupby(['CD_FILIAL', 'DT_PRODUCAO','MES_REF',]).agg({'CX_DIA': 'sum'}).reset_index()

df_plano_mensal_total_caixa_sem_pernil_terceiro = df_plano_mensal[
    ~df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isin(['Pernil terceiro'])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO',]).agg({'CX_DIA': 'sum'}).reset_index()

df_plano_mensal_total_caixa_manual = df_plano_mensal.groupby(['CD_FILIAL', 'DT_PRODUCAO','MES_REF',]).agg({'CX_DIA_MANUAL': 'sum'}).reset_index()

df_plano_mensal_total_pacote = df_plano_mensal.groupby(['CD_FILIAL', 'DT_PRODUCAO','MES_REF',]).agg({'PCT_DIA': 'sum'}).reset_index()

# COMMAND ----------

# Criandos dfs auxiliares que serão utilizados para adicionar o plano mensal diarizado

# ------------ Cabeça condicional -------------------- #
df_fatos_atividades_cabeca_condicional_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Cabeça - condicional')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Analitica')
].copy()

# ------------ Plano de produção -------------------- #
df_fatos_atividades_plano_dmdo_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Plano de Produção')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Plano')
].copy()

df_fatos_atividades_plano_sintetica_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Plano de Produção')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Sintetica')
].copy()

df_fatos_atividades_plano_analitica_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Plano de Produção')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Analitica')
].copy()

# ------------ Caixa -------------------- #
df_fatos_atividades_caixa_dmdo_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Caixa')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Plano')
].copy()

df_fatos_atividades_caixa_sintetica_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Caixa')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Sintetica')
].copy()

df_fatos_atividades_caixa_analitica_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Caixa')
    &(df_fatos_atividades['FAMILIA_PARA_CONSIDERAR'] == 'Familia Analitica')
].copy()

# ------------ Total caixas -------------------- #
df_fatos_atividades_total_caixa_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Total Caixa/hr')
].copy()

df_fatos_atividades_total_caixa_manual_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Total Caixa Manual/hr')
].copy()

df_fatos_atividades_total_pacote_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Total Pacote/hr')
].copy()

# ------------ Pausa -------------------- #
df_fatos_atividades_pausa_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Pausa')
].copy()

# ------------ Exceção -------------------- #
df_fatos_atividades_excecao_mensal = df_fatos_atividades[
    (df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Exceção')
].copy()

# COMMAND ----------

# Adicionando os respectivos valores de plano mensal diarizado em kg, plano em caixas, regras das exceções etc

# Plano de produção

# -------------------------------------------------------------------------- #
df_fatos_atividades_plano_dmdo_mensal = df_fatos_atividades_plano_dmdo_mensal.merge(
    df_plano_mensal_familia_dmdo[['CD_FILIAL', 'DT_PRODUCAO','MES_REF','FAMILIA_PLANO_PRODUCAO','QT_PESO_DIARIZADO']],
    how = 'left',
    on = ['CD_FILIAL','FAMILIA_PLANO_PRODUCAO']
).rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'})

colunas_para_merge = list(df_fatos_atividades_plano_dmdo_mensal.columns)
colunas_para_merge.remove('QT_CONSIDERADA_MENSAL')

df_fatos_atividades_plano_dmdo = df_fatos_atividades_plano_dmdo.merge(
    df_fatos_atividades_plano_dmdo_mensal,
    how = 'outer',
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

df_fatos_atividades_plano_sintetica_mensal = df_fatos_atividades_plano_sintetica_mensal.merge(
    df_plano_mensal_familia_sintetica[['CD_FILIAL', 'DT_PRODUCAO','MES_REF','DS_FAM_SINTETICA_PRO','QT_PESO_DIARIZADO']],
    how = 'left',
    left_on = ['CD_FILIAL','FAMILIA_SINTETICA'],
    right_on = ['CD_FILIAL', 'DS_FAM_SINTETICA_PRO']
).rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'}).drop(columns=['DS_FAM_SINTETICA_PRO'])

df_fatos_atividades_plano_sintetica = df_fatos_atividades_plano_sintetica.merge(
    df_fatos_atividades_plano_sintetica_mensal,
    how = 'outer',
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

df_fatos_atividades_plano_analitica_mensal = df_fatos_atividades_plano_analitica_mensal.merge(
    df_plano_mensal_familia_analitica[['CD_FILIAL', 'DT_PRODUCAO','MES_REF','DS_FAM_ANALITICA_PRO','QT_PESO_DIARIZADO']],
    how = 'left',
    left_on = ['CD_FILIAL','FAMILIA_ANALITICA'],
    right_on = ['CD_FILIAL','DS_FAM_ANALITICA_PRO']
).rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'}).drop(columns=['DS_FAM_ANALITICA_PRO'])

df_fatos_atividades_plano_analitica = df_fatos_atividades_plano_analitica.merge(
    df_fatos_atividades_plano_analitica_mensal,
    how = 'outer',
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Caixa
df_fatos_atividades_caixa_dmdo_mensal = df_fatos_atividades_caixa_dmdo_mensal.merge(
    df_plano_mensal_familia_dmdo[['CD_FILIAL', 'DT_PRODUCAO','MES_REF','FAMILIA_PLANO_PRODUCAO','CX_DIA']],
    how = 'left',
    on = ['CD_FILIAL','FAMILIA_PLANO_PRODUCAO']
).rename(columns={'CX_DIA': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_caixa_dmdo = df_fatos_atividades_caixa_dmdo.merge(
    df_fatos_atividades_caixa_dmdo_mensal,
    how = 'outer',
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

df_fatos_atividades_caixa_sintetica_mensal = df_fatos_atividades_caixa_sintetica_mensal.merge(
    df_plano_mensal_familia_sintetica[['CD_FILIAL', 'DT_PRODUCAO','MES_REF','DS_FAM_SINTETICA_PRO','CX_DIA']],
    how = 'left',
    left_on = ['CD_FILIAL','FAMILIA_SINTETICA'],
    right_on = ['CD_FILIAL','DS_FAM_SINTETICA_PRO']
).rename(columns={'CX_DIA': 'QT_CONSIDERADA_MENSAL'}).drop(columns=['DS_FAM_SINTETICA_PRO'])

df_fatos_atividades_caixa_sintetica = df_fatos_atividades_caixa_sintetica.merge(
    df_fatos_atividades_caixa_sintetica_mensal,
    how = 'outer',
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

df_fatos_atividades_caixa_analitica_mensal = df_fatos_atividades_caixa_analitica_mensal.merge(
    df_plano_mensal_familia_analitica[['CD_FILIAL', 'DT_PRODUCAO','MES_REF','DS_FAM_ANALITICA_PRO','CX_DIA']],
    how = 'left',
    left_on = ['CD_FILIAL','FAMILIA_ANALITICA'],
    right_on = ['CD_FILIAL','DS_FAM_ANALITICA_PRO']
).rename(columns={'CX_DIA': 'QT_CONSIDERADA_MENSAL'}).drop(columns=['DS_FAM_ANALITICA_PRO'])

df_fatos_atividades_caixa_analitica = df_fatos_atividades_caixa_analitica.merge(
    df_fatos_atividades_caixa_analitica_mensal,
    how = 'outer',
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# COMMAND ----------

# Total de caixas, caixas manuais e pacotes

# Dourados possui uma área de refile de pernil terceiro. É necessário retirar o numero de caixar dessa área.

df_fatos_atividades_total_caixa_mensal_ddo = df_fatos_atividades_total_caixa_mensal[df_fatos_atividades_total_caixa_mensal['CD_FILIAL'] == 228].copy()
df_fatos_atividades_total_caixa_mensal_ddo = df_fatos_atividades_total_caixa_mensal_ddo.merge(
    df_plano_mensal_total_caixa_sem_pernil_terceiro, 
    how = 'left', on = ['CD_FILIAL']
).rename(columns={'CX_DIA': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_total_caixa_mensal_sem_ddo = df_fatos_atividades_total_caixa_mensal[df_fatos_atividades_total_caixa_mensal['CD_FILIAL'] != 228].copy()
df_fatos_atividades_total_caixa_mensal_sem_ddo = df_fatos_atividades_total_caixa_mensal_sem_ddo.merge(
    df_plano_mensal_total_caixa, 
    how = 'left', 
    on = ['CD_FILIAL']
).rename(columns={'CX_DIA': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_total_caixa_mensal = pd.concat([df_fatos_atividades_total_caixa_mensal_ddo, df_fatos_atividades_total_caixa_mensal_sem_ddo])

df_fatos_atividades_total_caixa = df_fatos_atividades_total_caixa.merge(
    df_fatos_atividades_total_caixa_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

df_fatos_atividades_total_caixa_manual_mensal = df_fatos_atividades_total_caixa_manual_mensal.merge(df_plano_mensal_total_caixa_manual, how = 'left', on = ['CD_FILIAL']).rename(columns={'CX_DIA_MANUAL': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_total_caixa_manual = df_fatos_atividades_total_caixa_manual.merge(
    df_fatos_atividades_total_caixa_manual_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

df_fatos_atividades_total_pacote_mensal = df_fatos_atividades_total_pacote_mensal.merge(df_plano_mensal_total_pacote, how = 'left', on = ['CD_FILIAL']).rename(columns={'PCT_DIA': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_total_pacote = df_fatos_atividades_total_pacote.merge(
    df_fatos_atividades_total_pacote_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# COMMAND ----------

# Cabeça - condicional

df_fatos_atividades_cabeca_condicional_mensal = df_fatos_atividades_cabeca_condicional_mensal.merge(
    df_plano_mensal_familia_analitica[['CD_FILIAL', 'DT_PRODUCAO','MES_REF','DS_FAM_ANALITICA_PRO','QT_PESO_DIARIZADO']],
    how = 'left',
    left_on = ['CD_FILIAL','FAMILIA_ANALITICA'],
    right_on = ['CD_FILIAL', 'DS_FAM_ANALITICA_PRO']
).rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'}).drop(columns=['DS_FAM_ANALITICA_PRO'])

df_fatos_atividades_cabeca_condicional = df_fatos_atividades_cabeca_condicional.merge(
    df_fatos_atividades_cabeca_condicional_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

df_fatos_atividades_cabeca_condicional['EXISTE_PLANO_MENSAL'] = df_fatos_atividades_cabeca_condicional['QT_CONSIDERADA_MENSAL'].apply(lambda x: 1 if x > 0 else 0)

df_fatos_atividades_cabeca_condicional['QT_CONSIDERADA_MENSAL'] = np.nan

df_fatos_atividades_cabeca_condicional = df_fatos_atividades_cabeca_condicional[
    df_fatos_atividades_cabeca_condicional['EXISTE_PLANO_MENSAL'] == 1
]

# -------------------------------------------------------------------------- #

# COMMAND ----------

##### Exceções

# Salgados individuais

df_plano_mensal_salgados_individuais = df_plano_mensal[df_plano_mensal['CD_ITEM'].isin(lista_salgados_individuais)].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_DIARIZADO':'sum'}).reset_index()

df_fatos_atividades_excecao_aux1_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Abastecer Linha Individual - Salgados",
        "Ensacar individuais / Abrir embalagem - Costela",
        "Ensacar individuais / Abrir embalagem - Rabo",
        "Ensacar individuais / Abrir embalagem - Mix",
        "Ensacar individuais / Abrir embalagem - Pé",
        "Ensacar individuais / Abrir embalagem - Máscara",
        "Abastecer seladora - Costela - Salgados",
        "Abastecer seladora - Rabo - Salgados",
        "Abastecer seladora - Mix - Salgados",
        "Abastecer seladora - Pé - Salgados",
        "Abastecer seladora - Máscara - Salgados"
    ])
]

df_fatos_atividades_excecao_aux1_mensal = df_fatos_atividades_excecao_aux1_mensal.merge(df_plano_mensal_salgados_individuais, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_excecao_aux1 = df_fatos_atividades_excecao_aux1.merge(
    df_fatos_atividades_excecao_aux1_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Salgados

df_plano_mensal_salgados = df_plano_mensal[df_plano_mensal['DS_FAM_ANALITICA_PRO'].isin([
    "INGREDIENTES PARA FEIJOADA",
    "RABO SUINO SALGADO",
    "MASCARA SUINA SALGADA",
    "PES SUINO SALGADO",
    "COSTELA TEMPERADA",
    "COSTELA SUINA SALGADA"])].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_DIARIZADO':'sum'}).reset_index()

df_fatos_atividades_excecao_aux2_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Abastecer Linha Granel - Salgados",
        "Ressalgar / Montar Kis - Salgados",
        "Pesar Produto - Salgados",
        "Selar produtos / Passar filme - Salgados",
        "Ensacar Produto - Salgados",
        "Selar Pacotes - Salgados"
    ])
]

df_fatos_atividades_excecao_aux2_mensal = df_fatos_atividades_excecao_aux2_mensal.merge(df_plano_mensal_salgados, how = 'left', on = ['CD_FILIAL']).rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_excecao_aux2 = df_fatos_atividades_excecao_aux2.merge(
    df_fatos_atividades_excecao_aux2_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Transportar - Linha de Vacuo

df_plano_aux3_mensal = df_fatos_atividades_plano_dmdo_mensal[
    df_fatos_atividades_plano_dmdo_mensal['ATIVIDADE'].isin(["Abastecer Selar Costela","Abastecer Selar Carré","Abastecer Selar Paleta","Abastecer Selar Pernil","Abastecer Selar Pernil Vacuo c/ Osso"])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_CONSIDERADA_MENSAL':'sum'}).reset_index()

df_fatos_atividades_excecao_aux3_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Transportar  - Linha de Vacuo"
    ])
]

df_fatos_atividades_excecao_aux3_mensal = df_fatos_atividades_excecao_aux3_mensal.merge(df_plano_aux3_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux3 = df_fatos_atividades_excecao_aux3.merge(
    df_fatos_atividades_excecao_aux3_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Paletizar caixas - Linha de Vacuo

df_plano_aux4_mensal = df_fatos_atividades_plano_dmdo_mensal[
    df_fatos_atividades_plano_dmdo_mensal['ATIVIDADE'].isin(["Pesar e selar caixas de Costela","Pesar e selar caixas de Carré","Pesar e selar caixas de Paleta","Pesar e selar caixas de Pernil",
        "Pesar e selar caixas de Pernil Vacuo c/ Osso"])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_CONSIDERADA_MENSAL':'sum'}).reset_index()

df_fatos_atividades_excecao_aux4_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Paletizar caixas - Linha de Vacuo"
    ])
]

df_fatos_atividades_excecao_aux4_mensal = df_fatos_atividades_excecao_aux4_mensal.merge(df_plano_aux4_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux4 = df_fatos_atividades_excecao_aux4.merge(
    df_fatos_atividades_excecao_aux4_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Montar caixas Termoformados

df_plano_aux5_mensal = df_fatos_atividades_plano_dmdo_mensal[
    df_fatos_atividades_plano_dmdo_mensal['ATIVIDADE'].isin(["Encaixotar Costela Temp","EncaixotarFilé Temp","EncaixotarLombo Temp","EncaixotarPicanha Temp"])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_CONSIDERADA_MENSAL':'sum'}).reset_index()

df_fatos_atividades_excecao_aux5_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Montar caixas Termoformados"
    ])
]

df_fatos_atividades_excecao_aux5_mensal = df_fatos_atividades_excecao_aux5_mensal.merge(df_plano_aux5_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux5 = df_fatos_atividades_excecao_aux5.merge(
    df_fatos_atividades_excecao_aux5_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Transportar produtos Lina festa

df_plano_aux6_mensal = df_fatos_atividades_plano_dmdo_mensal[
    df_fatos_atividades_plano_dmdo_mensal['ATIVIDADE'].isin(["Embalar em filme pernil festa","Embalar em filme pernil c/osso s/pele festa","Embalar em filme filé Swift"])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_CONSIDERADA_MENSAL':'sum'}).reset_index()

df_fatos_atividades_excecao_aux6_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Transportar produtos Lina festa"
    ])
]

df_fatos_atividades_excecao_aux6_mensal = df_fatos_atividades_excecao_aux6_mensal.merge(df_plano_aux6_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux6 = df_fatos_atividades_excecao_aux6.merge(
    df_fatos_atividades_excecao_aux6_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Pesar e embalar miudos

df_plano_aux7_mensal = df_plano_mensal[
    (df_plano_mensal['DS_FAM_SINTETICA_PRO'] == 'MIUDOS')
    &(~(df_plano_mensal['DS_FAM_ANALITICA_PRO'].isin([
        'RETO SUINO', 
        'TRIPA SUINA NAO BENEFICIADA',
        "TRIPA SUINA",
        "TRIPA BENEF.SUINA",
        "TRIPA SUINA MATRIZ"
    ])))].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'CX_DIA':'sum'}).reset_index()

df_fatos_atividades_excecao_aux7_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Embalar e pesar miudos"
    ])
]

df_fatos_atividades_excecao_aux7_mensal = df_fatos_atividades_excecao_aux7_mensal.merge(df_plano_aux7_mensal, how = 'left', on = ['CD_FILIAL']).rename(columns={'CX_DIA': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_excecao_aux7 = df_fatos_atividades_excecao_aux7.merge(
    df_fatos_atividades_excecao_aux7_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Pesar e embalar miudos

df_plano_aux8_mensal = df_plano_mensal[
    (df_plano_mensal['DS_FAM_ANALITICA_PRO'].isin([
    "COSTELA P/ ATM",
    "COSTELA INTEIRA ATM",
    "COPA ATM",
    "LOMBO PEDACOS ATM",
    "FILE SUINO ATM",
    "BARRIGA P/ ATM",
    "CARRE SEM VERTEBRA ATM",
    "COPA ATM",
    "LOMBO PEDACOS ATM"
    ]))].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_DIARIZADO':'sum'}).reset_index().rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'})

df_plano_aux8_mensal['QT_CONSIDERADA_MENSAL'] = df_plano_aux8_mensal['QT_CONSIDERADA_MENSAL']/peso_bandeja

df_fatos_atividades_excecao_aux8_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Transportar cortes - ATM",
        "Embandejar cortes - ATM",
        "Abastecer maquina de BDJ - ATM",
        "Operador da maquina de BDJ - ATM",
        "Colar etiqueta na BDJ - ATM",
        "Encaixotar BDJ - ATM",
        "Pesar, paletizar e transportar caixas - ATM",
        "Colar etiqueta e colocar absorvente nas BDJ - ATM",
        "Monitor de processo - ATM"
    ])
]

df_fatos_atividades_excecao_aux8_mensal = df_fatos_atividades_excecao_aux8_mensal.merge(df_plano_aux8_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux8 = df_fatos_atividades_excecao_aux8.merge(
    df_fatos_atividades_excecao_aux8_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Descarte de Bisteca

df_plano_aux11_mensal = df_plano_mensal[
    (df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isin(lista_bisteca))].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_DIARIZADO':'sum'}).reset_index().rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'})

df_plano_aux11_mensal['QT_CONSIDERADA_MENSAL'] = pr_rejeito_bisteca*df_plano_aux11_mensal['QT_CONSIDERADA_MENSAL']

df_fatos_atividades_excecao_aux11_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Desossar descarte de bisteca(12%)"
    ])
]

df_fatos_atividades_excecao_aux11_mensal = df_fatos_atividades_excecao_aux11_mensal.merge(df_plano_aux11_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux11 = df_fatos_atividades_excecao_aux11.merge(
    df_fatos_atividades_excecao_aux11_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Abastecer tunel de bisteca

df_plano_aux12_mensal = df_plano_mensal[
    (df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isin(lista_bisteca))].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_DIARIZADO':'sum'}).reset_index().rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_excecao_aux12_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Abastecer tunel de bisteca"
    ])
]

df_fatos_atividades_excecao_aux12_mensal = df_fatos_atividades_excecao_aux12_mensal.merge(df_plano_aux12_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux12 = df_fatos_atividades_excecao_aux12.merge(
    df_fatos_atividades_excecao_aux12_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Embalar e pesar carre bisteca

df_plano_aux13_mensal = df_plano_mensal[
    (df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isin(lista_bisteca))].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'CX_DIA':'sum'}).reset_index().rename(columns={'CX_DIA': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_excecao_aux13_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Embalar e pesar carre bisteca",
        "Montar caixa / Encaixotar e selar bisteca"
    ])
]

df_fatos_atividades_excecao_aux13_mensal = df_fatos_atividades_excecao_aux13_mensal.merge(df_plano_aux13_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux13 = df_fatos_atividades_excecao_aux13.merge(
    df_fatos_atividades_excecao_aux13_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Coletar Panceta - Temp/Term

df_plano_aux14_mensal = df_plano_mensal[
    df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isin([
        'Panceta - Temp/Term',
        'Panceta - Term'])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_DIARIZADO':'sum'}).reset_index().rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_excecao_aux14_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Coletar Panceta - Temp/Term"
    ])
]

df_fatos_atividades_excecao_aux14_mensal = df_fatos_atividades_excecao_aux14_mensal.merge(df_plano_aux14_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux14 = df_fatos_atividades_excecao_aux14.merge(
    df_fatos_atividades_excecao_aux14_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Transportar Mp para as camaras

df_plano_aux15_mensal = df_plano_mensal[
    df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isin([
        "Short Rib - Term",
        'Panceta - Temp/Term',
        'Ancho - Termp/Term',
        'Lombo - Temp/Term',
        'Filé - Temp/Term',
        'Costela - Temp/Term',
        'Acaltra - Temp/Term',
        'Picanha - Temp/Term',
        'Costela - Term',
        'Prime Rib - Term',
        'Assado de Tiras - Term',
        'Panceta - Term',
        'Picanha - Term',
        'Filé - Term',
        'Short Rib - Festa'
    ])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_DIARIZADO':'sum'}).reset_index().rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_excecao_aux15_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Transportar Mp para as camaras",
        "Abastecer mesas e congelador com Mp",
        "Colar etiquetas - Termoformados"
    ])
]

df_fatos_atividades_excecao_aux15_mensal = df_fatos_atividades_excecao_aux15_mensal.merge(df_plano_aux15_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux15 = df_fatos_atividades_excecao_aux15.merge(
    df_fatos_atividades_excecao_aux15_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Serrar Costelas - Termoformados

df_plano_aux16_mensal = df_plano_mensal[
    df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isin([
        'Costela - Temp/Term',
        'Costela - Term'])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_DIARIZADO':'sum'}).reset_index().rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_excecao_aux16_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Serrar Costelas - Termoformados"
    ])
]

df_fatos_atividades_excecao_aux16_mensal = df_fatos_atividades_excecao_aux16_mensal.merge(df_plano_aux16_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux16 = df_fatos_atividades_excecao_aux16.merge(
    df_fatos_atividades_excecao_aux16_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Operar desmembradeira - Termoformados

df_plano_aux17_mensal = df_plano_mensal[
    df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isin([
        'Filé - Temp/Term',
        'Filé - Term'])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_DIARIZADO':'sum'}).reset_index().rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_excecao_aux17_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
        "Operar desmembradeira - Termoformados"
    ])
]

df_fatos_atividades_excecao_aux17_mensal = df_fatos_atividades_excecao_aux17_mensal.merge(df_plano_aux17_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux17 = df_fatos_atividades_excecao_aux17.merge(
    df_fatos_atividades_excecao_aux17_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Abastecer termoformadora - Termoformados

df_plano_aux18_mensal = df_plano_mensal[
    df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isin([
        "Short Rib - Term"
        ,"Panceta - Temp/Term"
        ,"Ancho - Termp/Term"
        ,"Lombo - Temp/Term"
        ,"Filé - Temp/Term"
        ,"Costela - Temp/Term"
        ,"Acaltra - Temp/Term"
        ,"Picanha - Temp/Term"
        ,"Costela - Term"
        ,"Prime Rib - Term"
        ,"Assado de Tiras - Term"
        ,"Panceta - Term"
        ,"Picanha - Term"
        ,"Filé - Term"
    ])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'QT_PESO_DIARIZADO':'sum'}).reset_index().rename(columns={'QT_PESO_DIARIZADO': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_excecao_aux18_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
         "Abastecer termoformadora - Termoformados"
    ])
]

df_fatos_atividades_excecao_aux18_mensal = df_fatos_atividades_excecao_aux18_mensal.merge(df_plano_aux18_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux18 = df_fatos_atividades_excecao_aux18.merge(
    df_fatos_atividades_excecao_aux18_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #

# Encaixotar /Pesar/Paletizar caixas - Termoformados

df_plano_aux19_mensal = df_plano_mensal[
    df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isin([
        "Short Rib - Term"
        ,"Panceta - Temp/Term"
        ,"Ancho - Termp/Term"
        ,"Lombo - Temp/Term"
        ,"Filé - Temp/Term"
        ,"Costela - Temp/Term"
        ,"Acaltra - Temp/Term"
        ,"Picanha - Temp/Term"
        ,"Costela - Term"
        ,"Prime Rib - Term"
        ,"Assado de Tiras - Term"
        ,"Panceta - Term"
        ,"Picanha - Term"
        ,"Filé - Term"
    ])
].groupby(['CD_FILIAL','MES_REF','DT_PRODUCAO']).agg({'CX_DIA':'sum'}).reset_index().rename(columns={'CX_DIA': 'QT_CONSIDERADA_MENSAL'})

df_fatos_atividades_excecao_aux19_mensal = df_fatos_atividades_excecao_mensal[
    df_fatos_atividades_excecao_mensal['ATIVIDADE'].isin([
         "Encaixotar /Pesar/Paletizar caixas - Termoformados"
    ])
]

df_fatos_atividades_excecao_aux19_mensal = df_fatos_atividades_excecao_aux19_mensal.merge(df_plano_aux19_mensal, how = 'left', on = ['CD_FILIAL'])

df_fatos_atividades_excecao_aux19 = df_fatos_atividades_excecao_aux19.merge(
    df_fatos_atividades_excecao_aux19_mensal, 
    how = 'outer', 
    on = colunas_para_merge
)

# -------------------------------------------------------------------------- #


# COMMAND ----------

df_fatos_atividades_excecao = pd.concat([
    df_fatos_atividades_excecao_aux1,
    df_fatos_atividades_excecao_aux2,
    df_fatos_atividades_excecao_aux3,
    df_fatos_atividades_excecao_aux4,
    df_fatos_atividades_excecao_aux5,
    df_fatos_atividades_excecao_aux6,
    df_fatos_atividades_excecao_aux7,
    df_fatos_atividades_excecao_aux8,
#     df_fatos_atividades_excecao_aux9,
#     df_fatos_atividades_excecao_aux10,
    df_fatos_atividades_excecao_aux11,
    df_fatos_atividades_excecao_aux12,
    df_fatos_atividades_excecao_aux13,
    df_fatos_atividades_excecao_aux14,
    df_fatos_atividades_excecao_aux15,
    df_fatos_atividades_excecao_aux16,
    df_fatos_atividades_excecao_aux17,
    df_fatos_atividades_excecao_aux18,
    df_fatos_atividades_excecao_aux19
          ], ignore_index = True)

df_fatos_atividades = pd.concat([
    df_fatos_atividades_abate,
    df_fatos_atividades_cabeca_condicional,
    df_fatos_atividades_plano_dmdo,
    df_fatos_atividades_plano_sintetica,
    df_fatos_atividades_plano_analitica,
    df_fatos_atividades_caixa_dmdo,
    df_fatos_atividades_caixa_sintetica,
    df_fatos_atividades_caixa_analitica,
    df_fatos_atividades_total_caixa,
    df_fatos_atividades_total_caixa_manual,
    df_fatos_atividades_total_pacote,
    df_fatos_atividades_excecao
], ignore_index = True)

# df_fatos_atividades = df_fatos_atividades[[
#     'CD_FILIAL', 
#     'ATIVIDADE',
#     'MES_REF',
#     'DT_PRODUCAO',
#     'QT_CONSIDERADA',
#     'EXISTE_PLANO',
#     'QT_CONSIDERADA_MENSAL',
#     'EXISTE_PLANO_MENSAL'
# ]]

df_fatos_atividades['MES_REF'] = df_fatos_atividades['MES_REF'].apply(lambda x: x.replace('-','/') if not pd.isnull(x) else x)

# COMMAND ----------

df_contagem_turnos = df_fatos_atividades.groupby(['CD_FILIAL','ATIVIDADE']).agg({'TURNO': "nunique"}).reset_index().rename(columns={'TURNO': 'CONTAGEM_TURNOS'})

# COMMAND ----------

df_fatos_atividades = df_fatos_atividades.merge(df_contagem_turnos, how = 'left', on = ['CD_FILIAL', 'ATIVIDADE'])

# COMMAND ----------

etapas_sem_divisao_por_turno_da_velocidade = [
    "Encaixotar  paleta"
    ,"Encaixotar Barriga"
    ,"Encaixotar carré"
    ,"Encaixotar copa"
    ,"Encaixotar costela da barriga"
    ,"Encaixotar e pesar Linha festa"
    ,"Encaixotar e pesar produtos - Porcionados"
    ,"Encaixotar filé"
    ,"Encaixotar Lombo"
    ,"Encaixotar pernil"
    ,"Encaixotar produtos - Linha Vacuo"
    ,"Encaixotar Produtos Fatiados"
    ,"Encaixotar Termoformados"

]

atividades_sem_divisao_por_turno_da_velocidade = df_dim_atividades.loc[df_dim_atividades['ETAPA'].isin(etapas_sem_divisao_por_turno_da_velocidade), 'ATIVIDADE'].unique()

atividades_paletizacao = df_dim_atividades.loc[df_dim_atividades['SUBAREA'] == 'Paletização', 'ATIVIDADE'].unique()

num_turnos_paletizacao = {
    136: 2,
    228: 3,
    475: 1,
    581: 1,
    633: 1,
    827: 2,
    902: 1,
    918: 1
}

# Encaixotar não possui a divisão da velocidade da linha pelo numero de turnos
# Paletização possui uma outra lógica, que leva em consideração os turnos de abate

df_fatos_atividades.loc[
    (~df_fatos_atividades['CD_FILIAL'].isin([136,228]))
    |(
        (~df_fatos_atividades['ATIVIDADE'].isin(atividades_sem_divisao_por_turno_da_velocidade))
        &(~df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao))
    )
,'QT_CONSIDERADA'] = df_fatos_atividades.loc[
    (~df_fatos_atividades['CD_FILIAL'].isin([136,228]))
    |(
        (~df_fatos_atividades['ATIVIDADE'].isin(atividades_sem_divisao_por_turno_da_velocidade))
        &(~df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao))
    )
,'QT_CONSIDERADA']/df_fatos_atividades.loc[
    (~df_fatos_atividades['CD_FILIAL'].isin([136,228]))
    |(
        (~df_fatos_atividades['ATIVIDADE'].isin(atividades_sem_divisao_por_turno_da_velocidade))
        &(~df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao))
    )
,'CONTAGEM_TURNOS']

df_fatos_atividades.loc[
    (~df_fatos_atividades['CD_FILIAL'].isin([136,228]))
    |(
        (~df_fatos_atividades['ATIVIDADE'].isin(atividades_sem_divisao_por_turno_da_velocidade))
        &(~df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao))
    )
,'QT_CONSIDERADA_MENSAL'] = df_fatos_atividades.loc[
    (~df_fatos_atividades['CD_FILIAL'].isin([136,228]))
    |(
        (~df_fatos_atividades['ATIVIDADE'].isin(atividades_sem_divisao_por_turno_da_velocidade))
        &(~df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao))
    )
,'QT_CONSIDERADA_MENSAL']/df_fatos_atividades.loc[
    (~df_fatos_atividades['CD_FILIAL'].isin([136,228]))
    |(
        (~df_fatos_atividades['ATIVIDADE'].isin(atividades_sem_divisao_por_turno_da_velocidade))
        &(~df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao))
    )
,'CONTAGEM_TURNOS']


df_fatos_atividades.loc[
    df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao)
,'QT_CONSIDERADA'] = df_fatos_atividades.loc[
    df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao)
,'QT_CONSIDERADA']/df_fatos_atividades.loc[
    df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao)
,'CD_FILIAL'].map(num_turnos_paletizacao)

df_fatos_atividades.loc[
    df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao)
,'QT_CONSIDERADA_MENSAL'] = df_fatos_atividades.loc[
    df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao)
,'QT_CONSIDERADA_MENSAL']/df_fatos_atividades.loc[
    df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao)
,'CD_FILIAL'].map(num_turnos_paletizacao)

# COMMAND ----------

#df_fatos_atividades['QT_CONSIDERADA'] = df_fatos_atividades['QT_CONSIDERADA']/df_fatos_atividades['CONTAGEM_TURNOS']

#df_fatos_atividades['QT_CONSIDERADA_MENSAL'] = df_fatos_atividades['QT_CONSIDERADA_MENSAL']/df_fatos_atividades['CONTAGEM_TURNOS']

# COMMAND ----------

# MAGIC %md
# MAGIC #### Filiais benchmark

# COMMAND ----------

### OLD CODE - MANTER EM CASO DE EMERGÊNCIA

#df_tempos_benchmark = pd.read_excel('gs://comparativo_produtividade/inputs_comp_suinos/filiais benchmark suínos.xlsx')

# df_tempos_benchmark.drop(columns=['Linha', 'Grupo Rendimento'], inplace = True)

# df_tempos_benchmark.rename(columns={
#     'Filial': 'CD_FILIAL_BENCHMARK',
#     'Tarefas': 'ATIVIDADE',
#     'Tempo Padrão (s)': 'TEMPO_PADRAO_SEG_BENCHMARK',
#     'Peso por peça': 'FATOR_TEMPO_BENCHMARK',
#     'REND POTENCIAL': 'RENDIMENTO_POTENCIAL_BENCHMARK',
#     'REND REAL': 'RENDIMENTO_REAL_BENCHMARK',
#     'BM 100': 'RENDIMENTO_B100_BENCHMARK'
# }, inplace = True)

# COMMAND ----------

df_rendimento_benchmark = df_rendimento.copy()

df_rendimento_benchmark['DESVIO_POSITIVO_REND_B100'] = (df_rendimento_benchmark['RENDIMENTO_B100'] - 1) #.apply(lambda x: x if x >= 0 else 0)

df_rendimento_benchmark['DESVIO_ABS_POSITIVO_REND_B100'] = abs(df_rendimento_benchmark['DESVIO_POSITIVO_REND_B100'])

df_rendimento_benchmark.sort_values(by=['GRUPO_RENDIMENTO','DESVIO_ABS_POSITIVO_REND_B100'], inplace = True)

df_rendimento_benchmark = df_rendimento_benchmark.groupby(['GRUPO_RENDIMENTO']).first().reset_index()

df_rendimento_benchmark.drop(columns=['DESVIO_POSITIVO_REND_B100', 'DESVIO_ABS_POSITIVO_REND_B100'], inplace = True)

df_fatos_atividades_benchmark = df_fatos_atividades[['CD_FILIAL', 'ATIVIDADE', 'TEMPO_PADRAO_SEG', 'FATOR_TEMPO', 'GRUPO_RENDIMENTO']].sort_values(by=['CD_FILIAL','ATIVIDADE', 'TEMPO_PADRAO_SEG'], ascending = False).groupby([
  'CD_FILIAL', 'ATIVIDADE'
]).first().reset_index()

df_rendimento_benchmark = df_rendimento_benchmark.merge(
  df_fatos_atividades_benchmark,
  how = 'left',
  on = ['CD_FILIAL', 'GRUPO_RENDIMENTO']
)

df_rendimento_benchmark.rename(columns={
  'CD_FILIAL': 'CD_FILIAL_BENCHMARK',
  'RENDIMENTO_POTENCIAL': 'RENDIMENTO_POTENCIAL_BENCHMARK',
  'RENDIMENTO_REAL': 'RENDIMENTO_REAL_BENCHMARK',
  'RENDIMENTO_B100': 'RENDIMENTO_B100_BENCHMARK',
  'TEMPO_PADRAO_SEG': 'TEMPO_PADRAO_SEG_BENCHMARK',
  'FATOR_TEMPO': 'FATOR_TEMPO_BENCHMARK'
}, inplace = True)

df_tempos_benchmark = df_rendimento_benchmark[[
  'CD_FILIAL_BENCHMARK',	
  'ATIVIDADE',	
  'TEMPO_PADRAO_SEG_BENCHMARK',	
  'FATOR_TEMPO_BENCHMARK',	
  'RENDIMENTO_POTENCIAL_BENCHMARK',	
  'RENDIMENTO_REAL_BENCHMARK',	
  'RENDIMENTO_B100_BENCHMARK'
]].copy()

# COMMAND ----------

df_fatos_atividades = df_fatos_atividades.merge(df_tempos_benchmark, how = 'left', on = ['ATIVIDADE'])

# COMMAND ----------

colunas = ['CD_FILIAL_BENCHMARK', 'TEMPO_PADRAO_SEG_BENCHMARK',
       'FATOR_TEMPO_BENCHMARK', 'RENDIMENTO_POTENCIAL_BENCHMARK',
       'RENDIMENTO_REAL_BENCHMARK', 'RENDIMENTO_B100_BENCHMARK']

for col in colunas:
    
    df_fatos_atividades.loc[df_fatos_atividades[col].isna(), col] = df_fatos_atividades.loc[df_fatos_atividades[col].isna(), col.replace('_BENCHMARK','')]

# COMMAND ----------

# Se o tempo padrão da atividade é zero ou nulo, o tempo padrão benchmark também deverá ser, para que náo haja 
# ql benchmark calculado para atividades que não possuem ql em uma determianda filial

df_fatos_atividades.loc[
    (df_fatos_atividades['TEMPO_PADRAO_SEG'] == 0)
    |(df_fatos_atividades['TEMPO_PADRAO_SEG'].isna())
, 'TEMPO_PADRAO_SEG_BENCHMARK'] = np.nan

# COMMAND ----------

df_fatos_atividades['PECAS_HR'] = round(3600/(df_fatos_atividades['TEMPO_PADRAO_SEG'].apply(lambda x: x if x > 0 else np.nan)/df_fatos_atividades['FATOR_TEMPO'].apply(lambda x: x if x > 0 else 1)),1)

# COMMAND ----------

df_fatos_atividades['PECAS_HR_BENCHMARK'] = round(3600/(df_fatos_atividades['TEMPO_PADRAO_SEG_BENCHMARK'].apply(lambda x: x if x > 0 else np.nan)/df_fatos_atividades['FATOR_TEMPO'].apply(lambda x: x if x > 0 else 1)),1)

# COMMAND ----------

df_fatos_atividades['SIGLA_BENCHMARK'] = df_fatos_atividades['CD_FILIAL_BENCHMARK'].apply(lambda x: dict_siglas_filiais.get(x))

# COMMAND ----------

df_fatos_atividades = df_fatos_atividades[
    ~df_fatos_atividades['DT_PRODUCAO'].isna()
]

# COMMAND ----------

df_fatos_atividades_diario = df_fatos_atividades.copy()

# COMMAND ----------

df_fatos_atividades = df_fatos_atividades.groupby([
    'CD_FILIAL', 
    'ATIVIDADE', 
    'TURNO',
    'TEMPO_PADRAO_SEG', 
    'FATOR_TEMPO',
#     'QL_ADICIONAL', 
#     'MOTIVO_QL_ADICIONAL', 
    'UNIDADE_TEMPO_PADRAO',
    'GRUPO_RENDIMENTO', 
    'MES_REF',
    'CONTAGEM_TURNOS',
    'CD_FILIAL_BENCHMARK',
    'TEMPO_PADRAO_SEG_BENCHMARK',
    'FAMILIA_PARA_CONSIDERAR',
#     'FATOR_TEMPO_BENCHMARK', 
    'RENDIMENTO_POTENCIAL_BENCHMARK',
    'RENDIMENTO_REAL_BENCHMARK', 
    'RENDIMENTO_B100_BENCHMARK', 
    'PECAS_HR',
    'PECAS_HR_BENCHMARK', 
    'SIGLA_BENCHMARK',
    'RENDIMENTO_B100',
    'RENDIMENTO_POTENCIAL', 
    'RENDIMENTO_REAL',
#     'EXISTE_PLANO',
    'EXISTE_PLANO_MENSAL',
    'FAMILIA_PLANO_PRODUCAO',
    'FAMILIA_ANALITICA', 
    'FAMILIA_SINTETICA'
], dropna = False).agg({
    'QT_CONSIDERADA_MENSAL': 'mean',
}).reset_index()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Absorção dos cálculos que antes eram feitos por métricas no pwbi

# COMMAND ----------

# MAGIC %md
# MAGIC #### Parâmetros das simulações

# COMMAND ----------

df_params_horas = pd.read_excel('gs://comparativo_produtividade/inputs_comp_suinos/parametros_simulacao_suinos.xlsx', sheet_name = 'Horas trabalhadas')

df_params_suinos_abatidos = pd.read_excel('gs://comparativo_produtividade/inputs_comp_suinos/parametros_simulacao_suinos.xlsx', sheet_name = 'Suinos abatidos dia')

df_params_eficiencia_meta = pd.read_excel('gs://comparativo_produtividade/inputs_comp_suinos/parametros_simulacao_suinos.xlsx', sheet_name = 'Eficiencia Meta')

df_params = df_params_horas.merge(
    df_params_suinos_abatidos,
    how = 'left',
    on = ['CD_FILIAL']
).merge(
    df_params_eficiencia_meta,
    how = 'left',
    on = ['CD_FILIAL']
)

df_params['VELOCIDADE_ABATE'] = ((2 - df_params['EFICIENCIA_META'])*df_params['NUM_SUINOS_DIA']/(df_params['HORAS_POR_TURNO']*df_params['NUM_TURNOS_COM_ABATE'])).apply(
    lambda x: math.ceil(x)
)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Visão Mensal

# COMMAND ----------

df_fatos_atividades = df_fatos_atividades.merge(
    df_params,
    how = 'left',
    on = ['CD_FILIAL']
)

# COMMAND ----------

index_velocidade_abate =  df_fatos_atividades['UNIDADE_TEMPO_PADRAO'].isin(['Cabeça', 'Cabeça - condicional'])

eficiencia_meta = df_fatos_atividades.loc[index_velocidade_abate,'EFICIENCIA_META']

volume_suinos_dia = df_fatos_atividades.loc[index_velocidade_abate,'NUM_SUINOS_DIA']

horas_por_turno = df_fatos_atividades.loc[index_velocidade_abate,'HORAS_POR_TURNO']

contagem_turnos = df_fatos_atividades.loc[index_velocidade_abate,'CONTAGEM_TURNOS']

df_fatos_atividades.loc[index_velocidade_abate, 'VELOCIDADE_DA_LINHA'] = ((2 - eficiencia_meta)*volume_suinos_dia/(horas_por_turno*contagem_turnos)).apply(lambda x: math.ceil(x))

# COMMAND ----------

index_velocidade_outros =  df_fatos_atividades['UNIDADE_TEMPO_PADRAO'].isin([
    'Total Pacote/hr', 'Total Caixa/hr', 'Total Caixa Manual/hr','Plano de Produção', 'Caixa', 'Exceção'
])

quantidade_considerada = df_fatos_atividades.loc[index_velocidade_outros,'QT_CONSIDERADA_MENSAL'].apply(lambda x: x if x > 0 else 0)

horas_por_turno = df_fatos_atividades.loc[index_velocidade_outros,'HORAS_POR_TURNO']

df_fatos_atividades.loc[index_velocidade_outros, 'VELOCIDADE_DA_LINHA'] = (quantidade_considerada/(horas_por_turno)).apply(lambda x: math.ceil(x))

# COMMAND ----------

# O dimensionamento da Sala de cortes e da Padronização e Embalagem em São Miguel é feito utilizando 7,3hr por turno, mesmo que o abate tenha mais horas por turno

atividades_horario_diferenciado = list(df_dim_atividades.loc[
        df_dim_atividades['AREA'].isin(['Sala de Cortes', 'Padronização e Embalagem'])
    ,'ATIVIDADE']
)

index_atividades_horario_diferenciado_outros =  df_fatos_atividades[
    (df_fatos_atividades['CD_FILIAL'] == 581)
    &(df_fatos_atividades['ATIVIDADE'].isin(atividades_horario_diferenciado))
    &(
        df_fatos_atividades['UNIDADE_TEMPO_PADRAO'].isin([
            'Total Pacote/hr', 'Total Caixa/hr', 'Total Caixa Manual/hr','Plano de Produção', 'Caixa', 'Exceção'
        ])
    )
].index

quantidade_considerada = df_fatos_atividades.loc[index_atividades_horario_diferenciado_outros,'QT_CONSIDERADA_MENSAL'].apply(lambda x: x if x > 0 else 0)

horas_por_turno = df_fatos_atividades.loc[index_atividades_horario_diferenciado_outros,'HORAS_POR_TURNO']

horas_por_turno = horas_por_turno.replace(9.3, 7.3).replace(8.3, 7.3)

df_fatos_atividades.loc[index_atividades_horario_diferenciado_outros, 'VELOCIDADE_DA_LINHA'] = (quantidade_considerada/(horas_por_turno)).apply(lambda x: math.ceil(x))

# ----------------------------------------------------------------------------------------------------------------------------- #

index_atividades_horario_diferenciado_abate =  df_fatos_atividades[
    (df_fatos_atividades['CD_FILIAL'] == 581)
    &(df_fatos_atividades['ATIVIDADE'].isin(atividades_horario_diferenciado))
    &(
        df_fatos_atividades['UNIDADE_TEMPO_PADRAO'].isin([
            'Cabeça', 'Cabeça - condicional'
        ])
    )
].index

eficiencia_meta = df_fatos_atividades.loc[index_atividades_horario_diferenciado_abate,'EFICIENCIA_META']

eficiencia_meta = eficiencia_meta.replace(0.94, 0.96)

volume_suinos_dia = df_fatos_atividades.loc[index_atividades_horario_diferenciado_abate,'NUM_SUINOS_DIA']

horas_por_turno = df_fatos_atividades.loc[index_atividades_horario_diferenciado_abate,'HORAS_POR_TURNO']

horas_por_turno = horas_por_turno.replace(9.3, 7.3).replace(8.3, 7.3)

contagem_turnos = df_fatos_atividades.loc[index_atividades_horario_diferenciado_abate,'CONTAGEM_TURNOS']

df_fatos_atividades.loc[index_atividades_horario_diferenciado_abate, 'VELOCIDADE_DA_LINHA'] = ((2 - eficiencia_meta)*volume_suinos_dia/(horas_por_turno*contagem_turnos)).apply(lambda x: math.ceil(x))

# COMMAND ----------

df_fatos_atividades['UTILIZACAO_QL'] = df_fatos_atividades['VELOCIDADE_DA_LINHA']/df_fatos_atividades['PECAS_HR']

df_fatos_atividades['UTILIZACAO_QL_BENCHMARK'] = df_fatos_atividades['VELOCIDADE_DA_LINHA']/df_fatos_atividades['PECAS_HR_BENCHMARK']

# COMMAND ----------

index_ql_fixo = df_fatos_atividades['UNIDADE_TEMPO_PADRAO'].isin([
    'QL Fixo'
])

df_fatos_atividades.loc[index_ql_fixo, 'UTILIZACAO_QL'] = df_fatos_atividades.loc[index_ql_fixo, 'TEMPO_PADRAO_SEG']

df_fatos_atividades.loc[index_ql_fixo, 'UTILIZACAO_QL_BENCHMARK'] = df_fatos_atividades.loc[index_ql_fixo, 'TEMPO_PADRAO_SEG_BENCHMARK']

# COMMAND ----------

index_ql_pausa = df_fatos_atividades['UNIDADE_TEMPO_PADRAO'].isin([
    'Pausa',
    'AFF',
    'Escala'
])

df_fatos_atividades.loc[index_ql_pausa, 'UTILIZACAO_QL'] = 0

df_fatos_atividades.loc[index_ql_pausa, 'UTILIZACAO_QL_BENCHMARK'] = 0

# COMMAND ----------

df_fatos_atividades = df_fatos_atividades.merge(df_dim_atividades[['ATIVIDADE','ARREDONDAMENTO_APOS_SOMA']], how = 'left', on = ['ATIVIDADE'])

# COMMAND ----------

index_arredondamento_antes_da_soma = df_fatos_atividades['ARREDONDAMENTO_APOS_SOMA'] == 0

df_fatos_atividades.loc[
    (index_arredondamento_antes_da_soma)
    &(~(df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'QL Fixo'))
,'UTILIZACAO_QL'] = df_fatos_atividades.loc[
    index_arredondamento_antes_da_soma, 'UTILIZACAO_QL'].apply(lambda x: math.ceil(x/0.95) if x > 0 else x)

df_fatos_atividades.loc[
    (index_arredondamento_antes_da_soma)
    &(~(df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'QL Fixo'))
,'UTILIZACAO_QL_BENCHMARK'] = df_fatos_atividades.loc[
    index_arredondamento_antes_da_soma, 'UTILIZACAO_QL_BENCHMARK'].apply(lambda x: math.ceil(x/0.95) if x > 0 else x)

# COMMAND ----------

index_cabeca_condicional = df_fatos_atividades['UNIDADE_TEMPO_PADRAO'] == 'Cabeça - condicional'

df_fatos_atividades.loc[
    (index_cabeca_condicional)
    &(df_fatos_atividades['EXISTE_PLANO_MENSAL'].isna()) 
,'UTILIZACAO_QL'] = 0

df_fatos_atividades.loc[
    (index_cabeca_condicional)
    &(df_fatos_atividades['EXISTE_PLANO_MENSAL'].isna()) 
,'UTILIZACAO_QL_BENCHMARK'] = 0


# COMMAND ----------

# MAGIC %md
# MAGIC #### Preço

# COMMAND ----------

query_preco_canal = f"""
SELECT
  DADOS_TABELA_PRECO.CD_EMPRESA,
  EMPRESA.NM_EMPRESA,
  DADOS_TABELA_PRECO.CD_TABELA_PRECO,
  DADOS_TABELA_PRECO.CD_TIPO_TABELA_PRECO,
  DADOS_TABELA_PRECO.CD_DIVISAO_EMPRESARIAL,
  DIVISAO_EMPRESARIAL.NM_DIVISAO_EMPRESARIAL,
  DADOS_TABELA_PRECO.CD_CANAL_PRECO,
  CASE 
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 1 THEN "DISTRIBUIDOR"   
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 2 THEN "ATACADO"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 3 THEN "AS REGIONAL"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 7 THEN "VAREJO TRADICIONAL"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 11 THEN "VIZINHANÇA"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 14 THEN "FOOD SERVICE"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 17 THEN "FOOD SERVICE - GRANDES TRANSFORMADORES"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 18 THEN "FOOD SERVICE - CONTAS ESPECIAIS"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 12 THEN "HS+"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 13 THEN "HS+ ATACADO"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 10 THEN "VAREJO NACIONAL"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 15 THEN "ATACADO NACIONAL"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 22 THEN "ATACADO REGIONAL"  
    WHEN DADOS_TABELA_PRECO.CD_CANAL_PRECO = 23 THEN "HS" 
    ELSE NULL END NM_CANAL_PRECO,
  DADOS_TABELA_PRECO.CD_GRANDE_REDE,
  DADOS_TABELA_PRECO.CD_ITEM,
  ITEM.NM_ITEM,
  DADOS_TABELA_PRECO.VL_TIPO_PRECO,
  DADOS_TABELA_PRECO.DT_INI_VIGENCIA,
  DADOS_TABELA_PRECO.DT_FIM_VIGENCIA
FROM
  `seara-analytics-prod.stg_erp_seara.dados_tabela_preco` DADOS_TABELA_PRECO
INNER JOIN  `seara-analytics-prod.stg_erp_seara.empresa` EMPRESA
        ON  ( DADOS_TABELA_PRECO.CD_EMPRESA=EMPRESA.CD_EMPRESA  )
INNER JOIN  `seara-analytics-prod.stg_erp_seara.divisao_empresarial` DIVISAO_EMPRESARIAL
        ON  ( DADOS_TABELA_PRECO.CD_DIVISAO_EMPRESARIAL=DIVISAO_EMPRESARIAL.CD_DIVISAO_EMPRESARIAL  )
LEFT JOIN   `seara-analytics-prod.stg_erp_seara.item` ITEM
        ON  ( DADOS_TABELA_PRECO.CD_ITEM=ITEM.CD_ITEM  )
WHERE
    DADOS_TABELA_PRECO.CD_TIPO_TABELA_PRECO  =  1 AND
    -- DADOS_TABELA_PRECO.CD_ITEM = 40101 AND
    -- DADOS_TABELA_PRECO.CD_TABELA_PRECO IN (380,395) AND
    -- DADOS_TABELA_PRECO.CD_ITEM = 571954 AND
    -- PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DADOS_TABELA_PRECO.DT_INI_VIGENCIA) >= '2021-08-01' AND
    PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DADOS_TABELA_PRECO.DT_FIM_VIGENCIA) >= '{data_inicio_mes}'
ORDER BY CD_EMPRESA, CD_CANAL_PRECO
"""

df_preco = client.query(query_preco_canal).to_dataframe()

print(df_preco.shape)

df_preco = df_preco[['CD_ITEM','VL_TIPO_PRECO']]

df_preco['VL_TIPO_PRECO'] = df_preco['VL_TIPO_PRECO'].astype(float)

df_preco = df_preco.groupby(['CD_ITEM']).mean().reset_index()

# df_preco.head()

# COMMAND ----------

df_preco = df_plano_mensal.groupby(
    ['CD_FILIAL','CD_ITEM','MES_REF','DS_FAM_ANALITICA_PRO','DS_FAM_SINTETICA_PRO','FAMILIA_PLANO_PRODUCAO']                     
).agg({
    'QT_PESO_DIARIZADO': 'mean'
}).reset_index().merge(
    de_para_fam_analitica_rendimento,
    how = 'left',
    on = ['DS_FAM_ANALITICA_PRO']
).merge(
    df_preco,
    how = 'left',
    on = ['CD_ITEM']
)

# COMMAND ----------

df_preco.loc[df_preco['VL_TIPO_PRECO'].isna(), 'QT_PESO_DIARIZADO'] = np.nan

# COMMAND ----------

df_preco['PRECO_PONDERADO'] = df_preco['VL_TIPO_PRECO']*df_preco['QT_PESO_DIARIZADO']

# COMMAND ----------

df_preco_familia_plano = df_preco.groupby(['CD_FILIAL','FAMILIA_PLANO_PRODUCAO']).agg({
    'QT_PESO_DIARIZADO': 'sum',
    'PRECO_PONDERADO': 'sum'
}).reset_index()

df_preco_familia_plano['PRECO_MEDIO'] = df_preco_familia_plano['PRECO_PONDERADO']/df_preco_familia_plano['QT_PESO_DIARIZADO']

df_preco_familia_plano.drop(columns=['QT_PESO_DIARIZADO','PRECO_PONDERADO'], inplace = True)

# COMMAND ----------

df_preco_familia_analitica = df_preco.groupby(['CD_FILIAL','DS_FAM_ANALITICA_PRO']).agg({
    'QT_PESO_DIARIZADO': 'sum',
    'PRECO_PONDERADO': 'sum'
}).reset_index()

df_preco_familia_analitica['PRECO_MEDIO'] = df_preco_familia_analitica['PRECO_PONDERADO']/df_preco_familia_analitica['QT_PESO_DIARIZADO']

df_preco_familia_analitica.drop(columns=['QT_PESO_DIARIZADO','PRECO_PONDERADO'], inplace = True)

df_preco_familia_analitica.rename(columns={'DS_FAM_ANALITICA_PRO': 'FAMILIA_ANALITICA'}, inplace = True)

# COMMAND ----------

df_preco_familia_sintetica = df_preco.groupby(['CD_FILIAL','DS_FAM_SINTETICA_PRO']).agg({
    'QT_PESO_DIARIZADO': 'sum',
    'PRECO_PONDERADO': 'sum'
}).reset_index()

df_preco_familia_sintetica['PRECO_MEDIO'] = df_preco_familia_sintetica['PRECO_PONDERADO']/df_preco_familia_sintetica['QT_PESO_DIARIZADO']

df_preco_familia_sintetica.drop(columns=['QT_PESO_DIARIZADO','PRECO_PONDERADO'], inplace = True)

df_preco_familia_sintetica.rename(columns={'DS_FAM_SINTETICA_PRO': 'FAMILIA_SINTETICA'}, inplace = True)

# COMMAND ----------

df_fatos_atividades_com_preco = df_fatos_atividades[
    df_fatos_atividades['UNIDADE_TEMPO_PADRAO'].isin([
#         'AFF', 
#         'Total Pacote/hr', 
#         'Total Caixa/hr', 
        'Plano de Produção',
#         'QL Fixo', 
#         'Cabeça', 
#         'Cabeça - condicional', 
        'Caixa', 
#         'Exceção',
#         'Escala', 
#         'Pausa'
    ])
]

# COMMAND ----------

df_fatos_atividades_sem_preco = df_fatos_atividades[
    df_fatos_atividades['UNIDADE_TEMPO_PADRAO'].isin([
        'AFF', 
        'Total Pacote/hr', 
        'Total Caixa/hr',
        'Total Caixa Manual/hr',
#         'Plano de Produção',
        'QL Fixo', 
        'Cabeça', 
        'Cabeça - condicional', 
#         'Caixa', 
        'Exceção',
        'Escala', 
        'Pausa',
        'Repouso'
    ])
]

# COMMAND ----------

df_fatos_atividades_com_preco_familia_plano = df_fatos_atividades_com_preco[
    df_fatos_atividades_com_preco['FAMILIA_PARA_CONSIDERAR'] == 'Familia Plano'
]

df_fatos_atividades_com_preco_familia_analitica = df_fatos_atividades_com_preco[
    df_fatos_atividades_com_preco['FAMILIA_PARA_CONSIDERAR'] == 'Familia Analitica'
]

df_fatos_atividades_com_preco_familia_sintetica = df_fatos_atividades_com_preco[
    df_fatos_atividades_com_preco['FAMILIA_PARA_CONSIDERAR'] == 'Familia Sintetica'
]

# COMMAND ----------

df_fatos_atividades_com_preco_familia_plano = df_fatos_atividades_com_preco_familia_plano.merge(
    df_preco_familia_plano,
    how = 'left',
    on = ['CD_FILIAL','FAMILIA_PLANO_PRODUCAO']
)

df_fatos_atividades_com_preco_familia_sintetica = df_fatos_atividades_com_preco_familia_sintetica.merge(
    df_preco_familia_sintetica,
    how = 'left',
    on = ['CD_FILIAL','FAMILIA_SINTETICA']
)

df_fatos_atividades_com_preco_familia_analitica = df_fatos_atividades_com_preco_familia_analitica.merge(
    df_preco_familia_analitica,
    how = 'left',
    on = ['CD_FILIAL','FAMILIA_ANALITICA']
)

# COMMAND ----------

df_fatos_atividades_com_preco = pd.concat([
    df_fatos_atividades_com_preco_familia_plano,
    df_fatos_atividades_com_preco_familia_analitica,
    df_fatos_atividades_com_preco_familia_sintetica
])

# COMMAND ----------

df_fatos_atividades = pd.concat([df_fatos_atividades_com_preco, df_fatos_atividades_sem_preco])

# COMMAND ----------

#df_fatos_atividades.to_excel('fatos_atividades.xlsx', index = False)

# COMMAND ----------

# Adiciona o ql para o repouso térmico

atividades_paletizacao = df_dim_atividades[
    df_dim_atividades['SUBAREA'] == 'Paletização'
]['ATIVIDADE'].unique()

df_fatos_atividades_repouso_aux = df_fatos_atividades[
    (df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao))
].copy()

df_fatos_atividades_repouso_aux['UTILIZACAO_QL'] = df_fatos_atividades_repouso_aux['UTILIZACAO_QL'].fillna(0).apply(lambda x: math.ceil(x))

df_fatos_atividades_repouso_aux = df_fatos_atividades_repouso_aux.groupby([
    'CD_FILIAL',
    'TURNO',
    'MES_REF',
    'HORAS_POR_TURNO', 
    'NUM_SUINOS_DIA', 
    'EFICIENCIA_META'
]).agg({'UTILIZACAO_QL': 'sum'}).reset_index()

# Repouso térmico é 20% do total de ql da subarea
df_fatos_atividades_repouso_aux.loc[df_fatos_atividades_repouso_aux['CD_FILIAL'] != 136, 'UTILIZACAO_QL'] = (df_fatos_atividades_repouso_aux.loc[
    df_fatos_atividades_repouso_aux['CD_FILIAL'] != 136, 'UTILIZACAO_QL']*0.2).apply(lambda x: math.ceil(x))

df_fatos_atividades_repouso_aux.loc[df_fatos_atividades_repouso_aux['CD_FILIAL'] == 136, 'UTILIZACAO_QL'] = (df_fatos_atividades_repouso_aux.loc[
    df_fatos_atividades_repouso_aux['CD_FILIAL'] == 136, 'UTILIZACAO_QL']*0.5).apply(lambda x: math.ceil(x))

df_fatos_atividades_sem_repouso_termico = df_fatos_atividades[
    df_fatos_atividades['ATIVIDADE'] != "Repouso Térmico - Paletização"
].copy()

df_fatos_atividades_com_repouso_termico = df_fatos_atividades[
    df_fatos_atividades['ATIVIDADE'] == "Repouso Térmico - Paletização"
].copy()

df_fatos_atividades_com_repouso_termico.drop(columns=['UTILIZACAO_QL'], inplace = True)

df_fatos_atividades_com_repouso_termico = df_fatos_atividades_com_repouso_termico.merge(
    df_fatos_atividades_repouso_aux,
    how = 'left',
    on = [
        'CD_FILIAL',
        'TURNO',
        'MES_REF',
        'HORAS_POR_TURNO', 
        'NUM_SUINOS_DIA', 
        'EFICIENCIA_META'
    ]
)

# Faz o mesmo para os qls benchmark

df_fatos_atividades_repouso_aux = df_fatos_atividades[
    (df_fatos_atividades['ATIVIDADE'].isin(atividades_paletizacao))
].copy()

df_fatos_atividades_repouso_aux['UTILIZACAO_QL_BENCHMARK'] = df_fatos_atividades_repouso_aux['UTILIZACAO_QL_BENCHMARK'].fillna(0).apply(lambda x: math.ceil(x))

df_fatos_atividades_repouso_aux = df_fatos_atividades_repouso_aux.groupby([
    'CD_FILIAL',
    'TURNO',
    'MES_REF',
    'HORAS_POR_TURNO', 
    'NUM_SUINOS_DIA', 
    'EFICIENCIA_META'
]).agg({'UTILIZACAO_QL_BENCHMARK': 'sum'}).reset_index()

# Repouso térmico é 20% do total de ql da subarea
df_fatos_atividades_repouso_aux.loc[df_fatos_atividades_repouso_aux['CD_FILIAL'] != 136, 'UTILIZACAO_QL_BENCHMARK'] = (df_fatos_atividades_repouso_aux.loc[
    df_fatos_atividades_repouso_aux['CD_FILIAL'] != 136, 'UTILIZACAO_QL_BENCHMARK']*0.2).apply(lambda x: math.ceil(x))

df_fatos_atividades_repouso_aux.loc[df_fatos_atividades_repouso_aux['CD_FILIAL'] == 136, 'UTILIZACAO_QL_BENCHMARK'] = (df_fatos_atividades_repouso_aux.loc[
    df_fatos_atividades_repouso_aux['CD_FILIAL'] == 136, 'UTILIZACAO_QL_BENCHMARK']*0.5).apply(lambda x: math.ceil(x))

df_fatos_atividades_com_repouso_termico.drop(columns=['UTILIZACAO_QL_BENCHMARK'], inplace = True)

df_fatos_atividades_com_repouso_termico = df_fatos_atividades_com_repouso_termico.merge(
    df_fatos_atividades_repouso_aux,
    how = 'left',
    on = [
        'CD_FILIAL',
        'TURNO',
        'MES_REF',
        'HORAS_POR_TURNO', 
        'NUM_SUINOS_DIA', 
        'EFICIENCIA_META'
    ]
)

df_fatos_atividades = pd.concat([df_fatos_atividades_com_repouso_termico,  df_fatos_atividades_sem_repouso_termico])

# COMMAND ----------

# df_fatos_atividades[[
#     'CD_FILIAL',
#     'ATIVIDADE',
#     'TURNO',
#     'TEMPO_PADRAO_SEG',
#     'FATOR_TEMPO',
#     'UNIDADE_TEMPO_PADRAO',
#     'PECAS_HR',
# #     'GRUPO_RENDIMENTO',
#     'MES_REF',
# #     'CD_FILIAL_BENCHMARK',
# #     'TEMPO_PADRAO_SEG_BENCHMARK',
# #     'RENDIMENTO_POTENCIAL_BENCHMARK',
# #     'RENDIMENTO_REAL_BENCHMARK',
# #     'RENDIMENTO_B100_BENCHMARK',
# #     'SIGLA_BENCHMARK',
# #     'RENDIMENTO_B100',
# #     'RENDIMENTO_POTENCIAL',
# #     'RENDIMENTO_REAL',
#     'QT_CONSIDERADA_MENSAL',
#     'HORAS_POR_TURNO',
#     'NUM_SUINOS_DIA',
#     'EFICIENCIA_META',
#     'VELOCIDADE_DA_LINHA',
#     'VELOCIDADE_ABATE',
#     'PRECO_MEDIO',
#     'UTILIZACAO_QL',
# #     'UTILIZACAO_QL_BENCHMARK'
# ]].to_excel('fatos_atividades.xlsx', index = False)

# COMMAND ----------

df_fatos_atividades.drop(columns=[
    'CONTAGEM_TURNOS',
    'PECAS_HR',
    'PECAS_HR_BENCHMARK',
    'EXISTE_PLANO_MENSAL',
    'NUM_TURNOS_COM_ABATE',
    'VELOCIDADE_DA_LINHA',
    'ARREDONDAMENTO_APOS_SOMA',
    'FAMILIA_PARA_CONSIDERAR',
    'FAMILIA_PLANO_PRODUCAO',
    'FAMILIA_ANALITICA', 
    'FAMILIA_SINTETICA'
], inplace = True)

# COMMAND ----------

# df_fatos_atividades[
#     (df_fatos_atividades['CD_FILIAL'] == 902)
# #     &(df_fatos_atividades['ATIVIDADE'].isin([
# #         "Selar pacote - Termoformados"
# #     ]))
# ].head()

# COMMAND ----------

carrega_tabela_bq("dmdo-gastos-305013", "dmdo_dataset", "dim_atividade", df_dim_atividades, 'WRITE_TRUNCATE')

# COMMAND ----------

carrega_tabela_bq("dmdo-gastos-305013", "dmdo_dataset", "fato_atividade", df_fatos_atividades, 'WRITE_TRUNCATE')

# COMMAND ----------

# MAGIC %md
# MAGIC #### Visão Resumo Executivo

# COMMAND ----------

df_resumo = df_fatos_atividades.copy()

df_resumo.reset_index(inplace = True, drop = True)

# COMMAND ----------

params_resumo = pd.read_excel('gs://comparativo_produtividade/inputs_comp_suinos/parametros_simulacao_suinos.xlsx', sheet_name = 'Parametros Resumo')

# COMMAND ----------

dict_params_resumo = {}

for filial in params_resumo['CD_FILIAL'].unique():

    dict_params_resumo['index_'+str(filial)] = list(df_resumo[
        (df_resumo['CD_FILIAL'] == filial)
        &(df_resumo['HORAS_POR_TURNO'] == params_resumo.loc[params_resumo['CD_FILIAL'] == filial, 'HORAS_POR_TURNO'].item())
        &(df_resumo['EFICIENCIA_META'] == params_resumo.loc[params_resumo['CD_FILIAL'] == filial, 'EFICIENCIA_META'].item())
        &(df_resumo['NUM_SUINOS_DIA'] == params_resumo.loc[params_resumo['CD_FILIAL'] == filial, 'NUM_SUINOS_DIA'].item())
    ].index)

# COMMAND ----------

df_resumo = df_resumo.loc[
    dict_params_resumo['index_136'] + dict_params_resumo['index_228'] + dict_params_resumo['index_475'] + dict_params_resumo['index_581']
    + dict_params_resumo['index_633'] + dict_params_resumo['index_827'] + dict_params_resumo['index_902'] + dict_params_resumo['index_918']
]

# COMMAND ----------

df_resumo = df_resumo.merge(df_dim_atividades[['ATIVIDADE','AREA','SUBAREA','ETAPA', 'ORDEM_AREA']], how = 'left', on = ['ATIVIDADE'])

# COMMAND ----------

df_resumo = df_resumo.groupby(['MES_REF','CD_FILIAL','AREA','SUBAREA','ETAPA', 'TURNO']).agg({
    'UTILIZACAO_QL': 'sum',
    'UTILIZACAO_QL_BENCHMARK': 'sum',
    'ORDEM_AREA': 'min'
}).reset_index()

# COMMAND ----------

df_resumo['UTILIZACAO_QL'] = df_resumo['UTILIZACAO_QL'].apply(lambda x: math.ceil(x))

df_resumo['UTILIZACAO_QL_BENCHMARK'] = df_resumo['UTILIZACAO_QL_BENCHMARK'].apply(lambda x: math.ceil(x))

# COMMAND ----------

df_resumo = df_resumo.groupby(['MES_REF','CD_FILIAL','AREA','SUBAREA','TURNO']).agg({
    'UTILIZACAO_QL': 'sum',
    'UTILIZACAO_QL_BENCHMARK': 'sum',
    'ORDEM_AREA': 'min'
}).reset_index()

# COMMAND ----------

df_resumo = df_resumo.merge(params_resumo[['CD_FILIAL','PAUSAS','ESCALA_MOVEL','ABSENTEISMO','FERIAS']], how = 'left', on = ['CD_FILIAL'])

# COMMAND ----------

df_resumo['UTILIZACAO_QL_PAUSA'] = (df_resumo['UTILIZACAO_QL']*df_resumo['PAUSAS']).apply(lambda x: math.ceil(x))

# O QL de repouso térmico já é calculado previamente e, portanto, para a subarea de paletização não é necessário calcular ql pausa
df_resumo.loc[df_resumo['SUBAREA'].isin(['Paletização']), 'UTILIZACAO_QL_PAUSA'] = 0

df_resumo['UTILIZACAO_QL_ESCALA'] = ((df_resumo['UTILIZACAO_QL'] + df_resumo['UTILIZACAO_QL_PAUSA'])*df_resumo['ESCALA_MOVEL']).apply(lambda x: math.ceil(x))

df_resumo['UTILIZACAO_QL_AFF'] = (
    (df_resumo['UTILIZACAO_QL'] + df_resumo['UTILIZACAO_QL_PAUSA'] + df_resumo['UTILIZACAO_QL_ESCALA'])*(df_resumo['ABSENTEISMO'] + df_resumo['FERIAS'])
).apply(lambda x: round(x,0))

# As aeas de Congelamento e Fábrica de Farinhas não possuem cálculo de aff, pausas etc
df_resumo.loc[df_resumo['AREA'].isin(['Congelamento', 'Fábrica de Farinhas']), 'UTILIZACAO_QL_PAUSA'] = \
    df_resumo.loc[df_resumo['AREA'].isin(['Congelamento', 'Fábrica de Farinhas']), 'UTILIZACAO_QL_ESCALA'] =  \
        df_resumo.loc[df_resumo['AREA'].isin(['Congelamento', 'Fábrica de Farinhas']), 'UTILIZACAO_QL_AFF'] = 0

df_resumo['UTILIZACAO_QL'] = df_resumo['UTILIZACAO_QL'] + df_resumo['UTILIZACAO_QL_PAUSA'] + df_resumo['UTILIZACAO_QL_ESCALA'] + df_resumo['UTILIZACAO_QL_AFF']

# COMMAND ----------

df_resumo['UTILIZACAO_QL_PAUSA_BENCHMARK'] = (df_resumo['UTILIZACAO_QL_BENCHMARK']*df_resumo['PAUSAS']).apply(lambda x: math.ceil(x))

# O QL de repouso térmico já é calculado previamente e, portanto, para a subarea de paletização não é necessário calcular ql pausa
df_resumo.loc[df_resumo['SUBAREA'].isin(['Paletização']), 'UTILIZACAO_QL_PAUSA_BENCHMARK'] = 0

df_resumo['UTILIZACAO_QL_ESCALA_BENCHMARK'] = ((df_resumo['UTILIZACAO_QL_BENCHMARK'] + df_resumo['UTILIZACAO_QL_PAUSA_BENCHMARK'])*df_resumo['ESCALA_MOVEL']).apply(lambda x: math.ceil(x))

df_resumo['UTILIZACAO_QL_AFF_BENCHMARK'] = (
    (df_resumo['UTILIZACAO_QL_BENCHMARK'] + df_resumo['UTILIZACAO_QL_PAUSA_BENCHMARK'] + df_resumo['UTILIZACAO_QL_ESCALA_BENCHMARK'])*(df_resumo['ABSENTEISMO'] + df_resumo['FERIAS'])
).apply(lambda x: round(x,0))

# As aeas de Congelamento e Fábrica de Farinhas não possuem cálculo de aff, pausas etc

df_resumo.loc[df_resumo['AREA'].isin(['Congelamento', 'Fábrica de Farinhas']), 'UTILIZACAO_QL_PAUSA_BENCHMARK'] = \
    df_resumo.loc[df_resumo['AREA'].isin(['Congelamento', 'Fábrica de Farinhas']), 'UTILIZACAO_QL_ESCALA_BENCHMARK'] =  \
        df_resumo.loc[df_resumo['AREA'].isin(['Congelamento', 'Fábrica de Farinhas']), 'UTILIZACAO_QL_AFF_BENCHMARK'] = 0

df_resumo['UTILIZACAO_QL_BENCHMARK'] = df_resumo['UTILIZACAO_QL_BENCHMARK'] + df_resumo['UTILIZACAO_QL_PAUSA_BENCHMARK'] + df_resumo['UTILIZACAO_QL_ESCALA_BENCHMARK'] + df_resumo['UTILIZACAO_QL_AFF_BENCHMARK']

# COMMAND ----------

df_resumo = df_resumo.groupby(['MES_REF', 'CD_FILIAL', 'AREA']).agg({
    'UTILIZACAO_QL': 'sum',
    'UTILIZACAO_QL_BENCHMARK': 'sum',
    'ORDEM_AREA': 'min'
}).reset_index()

# COMMAND ----------

carrega_tabela_bq("dmdo-gastos-305013", "dmdo_dataset", "resumo_fato_atividade", df_resumo, 'WRITE_TRUNCATE')

# COMMAND ----------

# MAGIC %md
# MAGIC #### Visão Diária

# COMMAND ----------

index_trp = list(df_params[
    (df_params['CD_FILIAL'] == 918)
    &(df_params['HORAS_POR_TURNO'] == 7.3)
    &(df_params['EFICIENCIA_META'] == 0.98)
    &(df_params['NUM_SUINOS_DIA'] == 2600)
].index)

index_cbi = list(df_params[
    (df_params['CD_FILIAL'] == 902)
    &(df_params['HORAS_POR_TURNO'] == 7.3)
    &(df_params['EFICIENCIA_META'] == 0.96)
    &(df_params['NUM_SUINOS_DIA'] == 1800)
].index)

index_anr = list(df_params[
    (df_params['CD_FILIAL'] == 827)
    &(df_params['HORAS_POR_TURNO'] == 7.3)
    &(df_params['EFICIENCIA_META'] == 0.96)
    &(df_params['NUM_SUINOS_DIA'] == 3480)
].index)

index_its = list(df_params[
    (df_params['CD_FILIAL'] == 633)
    &(df_params['HORAS_POR_TURNO'] == 7.3)
    &(df_params['EFICIENCIA_META'] == 0.95)
    &(df_params['NUM_SUINOS_DIA'] == 2000)
].index)

index_smo = list(df_params[
    (df_params['CD_FILIAL'] == 581)
    &(df_params['HORAS_POR_TURNO'] == 9.3)
    &(df_params['EFICIENCIA_META'] == 0.94)
    &(df_params['NUM_SUINOS_DIA'] == 2500)
].index)

index_sbi = list(df_params[
    (df_params['CD_FILIAL'] == 475)
    &(df_params['HORAS_POR_TURNO'] == 7.3)
    &(df_params['EFICIENCIA_META'] == 0.96)
    &(df_params['NUM_SUINOS_DIA'] == 2800)
].index)

index_ddo = list(df_params[
    (df_params['CD_FILIAL'] == 228)
    &(df_params['HORAS_POR_TURNO'] == 6.3)
    &(df_params['EFICIENCIA_META'] == 0.95)
    &(df_params['NUM_SUINOS_DIA'] == 5000)
].index)

index_sea = list(df_params[
    (df_params['CD_FILIAL'] == 136)
    &(df_params['HORAS_POR_TURNO'] == 7.3)
    &(df_params['EFICIENCIA_META'] == 0.96)
    &(df_params['NUM_SUINOS_DIA'] == 5450)
].index)

df_params_diario = df_params.loc[
    index_trp + index_cbi + index_anr + index_its + index_smo + index_sbi + index_ddo + index_sea
]

# COMMAND ----------

df_fatos_atividades_diario = df_fatos_atividades_diario.merge(
    df_params_diario,
    how = 'left',
    on = ['CD_FILIAL']
)

# COMMAND ----------

index_velocidade_abate =  df_fatos_atividades_diario['UNIDADE_TEMPO_PADRAO'].isin(['Cabeça', 'Cabeça - condicional'])

eficiencia_meta = df_fatos_atividades_diario.loc[index_velocidade_abate,'EFICIENCIA_META']

volume_suinos_dia = df_fatos_atividades_diario.loc[index_velocidade_abate,'NUM_SUINOS_DIA']

horas_por_turno = df_fatos_atividades_diario.loc[index_velocidade_abate,'HORAS_POR_TURNO']

contagem_turnos = df_fatos_atividades_diario.loc[index_velocidade_abate,'CONTAGEM_TURNOS']

df_fatos_atividades_diario.loc[index_velocidade_abate, 'VELOCIDADE_DA_LINHA'] = ((2 - eficiencia_meta)*volume_suinos_dia/(horas_por_turno*contagem_turnos)).apply(lambda x: math.ceil(x))

# COMMAND ----------

index_velocidade_outros =  df_fatos_atividades_diario['UNIDADE_TEMPO_PADRAO'].isin([
    'Total Pacote/hr', 'Total Caixa/hr', 'Total Caixa Manual/hr', 'Plano de Produção', 'Caixa', 'Exceção'
])

quantidade_considerada = df_fatos_atividades_diario.loc[index_velocidade_outros,'QT_CONSIDERADA'].apply(lambda x: x if x > 0 else 0)

horas_por_turno = df_fatos_atividades_diario.loc[index_velocidade_outros,'HORAS_POR_TURNO']

df_fatos_atividades_diario.loc[index_velocidade_outros, 'VELOCIDADE_DA_LINHA'] = (quantidade_considerada/(horas_por_turno)).apply(lambda x: math.ceil(x))

# COMMAND ----------

df_fatos_atividades_diario['UTILIZACAO_QL'] = df_fatos_atividades_diario['VELOCIDADE_DA_LINHA']/df_fatos_atividades_diario['PECAS_HR']

df_fatos_atividades_diario['UTILIZACAO_QL_BENCHMARK'] = df_fatos_atividades_diario['VELOCIDADE_DA_LINHA']/df_fatos_atividades_diario['PECAS_HR_BENCHMARK']

# COMMAND ----------

index_ql_fixo = df_fatos_atividades_diario['UNIDADE_TEMPO_PADRAO'].isin([
    'QL Fixo'
])

df_fatos_atividades_diario.loc[index_ql_fixo, 'UTILIZACAO_QL'] = df_fatos_atividades_diario.loc[index_ql_fixo, 'TEMPO_PADRAO_SEG']

df_fatos_atividades_diario.loc[index_ql_fixo, 'UTILIZACAO_QL_BENCHMARK'] = df_fatos_atividades_diario.loc[index_ql_fixo, 'TEMPO_PADRAO_SEG_BENCHMARK']

# COMMAND ----------

index_ql_pausa = df_fatos_atividades_diario['UNIDADE_TEMPO_PADRAO'].isin([
    'Pausa',
    'AFF',
    'Escala'
])

df_fatos_atividades_diario.loc[index_ql_pausa, 'UTILIZACAO_QL'] = 0

df_fatos_atividades_diario.loc[index_ql_pausa, 'UTILIZACAO_QL_BENCHMARK'] = 0

# COMMAND ----------

df_fatos_atividades_diario = df_fatos_atividades_diario.merge(df_dim_atividades[['ATIVIDADE','ARREDONDAMENTO_APOS_SOMA']], how = 'left', on = ['ATIVIDADE'])

# COMMAND ----------

index_arredondamento_antes_da_soma = df_fatos_atividades_diario['ARREDONDAMENTO_APOS_SOMA'] == 0

df_fatos_atividades_diario.loc[
    (index_arredondamento_antes_da_soma)
    &(~(df_fatos_atividades_diario['UNIDADE_TEMPO_PADRAO'] == 'QL Fixo'))
,'UTILIZACAO_QL'] = df_fatos_atividades_diario.loc[
    index_arredondamento_antes_da_soma, 'UTILIZACAO_QL'].apply(lambda x: math.ceil(x/0.95) if x > 0 else x)

df_fatos_atividades_diario.loc[
    (index_arredondamento_antes_da_soma)
    &(~(df_fatos_atividades_diario['UNIDADE_TEMPO_PADRAO'] == 'QL Fixo'))
,'UTILIZACAO_QL_BENCHMARK'] = df_fatos_atividades_diario.loc[
    index_arredondamento_antes_da_soma, 'UTILIZACAO_QL_BENCHMARK'].apply(lambda x: math.ceil(x/0.95) if x > 0 else x)

# COMMAND ----------

index_cabeca_condicional = df_fatos_atividades_diario['UNIDADE_TEMPO_PADRAO'] == 'Cabeça - condicional'

df_fatos_atividades_diario.loc[
    (index_cabeca_condicional)
    &(df_fatos_atividades_diario['EXISTE_PLANO'].isna()) 
,'UTILIZACAO_QL'] = 0

df_fatos_atividades_diario.loc[
    (index_cabeca_condicional)
    &(df_fatos_atividades_diario['EXISTE_PLANO'].isna()) 
,'UTILIZACAO_QL_BENCHMARK'] = 0


# COMMAND ----------

#df_fatos_atividades_diario.to_excel('fatos_atividades_diario.xlsx', index = False)

# COMMAND ----------

df_fatos_atividades_diario = df_fatos_atividades_diario[[
    'CD_FILIAL', 
    'ATIVIDADE', 
    'TURNO', 
#     'TEMPO_PADRAO_SEG', 
#     'FATOR_TEMPO',
#     'QL_ADICIONAL',
#     'MOTIVO_QL_ADICIONAL',
#     'UNIDADE_TEMPO_PADRAO',
#     'GRUPO_RENDIMENTO', 
#     'RENDIMENTO_POTENCIAL', 'RENDIMENTO_REAL',
#     'RENDIMENTO_B100', 
#     'MES_REF',
    'DT_PRODUCAO',
    'HORAS_POR_TURNO',
    'NUM_SUINOS_DIA',
    'EFICIENCIA_META',
#     'QT_CONSIDERADA',
#     'EXISTE_PLANO', 
#     'QT_CONSIDERADA_MENSAL', 
#     'EXISTE_PLANO_MENSAL',
#     'CONTAGEM_TURNOS', 
#     'CD_FILIAL_BENCHMARK', 
#     'TEMPO_PADRAO_SEG_BENCHMARK',
#     'FATOR_TEMPO_BENCHMARK', 
#     'RENDIMENTO_POTENCIAL_BENCHMARK',
#     'RENDIMENTO_REAL_BENCHMARK', 
#     'RENDIMENTO_B100_BENCHMARK',
#     'PECAS_HR',
#     'PECAS_HR_BENCHMARK', 
#     'SIGLA_BENCHMARK',
    'UTILIZACAO_QL'
]]

# COMMAND ----------

# MAGIC %md
# MAGIC #### Escrevendo as tabelas no BQ

# COMMAND ----------

carrega_tabela_bq("dmdo-gastos-305013", "dmdo_dataset", "dim_filiais", df_filiais, 'WRITE_TRUNCATE')

# COMMAND ----------

carrega_tabela_bq("dmdo-gastos-305013", "dmdo_dataset", "dmdo_resumo_volumes_producao", df_resumo_volumes, 'WRITE_TRUNCATE')

# COMMAND ----------

carrega_tabela_bq("dmdo-gastos-305013", "dmdo_dataset", "fato_atividade_diario", df_fatos_atividades_diario, 'WRITE_TRUNCATE')

# COMMAND ----------

df_plano_mensal_copy['NM_MERCADO'] = df_plano_mensal_copy['NM_MERCADO'].astype(str)

carrega_tabela_bq("dmdo-gastos-305013", "dmdo_dataset", "dmdo_plano_mensal", df_plano_mensal_copy, 'WRITE_TRUNCATE')

# COMMAND ----------

carrega_tabela_bq("dmdo-gastos-305013", "dmdo_dataset", "dmdo_parametros_utilizados", params_resumo, 'WRITE_TRUNCATE')

# COMMAND ----------

df_plano_mensal[
    df_plano_mensal['FAMILIA_PLANO_PRODUCAO'].isna()
].merge(
    ITEM[['CD_ITEM','NM_CLASSE','NM_GRUPO','NM_SUBGRUPO']],
    how = 'left',
    on = ['CD_ITEM']
)[['CD_FILIAL',
   'CD_ITEM',
   'NM_ITEM',
   'FILTRO_ITEM',
   'DS_FAM_SINTETICA_PRO',
   'DS_FAM_ANALITICA_PRO',
   'FAMILIA_PLANO_PRODUCAO',
   'NM_CLASSE',
   'NM_GRUPO',
   'NM_SUBGRUPO'
  ]].drop_duplicates().to_excel('gs://comparativo_produtividade/itens_sem_plano_dmdo.xlsx', index = False)
