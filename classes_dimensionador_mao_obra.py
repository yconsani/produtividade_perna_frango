# Databricks notebook source
import pandas as pd

df = pd.read_csv('gs://parametros_fixos/Reta Produtividade.csv', sep=';', encoding = "ISO-8859-1")

df = df.T
df.reset_index(drop = False, inplace = True)
df = df.rename(columns=df.iloc[0])
reta_produtividade = df.drop(df.index[0])

for coluna in reta_produtividade.columns[1:]:
    reta_produtividade[coluna] = 0.01*reta_produtividade[coluna].apply(lambda x: float(x.replace(',','.').replace('%','')))

reta_produtividade['Faixa'] = reta_produtividade['Faixa'].astype(int)
reta_produtividade['Faixa_ant'] = reta_produtividade['Faixa'].shift(1)
reta_produtividade['Faixa_post'] = reta_produtividade['Faixa'].shift(-1)

reta_produtividade['PERNA S/O C/P'] = reta_produtividade['PERNA C/O C/P REF'] - reta_produtividade['CARTILAGEM DE PERNA'] - reta_produtividade['RESÍDUO DA DESOSSA DE PERNA'] - reta_produtividade['EXCESSO DE PELE DA SOBRECOXA'] - reta_produtividade['OSSO DA SOBRECOXA'] - reta_produtividade['OSSO DA COXA']   

reta_produtividade['PERNA S/O S/P'] = reta_produtividade['PERNA S/O C/P'] - reta_produtividade['PELE DA COXA'] - reta_produtividade['PELE DA SOBRECOXA']

reta_produtividade.reset_index(drop = True, inplace = True)

class AvePesadaCobbMacho():
    
    def __init__(self, peso_medio):
        
        '''
        Dado o peso, raça e sexo da ave, retorna o peso médio de cada perna.
        
        '''
        
        if peso_medio > 4000:
            
            rendimento_perna_c_pele = 0.1764

            rendimento_perna_s_pele = 0.1591
            
        else:
        
            faixas = list(reta_produtividade.loc[(reta_produtividade['Faixa_ant'] < peso_medio)&(reta_produtividade['Faixa_post'] > peso_medio),'Faixa'])

            rendimentos_faixas_com_pele = list(reta_produtividade.loc[(reta_produtividade['Faixa_ant'] < peso_medio)&(reta_produtividade['Faixa_post'] > peso_medio),'PERNA S/O C/P'])

            rendimentos_faixas_sem_pele = list(reta_produtividade.loc[(reta_produtividade['Faixa_ant'] < peso_medio)&(reta_produtividade['Faixa_post'] > peso_medio),'PERNA S/O S/P'])

            if len(faixas) == 1:
                rendimento_perna_c_pele = rendimentos_faixas_com_pele[0]

                rendimento_perna_s_pele = rendimentos_faixas_sem_pele[0]

            else:

                rendimento_perna_c_pele = (rendimentos_faixas_com_pele[0]*(100 - (peso_medio - faixas[0])) + rendimentos_faixas_com_pele[1]*(100 - (faixas[1] - peso_medio)))/100

                rendimento_perna_s_pele = (rendimentos_faixas_sem_pele[0]*(100 - (peso_medio - faixas[0])) + rendimentos_faixas_sem_pele[1]*(100 - (faixas[1] - peso_medio)))/100
        
        self.rendimento_perna_c_pele = round(rendimento_perna_c_pele,4)
        
        self.rendimento_perna_s_pele = round(rendimento_perna_s_pele,4)
        
