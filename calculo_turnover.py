# Databricks notebook source
# !pip install gcsfs
# !pip install xlsxwriter

# COMMAND ----------

import pandas as pd
import numpy as np
import math
import io
from datetime import datetime
from datetime import timedelta
from calendar import monthrange

from google.cloud import storage

from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
sc = SparkContext.getOrCreate()
spark = SparkSession(sc)

# COMMAND ----------

df = spark.read.format('parquet').load('gs://seara-us-dev-absent-data/base_cad_funcionarios')

# COMMAND ----------

df = df.select(
  'CD_FILIAL_HIERARQUIA',
  'FILIAL',
  'CPF',
  'CHAPA_CAD',
  'DATAADMISSAO',
  'DATADEMISSAO',
  'SECAO',
  'SITUACAO',
  'DATA_CAD'
)

# COMMAND ----------

# df = df.filter(
#   df.DATA_CAD >= datetime(2018,1,1).date()
# )

# COMMAND ----------

df = df.toPandas()

# COMMAND ----------

df.rename(columns={
  'CD_FILIAL_HIERARQUIA': 'Filial',
  'FILIAL': 'Filial Contábil',
  'CHAPA_CAD': 'Chapa',
  'DATAADMISSAO': 'Admissão',
  'DATADEMISSAO': 'Demissão',
  'SECAO': 'Seção',
  'SITUACAO': 'Situação',
  'DATA_CAD': 'Data'
}, inplace = True)

# COMMAND ----------

df = df[~df['Seção'].isna()]

# COMMAND ----------

# df = pd.read_csv('gs://bases_qlik/quadro-funcionarios/QUADRO FUNCIONARIOS 2020-01 A 2021-04.csv', sep=';')

df['Demissão'] = pd.to_datetime(df['Demissão'], format = '%d/%m/%Y', errors = 'coerce')

df['Data'] = pd.to_datetime(df['Data'], format = '%d/%m/%Y', errors = 'coerce')

df['Admissão'] = pd.to_datetime(df['Admissão'], format = '%d/%m/%Y', errors = 'coerce')

# print(df.shape)

# df.head()

# COMMAND ----------

df['TURNO'] = df['Seção'].apply(lambda x: '1' if ('1° Turno' in x)|('1T' in x)|('1° T' in x) else (
    '2' if ('2° Turno' in x)|('2T' in x)|('2° T' in x) else (
        '3' if ('3° Turno' in x)|('3T' in x)|('3° T' in x) else
            '0'
    )
))

df['Atividade'] = df['Seção'].apply(
    lambda x: 'Desossar Perna' if 'Desossar Perna' in x else (
        'Refilar Perna' if 'Refilar Perna' in x else ('Revisar Perna' if 'Revisar Desossa Perna' in x else None)
    )
)

# COMMAND ----------

# Retira os colaboradores demitidos antes de Jan 2020, pois eles não serão relevantes para calcular o turnover a partir de 2020.
df = df[(df['Demissão'] >= datetime(2020,1,1))|(pd.isnull(df['Demissão']))]

# Nesse momento estamos interessados somente nas seções de dessosar e revisar desossa de perna
#df = df[df['Atividade'].isin(['Desossar Perna','Revisar Perna'])]

# COMMAND ----------

df = df[df['Data'].apply(lambda x: x.day == 1)]

# COMMAND ----------

lista_colunas = ['Filial Contábil','Filial','CPF','Chapa','Admissão','Demissão','Seção','TURNO','Situação']

df_final = pd.DataFrame(columns=['Filial Contábil','Filial','CPF','Chapa_1','Chapa_2', 'Admissão_1','Admissão_2', 'Demissão_1','Demissão_2', 'Seção_1', 'Seção_2', 'TURNO_1', 'TURNO_2',
       'Situação_1', 'Situação_2'])

for data in df['Data'].sort_values(ascending = True).unique():

    data_1 = pd.to_datetime(data)

    if data_1.month == 12:
        data_2 = data_1.replace(month = 1, year = data_1.year+1)
    else:
        data_2 = data_1.replace(month = data_1.month + 1)

    df_1 = df[(df['Data'] == data_1)&(df['Atividade'].isin(['Desossar Perna','Revisar Perna']))][lista_colunas]
    df_2 = df[df['Data'] == data_2][lista_colunas]

    aux = df_1.merge(df_2, how = 'left', on=['Filial Contábil','Filial','CPF'], suffixes=("_1","_2"))

    aux = aux[['Filial Contábil','Filial','CPF','Chapa_1','Chapa_2', 'Admissão_1','Admissão_2', 'Demissão_1','Demissão_2', 'Seção_1', 'Seção_2', 'TURNO_1', 'TURNO_2',
           'Situação_1', 'Situação_2']]

    aux['Período de referência'] = str(data_1)[:7] + ' a ' + str(data_2)[:7]

    df_final = pd.concat([df_final, aux], ignore_index = True)

df_final = df_final[df_final['Período de referência'] != '2021-04 a 2021-05']

# print(df_final.shape)

# df_final.head()

# COMMAND ----------

df_final['QL_ATIVO_1'] = df_final['Situação_1'].isin([
    'Ativo', 
    'Férias',
    #'Licença Mater.',
    'Aviso Prévio',
    #'Licença Mater. Compl. 180 dias',
      ]).apply(lambda x: 'Sim' if x == True else 'Não')

df_final['QL_ATIVO_2'] = df_final['Situação_2'].isin([
    'Ativo', 
    'Férias',
    #'Licença Mater.',
    'Aviso Prévio',
    #'Licença Mater. Compl. 180 dias',
      ]).apply(lambda x: 'Sim' if x == True else 'Não')

# COMMAND ----------

df_final['TURNOVER'] = 'Não'

df_final.loc[df_final['QL_ATIVO_1'] == 'Não', 'TURNOVER'] = 'N/A'

df_final.loc[(df_final['QL_ATIVO_1']=='Sim')&(df_final['QL_ATIVO_2']=='Não')&(df_final['Situação_2']=='Demitido'),'TURNOVER'] = 'Sim'

#df_final['MOTIVO TURNOVER'] = np.nan

#df_final.loc[df_final['TURNOVER'] == 'Sim','MOTIVO TURNOVER'] = df_final.loc[df_final['TURNOVER'] == 'Sim','Situação_2']

# COMMAND ----------

# Pessoas que mudaram de seção também causam turnover <---- POR ENQUANTO NÃO SERÁ CONSIDERADO ISSO

#df_final.loc[(df_final['Seção_1'] != df_final['Seção_2'])&(~pd.isnull(df_final['Seção_2'])), 'TURNOVER'] = 'Sim'

#df_final.loc[(df_final['Seção_1'] != df_final['Seção_2'])&(~pd.isnull(df_final['Seção_2'])), 'MOTIVO TURNOVER'] = 'Mudança de Seção'

# COMMAND ----------

#df_final.to_csv('Turnover funcionários.csv', sep = ';', index = False)

# COMMAND ----------

aux = df_final[df_final['QL_ATIVO_1']=='Sim']

aux = aux.groupby(['Filial Contábil','Filial','Seção_1','Período de referência']).agg({
    'CPF': 'count'
}).reset_index().rename(columns={'CPF':'QL ATIVO', 'Seção_1': 'Seção'})

# aux.head()

# COMMAND ----------

df_final_agg = df_final.groupby(['Filial Contábil','Filial','Seção_1','Período de referência','TURNOVER']).agg({
    'CPF': 'count'
}).reset_index()

df_final_agg.rename(columns={
    'Seção_1':'Seção',
    'CPF': 'Qtde Colaboradores'
}, inplace = True)

df_final_agg = df_final_agg[df_final_agg['TURNOVER'] == 'Sim']

df_final_agg = df_final_agg.merge(aux, how = 'left', on=['Filial Contábil','Filial','Seção','Período de referência'])

df_final_agg['TURNOVER'] = round(df_final_agg['Qtde Colaboradores']/df_final_agg['QL ATIVO'],2)

df_final_agg['TURNO'] = df_final_agg['Seção'].apply(lambda x: '1' if ('1° Turno' in x)|('1T' in x)|('1° T' in x) else (
    '2' if ('2° Turno' in x)|('2T' in x)|('2° T' in x) else (
        '3' if ('3° Turno' in x)|('3T' in x)|('3° T' in x) else
            '0'
    )
))

df_final_agg['Atividade'] = df_final_agg['Seção'].apply(
    lambda x: 'Desossar Perna' if 'Desossar Perna' in x else (
        'Refilar Perna' if 'Refilar Perna' in x else ('Revisar Perna' if 'Revisar Desossa Perna' in x else None)
    )
)

df_final_agg['MES REF'] = df_final_agg['Período de referência'].apply(lambda x: x[5:7]+'-'+x[:4])

# COMMAND ----------

# df_final_agg.columns

# COMMAND ----------

df_final_agg['POND_TURNOVER'] = df_final_agg['QL ATIVO']*df_final_agg['TURNOVER']

df_final_agg_aux = df_final_agg.groupby(['Filial Contábil', 'Filial', 'Seção', 'Período de referência', 'TURNO', 'MES REF']).agg({
    'QL ATIVO': 'sum',
    'POND_TURNOVER': 'sum',
    'Qtde Colaboradores': 'sum'
}).reset_index()

df_final_agg_aux['TURNOVER'] = df_final_agg_aux['POND_TURNOVER']/df_final_agg_aux['QL ATIVO']

df_final_agg_aux['Atividade'] = 'Desossar e Revisar'

df_final_agg = pd.concat([df_final_agg,df_final_agg_aux], ignore_index = True)

# COMMAND ----------

df_final_agg.drop(columns=['Qtde Colaboradores','QL ATIVO', 'POND_TURNOVER'], inplace = True)

# df_final_agg

# COMMAND ----------

df_final = df_final[:1000000]

# COMMAND ----------

storage_client = storage.Client()
bucket = storage_client.bucket('resultados_dmdo')
blob = bucket.blob('Turnover/Turnover.xlsx')
with io.BytesIO() as output:
    writer = pd.ExcelWriter(output, engine='xlsxwriter')
    df_final.to_excel(writer, sheet_name='Analítico Turnover', index = False)
    df_final_agg.to_excel(writer, sheet_name='Resultados Turnover', index = False)
    writer.save()
    output.seek(0)
    blob.upload_from_file(output, content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
