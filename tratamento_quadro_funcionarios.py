# Databricks notebook source
# !pip install unidecode
# !pip install bigquery

# COMMAND ----------

# MAGIC %run /Users/yohan.consani@seara.com.br/comparativo_produtividade/Aves/parametros_padrao

# COMMAND ----------

from pyspark.sql.functions import when, col, floor, trim, regexp_replace, lit, current_date, to_timestamp, create_map, split, last 
from pyspark.sql.types import StructType, StructField, DoubleType, IntegerType, StringType, DateType
from datetime import date,  timedelta
from pyspark.sql.functions import unix_timestamp, from_unixtime
from pyspark.sql.functions import to_date
from pyspark.sql.functions import udf
import pyspark.sql.functions as F

from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
sc = SparkContext.getOrCreate()
spark = SparkSession(sc)

# COMMAND ----------

import pandas as pd
import numpy as np
import math
from datetime import datetime
from datetime import timedelta
from calendar import monthrange
from parametros_padrao import dict_prod_padrao_desossa
from parametros_padrao import dict_prod_padrao_revisao
from parametros_padrao import dict_minutos_efetivos_turno

from google.cloud import bigquery as bq
client = bq.Client()
import unidecode

# COMMAND ----------

def remove_caracteres_especiais(texto):
    
    texto_final = ""
    
    for character in texto:
        if (character.isalnum())|(character == "_"):
            texto_final += character
    
    return texto_final

# COMMAND ----------

def trata_nome_coluna(nome_coluna):
    
    return remove_caracteres_especiais(unidecode.unidecode(nome_coluna).replace(" ", "_"))

# COMMAND ----------

# MAGIC %md
# MAGIC #### Horas trabalhadas - Gera txt

# COMMAND ----------

#!gsutil cp gs://raw-absent-new-bases-full/HORAS_TRABALHADAS.csv gs://gera-txt-funcionario/HORAS_TRABALHADAS.csv

# COMMAND ----------

query = """
SELECT
    *
FROM 
    `auditoria-horas-extras-nonprod.horas_extras_output.TB_HORAS_TRABALHADAS`
WHERE 
    DATA_TRABALHADA >= '2021-01-01'
    AND NOMEDIRETORIAEXECUTIVA = 'NEG. AVES PESADAS'
"""

df_horas_trabalhadas_original = client.query(query).to_dataframe()

df_horas_trabalhadas_original['CPF'] = df_horas_trabalhadas_original['CPF'].astype(int).astype(str)

# print(df_horas_trabalhadas_original.shape)

# COMMAND ----------

# df_horas_trabalhadas_original[
#   (df_horas_trabalhadas_original['CODEMPRESA'] == 's639')
#   &(df_horas_trabalhadas_original['TURNO'] == '1')
#   &(df_horas_trabalhadas_original['DATA_TRABALHADA'].dt.date == datetime(2021,1,4).date())
#   &(df_horas_trabalhadas_original['SECAO'] == 'Gr Aves - Perna - Desossar Perna - 1° Turno')
# ].shape[0]

# COMMAND ----------

# MAGIC %md
# MAGIC ### Adicionando a data de admissão dos colaboradores

# COMMAND ----------

df_base_cad = spark.read.format('parquet').load('gs://seara-us-dev-absent-data/base_cad_funcionarios')

df_horas_trabalhadas_aux = df_base_cad.alias('df_horas_trabalhadas_aux')

df_horas_trabalhadas_aux = df_horas_trabalhadas_aux.select('CPF', 'DATAADMISSAO').distinct()

df_horas_trabalhadas_aux = df_horas_trabalhadas_aux.withColumn('DATAADMISSAO', to_date(col('DATAADMISSAO')))

df_horas_trabalhadas_aux = df_horas_trabalhadas_aux.filter(
  ~col('CPF').isNull()
).orderBy('CPF', 'DATAADMISSAO')

# COMMAND ----------

df_horas_trabalhadas_aux = df_horas_trabalhadas_aux.groupBy('CPF').agg(last('DATAADMISSAO').alias('DATAADMISSAO'))

# COMMAND ----------

df_horas_trabalhadas_aux = df_horas_trabalhadas_aux.withColumnRenamed('DATAADMISSAO', 'DATAADMISSAO_new')

print(df_horas_trabalhadas_aux.count())

# df_horas_trabalhadas_aux.display()

# COMMAND ----------

df_horas_trabalhadas_original['DATA_TRABALHADA'] = df_horas_trabalhadas_original['DATA_TRABALHADA'].dt.date
df_horas_trabalhadas_original['DATAADMISSAO'] = df_horas_trabalhadas_original['DATAADMISSAO'].dt.date
df_horas_trabalhadas_original['DATANASCIMENTO'] = df_horas_trabalhadas_original['DATANASCIMENTO'].dt.date

# COMMAND ----------

schema = StructType([
  StructField("EMPRESA", StringType(), True), 
  StructField("CPF_GESTOR", StringType(), True), 
  StructField("CODCCUSTO", StringType(), True), 
  StructField("CODEMPRESA", StringType(), True), 
  StructField("DATA_TRABALHADA", DateType(), True), 
  StructField("QTDEHORASTRABALHADAS", DoubleType(), True), 
  StructField("CENTROCUSTO", StringType(), True),
  StructField("NOME", StringType(), True),
  StructField("CARGO", StringType(), True),
  StructField("FUNCAO", StringType(), True),
  StructField("DATAADMISSAO", DateType(), True),
  StructField("DATANASCIMENTO", DateType(), True),
  StructField("TEMPOCASA", StringType(), True),
  StructField("SEXO", StringType(), True),
  StructField("CPF", StringType(), True),
  StructField("COD_SECAO", StringType(), True),
  StructField("SECAO", StringType(), True),
  StructField("TURNO", StringType(), True),
  StructField("SITUACAO", StringType(), True),
  StructField("NOMEDIRETORIAEXECUTIVA", StringType(), True),
  StructField("NOMEDIRETORIAADJUNTA", StringType(), True),
  StructField("NOMEGERENCIAEXECUTIVA", StringType(), True),
  StructField("NOMEGERENCIAAREA", StringType(), True),
  StructField("NOMECOORDENACAOSUPERVISAO", StringType(), True),
  StructField("NOME_GESTOR", StringType(), True),
  StructField("REGIONAL", StringType(), True),
  StructField("UF", StringType(), True),
  StructField("ANO_MES", StringType(), True),
])

df_horas_trabalhadas_original = spark.createDataFrame(df_horas_trabalhadas_original, schema = schema)

# COMMAND ----------

df_horas_trabalhadas_original = df_horas_trabalhadas_original.join(
  df_horas_trabalhadas_aux, 
  how = 'left', 
  on = 'CPF'
)

# COMMAND ----------

df_horas_trabalhadas_original = df_horas_trabalhadas_original.drop('DATAADMISSAO')

df_horas_trabalhadas_original = df_horas_trabalhadas_original.withColumnRenamed(
    'DATAADMISSAO_new', 'DATAADMISSAO'
)

# COMMAND ----------

df_horas_trabalhadas = df_horas_trabalhadas_original.select(
    'EMPRESA','CODEMPRESA', 'DATA_TRABALHADA',
       'QTDEHORASTRABALHADAS',
       'DATAADMISSAO','CPF',
       'COD_SECAO', 'SECAO', 'TURNO', 'SITUACAO'
)

# COMMAND ----------

df_horas_trabalhadas = df_horas_trabalhadas.toPandas()

# COMMAND ----------

df_horas_trabalhadas = df_horas_trabalhadas[df_horas_trabalhadas['SECAO'].apply(lambda x: True if (('Desossar Perna'in x)|
                                                                                                         #('Refilar Perna'in x)|
                                                                                                         ('Revisar Desossa Perna' in x)
                                                                                                       ) else False)]

df_horas_trabalhadas['Atividade'] = df_horas_trabalhadas['SECAO'].apply(
    lambda x: 'Desossar Perna' if 'Desossar Perna' in x else (
        'Refilar Perna' if 'Refilar Perna' in x else ('Revisar Perna' if 'Revisar Desossa Perna' in x else None)
    )
)

df_horas_trabalhadas['CODEMPRESA'] = df_horas_trabalhadas['CODEMPRESA'].apply(lambda x: int(x.replace("s","")))

# print(df_horas_trabalhadas.shape)

# df_horas_trabalhadas.head()

# COMMAND ----------

# df_horas_trabalhadas[
#   (df_horas_trabalhadas['CODEMPRESA'] == 639)
#   &(df_horas_trabalhadas['TURNO'] == '1')
#   &(df_horas_trabalhadas['DATA_TRABALHADA'] == datetime(2021,1,4))
#   &(df_horas_trabalhadas['SECAO'] == 'Gr Aves - Perna - Desossar Perna - 1° Turno')
# ].shape[0]

# COMMAND ----------

# df_horas_trabalhadas['DATA_TRABALHADA'] = df_horas_trabalhadas['DATA_TRABALHADA'].dt.date

# COMMAND ----------

df_horas_trabalhadas['MES_REF'] = df_horas_trabalhadas['DATA_TRABALHADA'].apply(lambda x: str(x)[:7])

df_horas_trabalhadas['Tempo de Empresa (Meses)'] = (df_horas_trabalhadas['DATA_TRABALHADA'] - df_horas_trabalhadas['DATAADMISSAO']).apply(lambda x: round(abs(x.days)/30,1) if not pd.isnull(x) else x)

### REMOVA ESSA LINHA QUANDO A DATA DE ADMISSÃO FOR CORRIGIDA NA BASE
df_horas_trabalhadas['Tempo de Empresa (Meses)'].fillna(5, inplace = True)

# COMMAND ----------

indices_desossar_perna = df_horas_trabalhadas['Atividade'] == 'Desossar Perna'

df_horas_trabalhadas.loc[(indices_desossar_perna)&(df_horas_trabalhadas['Tempo de Empresa (Meses)'] <= 2), 'Capacidade Nominal Pernas/min'] = 1

df_horas_trabalhadas.loc[(indices_desossar_perna)&(df_horas_trabalhadas['Tempo de Empresa (Meses)'] > 2)&(df_horas_trabalhadas['Tempo de Empresa (Meses)'] <= 3), 'Capacidade Nominal Pernas/min'] = 2

df_horas_trabalhadas.loc[(indices_desossar_perna)&(df_horas_trabalhadas['Tempo de Empresa (Meses)'] > 3)&(df_horas_trabalhadas['Tempo de Empresa (Meses)'] <= 4), 'Capacidade Nominal Pernas/min'] = 3

df_horas_trabalhadas.loc[(indices_desossar_perna)&(df_horas_trabalhadas['Tempo de Empresa (Meses)'] > 4), 'Capacidade Nominal Pernas/min'] = df_horas_trabalhadas.loc[(indices_desossar_perna)&(df_horas_trabalhadas['Tempo de Empresa (Meses)'] > 4), 'CODEMPRESA'
].apply(lambda x: dict_prod_padrao_desossa[str(x)] if str(x) in dict_prod_padrao_desossa.keys() else 3.5)


indices_revisar_perna = df_horas_trabalhadas['Atividade'] == 'Revisar Perna'

df_horas_trabalhadas.loc[(indices_revisar_perna)&(df_horas_trabalhadas['Tempo de Empresa (Meses)'] <= 2), 'Capacidade Nominal Pernas/min'] = df_horas_trabalhadas.loc[(indices_revisar_perna)&(df_horas_trabalhadas['Tempo de Empresa (Meses)'] <= 2), 'CODEMPRESA'
].apply(lambda x: dict_prod_padrao_revisao[str(x)]/2 if str(x) in dict_prod_padrao_revisao.keys() else 6)

df_horas_trabalhadas.loc[
    (indices_revisar_perna)
    &(df_horas_trabalhadas['Tempo de Empresa (Meses)'] > 2)
    &(df_horas_trabalhadas['Tempo de Empresa (Meses)'] <= 3)
    , 'Capacidade Nominal Pernas/min'] = df_horas_trabalhadas.loc[
        (indices_revisar_perna)
        &(df_horas_trabalhadas['Tempo de Empresa (Meses)'] > 2)
        &(df_horas_trabalhadas['Tempo de Empresa (Meses)'] <= 3)
        , 'CODEMPRESA'].apply(lambda x: 9 if (
            (str(x) in dict_prod_padrao_revisao.keys())
            &(dict_prod_padrao_revisao.get(str(x)) == 14)
            ) else 8)

df_horas_trabalhadas.loc[
    (indices_revisar_perna)
    &(df_horas_trabalhadas['Tempo de Empresa (Meses)'] > 3)
    &(df_horas_trabalhadas['Tempo de Empresa (Meses)'] <= 4)
    , 'Capacidade Nominal Pernas/min'] = df_horas_trabalhadas.loc[
        (indices_revisar_perna)
        &(df_horas_trabalhadas['Tempo de Empresa (Meses)'] > 3)
        &(df_horas_trabalhadas['Tempo de Empresa (Meses)'] <= 4)
        , 'CODEMPRESA'].apply(lambda x: 11 if (
            (str(x) in dict_prod_padrao_revisao.keys())
            &(dict_prod_padrao_revisao.get(str(x)) == 14)
            ) else 10)

df_horas_trabalhadas.loc[
    (indices_revisar_perna)
    &(df_horas_trabalhadas['Tempo de Empresa (Meses)'] > 4)
    , 'Capacidade Nominal Pernas/min'] = df_horas_trabalhadas.loc[
        (indices_revisar_perna)
        &(df_horas_trabalhadas['Tempo de Empresa (Meses)'] > 4)
        , 'CODEMPRESA'].apply(lambda x: dict_prod_padrao_revisao[str(x)] if (
            (str(x) in dict_prod_padrao_revisao.keys())
            ) else 12)


df_horas_trabalhadas['Tempo de Empresa (Meses)'] = df_horas_trabalhadas['Tempo de Empresa (Meses)'].apply(
    lambda x: '0 a 1 meses' if (x<=1) else (
        '1 a 2 meses' if (x>1)&(x<=2) else (
            '2 a 3 meses' if (x>2)&(x <= 3) else (
                '3 a 4 meses' if (x>3)&(x<=4) else (
                    'Acima de 4 meses' if x > 4 else 'Erro'
                )
            )
        )
    )
)

# COMMAND ----------

df_horas_trabalhadas['QtdeMinutosTrabalhados'] = (df_horas_trabalhadas['QTDEHORASTRABALHADAS']*60).astype(int)

# Retira 60min em pausas se a jornada for maior que 8h, ou 40min em pausas se a jornada for menor que 8h. Retira 15min para vestir uniforme.
df_horas_trabalhadas['MIN_EFETIVOS_TRABALHADOS'] = df_horas_trabalhadas['QtdeMinutosTrabalhados'].apply(lambda x: x - 75 if x < 480 else x - 55)

df_horas_trabalhadas['CAPAC_NOMINAL_PONDERADA'] = df_horas_trabalhadas['Capacidade Nominal Pernas/min']*df_horas_trabalhadas['MIN_EFETIVOS_TRABALHADOS']

# COMMAND ----------

# MAGIC %md
# MAGIC ### Resumo Capacidade nominal trabalhadores

# COMMAND ----------

df_capacidade_nominal = df_horas_trabalhadas.groupby(['MES_REF','DATA_TRABALHADA','CODEMPRESA','EMPRESA','SECAO','Atividade','TURNO','Capacidade Nominal Pernas/min','Tempo de Empresa (Meses)']).agg({
    'CPF': 'count'
}).round(2).reset_index()

df_capacidade_nominal.rename(columns={
    'CPF': 'Nº Colaboradores'
}, inplace = True)

#df_capacidade_nominal = df_capacidade_nominal.groupby(['MES_REF','CodEmpresa','Empresa','Seção','Atividade','Capacidade Nominal Pernas/min','Tempo de Empresa (Meses)']).agg({
#    'Nº Colaboradores': 'mean'
#}).round(2).reset_index()

df_capacidade_nominal.sort_values(by=['CODEMPRESA','DATA_TRABALHADA','TURNO','Atividade'], inplace = True)

df_capacidade_nominal.drop(columns=['SECAO'], inplace = True)

# COMMAND ----------

aux = df_capacidade_nominal.copy()

aux = aux.groupby(['MES_REF', 'DATA_TRABALHADA', 'CODEMPRESA', 'EMPRESA','TURNO',
       'Tempo de Empresa (Meses)']).agg({
    'Nº Colaboradores': 'sum',
    'Capacidade Nominal Pernas/min': 'min'
}).reset_index()

aux['Atividade'] = 'Desossar e Revisar'

df_capacidade_nominal = pd.concat([df_capacidade_nominal, aux], ignore_index = True)

df_capacidade_nominal.sort_values(by=['CODEMPRESA','DATA_TRABALHADA','TURNO','Atividade'], inplace = True)

# df_capacidade_nominal.head()

# COMMAND ----------

# MAGIC %md
# MAGIC #### Obtém a base final de funcionários

# COMMAND ----------

df_quadro_funcionarios = df_horas_trabalhadas.groupby(['EMPRESA', 'CODEMPRESA', 'DATA_TRABALHADA', 'COD_SECAO', 'SECAO', 'TURNO', 'Atividade',
       'MES_REF']).agg({
    'CPF': 'count',
    'MIN_EFETIVOS_TRABALHADOS': 'sum', 
    'CAPAC_NOMINAL_PONDERADA': 'sum'
}).reset_index()

df_quadro_funcionarios['CAPACIDADE_NOMINAL'] = df_quadro_funcionarios['CAPAC_NOMINAL_PONDERADA']/df_quadro_funcionarios['MIN_EFETIVOS_TRABALHADOS']

df_quadro_funcionarios = df_quadro_funcionarios.round({'CAPACIDADE_NOMINAL': 2})

df_quadro_funcionarios['DATA_TRABALHADA'] = pd.to_datetime(df_quadro_funcionarios['DATA_TRABALHADA'])

df_quadro_funcionarios['TURNO'] = df_quadro_funcionarios['TURNO'].astype(int)

df_quadro_funcionarios.drop(columns=['CAPAC_NOMINAL_PONDERADA'], inplace = True)

df_quadro_funcionarios.rename(columns={
    'CPF': 'QL_ATIVO',
    'CODEMPRESA': 'CD_FILIAL',
    'DATA_TRABALHADA': 'DATA'
}, inplace = True)

# COMMAND ----------

df_quadro_funcionarios[
  (df_quadro_funcionarios['CD_FILIAL'] == 639)
  &(df_quadro_funcionarios['TURNO'] == 1)
  &(df_quadro_funcionarios['DATA'] == datetime(2021,1,4))
  &(df_quadro_funcionarios['Atividade'] == 'Desossar Perna')
].shape[0]

# COMMAND ----------

# MAGIC %md
# MAGIC ### Carrega tabelas no BQ

# COMMAND ----------

dataset = bq.dataset.Dataset("dmdo-gastos-305013.bases_qlik")

# COMMAND ----------

# MAGIC %md
# MAGIC #### analitico_quadro_funcionarios

# COMMAND ----------

df_horas_trabalhadas.columns = [trata_nome_coluna(x) for x in df_horas_trabalhadas.columns]

# COMMAND ----------

table_id = "analitico_quadro_funcionarios"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_horas_trabalhadas, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# MAGIC %md
# MAGIC #### quadro_funcionarios_consolidado

# COMMAND ----------

df_quadro_funcionarios.columns = [trata_nome_coluna(x) for x in df_quadro_funcionarios.columns]

# COMMAND ----------

table_id = "quadro_funcionarios_consolidado"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_quadro_funcionarios, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# MAGIC %md
# MAGIC #### capacidade_nominal

# COMMAND ----------

df_capacidade_nominal.columns = [trata_nome_coluna(x) for x in df_capacidade_nominal.columns]

# COMMAND ----------

table_id = "capacidade_nominal"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_capacidade_nominal, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))
